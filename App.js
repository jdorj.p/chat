/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ChatScreen from './src/chat/ChatScreen';
import RoomListScreen from './src/room/RoomListScreen';
import AsyncStorage from '@react-native-community/async-storage';
import AuthLoadingScreen from './src/auth/AuthLoadingScreen';
import LoginScreen from './src/auth/LoginScreen';
import RegisterScreen from './src/auth/RegisterScreen';
import TermsScreen from './src/auth/TermsScreen';
import ConfirmPhoneScreen from './src/auth/ConfirmPhoneScreen';
import ForgotPassScreen from './src/auth/ForgotPassScreen';
import ResetPassScreen from './src/auth/ResetPassScreen';
import NavigationOption from './src/common/NavigationOption';
import SearchScreen from './src/room/SearchScreen';
import {AlertModalView} from './src/common/AlertModalView';
import ProfileScreen from './src/profile/ProfileScreen';
import UserScreen from './src/user/UserScreen';
import {AuthContext} from './src/common/Context';
import PushNotification from './src/common/PushNotification';
import TopNotification from './src/chat/TopNotification';
import ContactsScreen from './src/room/ContactsScreen';
import ContractListScreen from './src/contract/ContractListScreen';
import ContractScreen from './src/contract/ContractScreen';
import ChatInfoScreen from './src/chat/ChatInfoScreen';
import ImageScreen from './src/room/ImageScreen';
import EscrowScreen from './src/escrow/EscrowScreen';
import EscrowModal from './src/escrow/InvoiceModal';
import Util from './src/common/Util';
import BottomModal, {bottomModal, setBottomModal} from './src/components/BottomModal';
import ChooseCard from './src/payment/ChooseCard';
import CardTerms from './src/payment/CardTerms';
import NewCard from './src/payment/NewCard';
import TokenizePayCard from './src/payment/TokenizePayCard';
import WithdrawScreen from './src/escrow/WithdrawScreen';
import UpdateScreen from './src/auth/UpdateScreen';
import Constants from './src/common/Constants';
import {SafeAreaView} from 'react-native-safe-area-context';
import WalkthroughScreen from './src/walkthrough/WalkthroughScreen';
import codePush from "react-native-code-push";
import ForwardScreen from './src/forward/ForwardScreen';

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            syncing: false,
            online: true,
        }
    }

    componentDidMount(): void {

        this.isLogged()

    }

    isLogged = () => {

        AsyncStorage.getItem('token').then((token)=>{

            this.setState({
                token: token,
                isLoading: false
            })
        })
    }

    syncUpdate = () => {

            this.setState({
                syncing: true,
            })
    }

    stopSyncUpdate = () => {

        this.setState({
            syncing: false,
        })
    }

    setConnectionStatus = (online) => {

        this.setState({online: online})
    }

    authStack = () => {

        const Stack = createStackNavigator();

        return (<Stack.Navigator
            screenOptions = {{
                ...NavigationOption,
            }}
        >
            <Stack.Screen name="Walkthrough" component={WalkthroughScreen} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={LoginScreen}/>
            <Stack.Screen name="Register" component={RegisterScreen}/>
            <Stack.Screen name="Terms" component={TermsScreen}/>
            <Stack.Screen name="ConfirmPhone" component={ConfirmPhoneScreen}/>
            <Stack.Screen name="ForgotPass" component={ForgotPassScreen}/>
            <Stack.Screen name="ResetPass" component={ResetPassScreen}/>
        </Stack.Navigator>)
    }


    mainStack = () => {



        const Stack = createStackNavigator();
        return (<Stack.Navigator screenOptions = {{...NavigationOption}}>
            <Stack.Screen name="RoomList" component={RoomListScreen} options={{headerShown: false}} />
            <Stack.Screen name="Chat" component={ChatScreen} options={{headerShown: false}} />
            <Stack.Screen name="User" component={UserScreen}/>
            <Stack.Screen name="Profile" component={ProfileScreen}/>
            <Stack.Screen name="Contracts" component={ContractListScreen}/>
            <Stack.Screen name="Contract" component={ContractScreen} />
            <Stack.Screen name="ChatInfo" component={ChatInfoScreen}/>
            <Stack.Screen name="Escrow" component={EscrowScreen}/>
            <Stack.Screen name="ChooseCard" component={ChooseCard}/>
            <Stack.Screen name="CardTerms" component={CardTerms}/>
            <Stack.Screen name="NewCard" component={NewCard}/>
            <Stack.Screen name="TokenizePayCard" component={TokenizePayCard}/>
            <Stack.Screen name="Withdraw" component={WithdrawScreen}/>
        </Stack.Navigator>)
    }

    rootStack = () => {

        PushNotification.requestUserPermission()
        PushNotification.saveToken()


        const Stack = createStackNavigator();

        return (<Stack.Navigator mode="modal" screenOptions = {{...NavigationOption}}>
                    <Stack.Screen
                        name="Root"
                        component={this.mainStack}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen name="Search" component={SearchScreen} />
                    <Stack.Screen name="Contacts" component={ContactsScreen} />
                    <Stack.Screen name="Forward" component={ForwardScreen} />
                    <Stack.Screen name="Image" component={ImageScreen}/>
        </Stack.Navigator>)
    }


    render() {

        if(this.state.syncing){
            return (<UpdateScreen stopSyncUpdate = {this.stopSyncUpdate} />)
        }

        if(this.state.isLoading){
            return (<AuthLoadingScreen />)
        }

        return (
            <AuthContext.Provider value={{isLogged: this.isLogged, syncUpdate: this.syncUpdate, setConnectionStatus: this.setConnectionStatus}}>
                <View style = {{flex:1}} >
                    {(!this.state.online) ? (<SafeAreaView><View style = {styles.statusContainer} ><Text style = {styles.status} >Холболт салсан байна</Text></View></SafeAreaView>) : null}
                    <NavigationContainer ref = "navigation"
                                         onReady={() => { Util.navigation = this.refs.navigation }}
                    >
                        {(!this.state.token) ? (this.authStack()) : (this.rootStack())}
                    </NavigationContainer>
                    <TopNotification/>
                    <BottomModal ref = {(ref)=> setBottomModal(ref)} />
                    <AlertModalView/>
                </View>
             </AuthContext.Provider>
        );
    }


}

let codePushOptions = {
    installMode: codePush.InstallMode.ON_NEXT_RESTART
};

export default codePush(codePushOptions)(App)
// export default codePush(App)


const styles = StyleSheet.create({

    statusContainer: {
        padding: 8,
        alignItems: 'center',
        backgroundColor: Constants.COLORS.ERROR
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

})
