import React from 'react';
import {
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    View,
    Text,
    Platform,
    TouchableOpacity,
} from 'react-native';

import { getStatusBarHeight } from 'react-native-status-bar-height';
import UserUtil from '../common/UserUtil';
import ListButton from '../components/ListButton';
import VersionView from '../components/VersionView';
import CustomAlert from '../common/CustomAlert';
import {AuthContext} from '../common/Context';
import {ProfileImage} from '../common/ProfileImage';
import {stompContext} from '../chat/Stomp';
let statusBarHeight = getStatusBarHeight(true);
import CodePush from 'react-native-code-push';

export default class UserScreen extends React.Component {


    constructor(props){
        super(props);

        this.state = {
            user: {},
            update: false
        }

    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: "Хэрэглэгч",
        })

         this.unsubscribeFocus = this.props.navigation.addListener(
            'focus',
            payload => {

                UserUtil.getData().then((data)=>{

                    this.setState({user: data})
                })

                UserUtil.updateUserData().then((data)=>{

                    this.setState({user: data})
                })

            }
        );

         this.checkUpdate()
    }

    componentWillUnmount() {

        this.unsubscribeFocus();
    }

    checkUpdate = () => {

        CodePush.checkForUpdate()
            .then((update) => {

                console.log('update',update)

                if (!update) {
                    this.setState({update: false})
                } else {
                    this.setState({update: true})
                }
            });

    }

    static contextType = AuthContext

    logout = () => {

        CustomAlert.confirm("Гарах","Та апп-с гарах гэж байна уу?",(response)=>{
        // this.refs.alertModalView.confirm("Гарах","Та апп-с гарах гэж байна уу?",(response)=>{

            if(response){

                UserUtil.deleteUserData(()=>{

                    let isLogged = this.context.isLogged
                    isLogged()
                })

                stompContext.removeStompClient()
            }

        })
    }

    update = () => {

        this.context.syncUpdate()

    }


    render() {

        return (
            <SafeAreaView style = {{flex: 1, backgroundColor: '#fff'}} >
                <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} keyboardVerticalOffset = {(Platform.OS === 'ios')? 88 + statusBarHeight : 0} >
                    <ScrollView >
                        <View style = {{padding: 16,flexDirection: 'row',alignItems: 'center'}} >

                            <TouchableOpacity onPress = {()=> this.state.user.profileImg && this.props.navigation.navigate('Image',{url: this.state.user.profileImg})} >
                                <ProfileImage size = {88} url = {this.state.user.profileImg}  />
                            </TouchableOpacity>

                            <View style= {{marginLeft: 16}} >
                                { this.state.user.lastName ? <Text style = {styles.lastName} >{this.state.user.lastName}</Text> : null }
                                <Text style = {styles.name} >{this.state.user.firstName}</Text>
                                {(this.state.user.email) ? (<Text style = {styles.text} >{this.state.user.email}</Text>) : null}
                                <Text style = {styles.text} >{this.state.user.phone}</Text>
                            </View>
                        </View>

                        <View>
                            <ListButton text = "Хэлцэлүүд" onPress = {()=>this.props.navigation.navigate('Contracts')} ></ListButton>
                            {(this.state.update) ? (<ListButton text = "Апп-г шинэчлэх" onPress = {this.update} ></ListButton>) : null}
                            <ListButton text = "Гарах" onPress = {this.logout} hideChevron hideBorder ></ListButton>
                            <VersionView/>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    imageBackground: {
        height: 88,
        width: 88,
        borderRadius: 44,
        overflow: 'hidden',
    },

    lastName: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 14,
        color: '#000',
    },

    name: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 20,
    },

    text:{
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#666',
        marginTop: 4,
    }
});
