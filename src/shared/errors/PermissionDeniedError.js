export default class PermissionDeniedError extends Error {
    constructor(message = 'Татгалзсан хариу ирсэн.') {
        super(message);
        this.name = 'PermissionDeniedError';
    }
}
