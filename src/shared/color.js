export default {
    defaultText: '#000000',
    bgGray: '#f8f9fb',
    mutedText: '#6c757d',
    placeholderText: '#999999',
    headerLineColor: '#e4e4e4',
    inputBorderColor: '#e0e0e0',
    actionIcon: '#6060a0',
};
