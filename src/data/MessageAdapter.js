import getRealm from './Realm';
import 'react-native-get-random-values'
import {v4 as uuid} from 'uuid';
import {stompContext} from '../chat/Stomp';
import Adapter from './Adapter';
import Constants from '../common/Constants';
import {RoomUtil} from '../room/RoomUtil';
import TopNotification from '../chat/TopNotification';
import DateUtil from '../common/DateUtil';
import {EventRegister} from 'react-native-event-listeners';
import Analytics from 'appcenter-analytics';
import AppCenter from 'appcenter';

export const Status = {
    pending: 'pending',
    sent: 'sent',
    failed: 'failed',
}

export const EventTypes = {
    add: 'add',
    update: 'update',
    seen: 'seen',
    roomUpdated: 'roomUpdated',
    roomChanged: 'roomChanged',
    fetch: 'fetch',
}

export const MessageTypes = {
    CHAT: 'CHAT',
    SEEN: 'SEEN',
    IMAGE: 'IMAGE',
    OPTION: 'OPTION',
    SYSTEM: 'SYSTEM',
    UPDATE: 'UPDATE',
    DELETE: 'DELETE',
    DELETED: 'DELETED',
    INVOICE: 'INVOICE',
    WITHDRAW: 'WITHDRAW',
    WITHDRAW_REQUEST: 'WITHDRAW_REQUEST',
    CONTRACT_REQUEST: 'CONTRACT_REQUEST',
    CONTRACT_END_REQUEST: 'CONTRACT_END_REQUEST',
    INVOICE_CANCEL_REQUEST: 'INVOICE_CANCEL_REQUEST',
    CONTRACT_CANCEL_REQUEST: 'CONTRACT_CANCEL_REQUEST',
    INVOICE_CREATE: 'INVOICE_CREATE',
    INVOICE_CREATE_WITH_DESC: 'INVOICE_CREATE_WITH_DESC',
    CONTRACT_END: 'CONTRACT_END',
    ESCROW_UPDATE: 'ESCROW_UPDATE',
    GIFTCARD: 'GIFTCARD',
    MUTE: 'MUTE',
    UNMUTE: 'UNMUTE',
}

export const OptionTypes = {
    cancel: 'cancel',
    info: 'info',
    withdrawRequest: 'withdrawRequest',
    reverseRequest: 'reverseRequest',
    invoice: 'invoice',
    amount: 'amount',
    giftCard: 'giftCard',
}

const PAGE_SIZE = 20

export default class MessageAdapter extends Adapter {

    constructor(roomId,data,dataSetter) {
        super(data,dataSetter);

        this.roomId = roomId
        this.initData()
        this.seenDictionary = {}
        this.fetching = false

        this.subscribeStomp()

    }


    subscribeStomp = async () => {

        this.unSubscribeStomp()
        console.log('subscribeStomp')

        try{

            // let userId = await this.getUserId()
            let deviceId = await AppCenter.getInstallId()


            this.stompSubscribed = stompContext.getStompClient().subscribe('/queue/private.' + deviceId,
                (message) => {

                    let data = JSON.parse(message.body)
                    console.log('stomp message',data)
                    data.status = Status.sent
                    this.save(data)

                })

        }catch (e) {

            console.log('subscribeStomp',e)
        }
    }


    // save local and send
    add = async (data,send = true,roomId = this.roomId) => {

        data = {
            id: uuid(),
            status: Status.pending,
            roomId: roomId,
            fromUserId: await this.getUserId(),
            ...data,
        }

        console.log('add',data)

        this.save(data,true)
        if(send){
            this.send(data)
        }
    }


    saveMultiple = async (data,callback) => {

        let realm = await getRealm()

        realm.write(()=>{

            let results = []
            for(let i = 0; i < data.length; i++){

              let message = data[i]
              message.time = DateUtil.getTime(message.createdDate)
              message.status = Status.sent

              let result = realm.create("Message",message,"modified")
              results.push(result)
            }

            callback && callback(results)
        })

    }

    // save to local
    save = async (message,isNew = false) => {

        message.time = DateUtil.getTime(message.createdDate)
        // console.log('save',message)

        if(isNew){

            if(message.type == MessageTypes.ESCROW_UPDATE || message.type == MessageTypes.CONTRACT_END){
                 EventRegister.emit('chatBadge');
            }

            await this.new(message)
        }else if(message.type == MessageTypes.SEEN){
           await this.seen(message)
        }else if(message.type == MessageTypes.UPDATE){
            await this.update(message)
        }else if(message.type == MessageTypes.DELETED){
            await this.delete(message)
        }else if(await this.getMessage(message.id)) {
            await this.update(message)
        }else{
            await this.new(message)
        }

    }

    new = async (message) => {

        let realm = await getRealm()
        let userId = await this.getUserId()

        realm.write(()=>{

            try{
                let result = realm.create("Message",message, "modified")

                if(this.roomId == message.roomId){
                    this.prependData(result)
                    this.callListener(EventTypes.add,result)
                    if(message.type == 'SYSTEM' && message.room){
                        this.callListener(EventTypes.roomUpdated,result.room)
                    }
                }else{

                    if(message.fromUserId != userId) {

                        let roomUtil = new RoomUtil(userId)
                        let data =  {
                            image: roomUtil.roomImage(message.room),
                            name: roomUtil.roomName(message.room),
                            content: roomUtil.getContent(message,message.room.isGroup),
                        }

                        TopNotification.show(data,() => {
                            this.callListener(EventTypes.roomChanged,result.room)
                        })
                    }
                }
            }catch (e) {

                Analytics.trackEvent('New message error', {userId: userId,messageId: message.id, error: e.message,});
            }
        })

    }


    seen = async (newMessage) => {

        let realm = await getRealm()
        let message = await this.getMessage(newMessage.id)

        if(message){
            realm.write(()=>{
                message.seenUserList = newMessage.seenUserList

                if(this.roomId == message.roomId) {
                    this.updateData(message)
                    this.callListener(EventTypes.seen, message)
                }
            })
        }else{
            console.log('message not found')
        }
    }

    update = async (newMessage) => {

        console.log('update',newMessage)

        let message = await this.getMessage(newMessage.id)
        console.log('update message',message)
        let realm = await getRealm()

            realm.write(()=>{

                newMessage.type = message.type
                let result = realm.create("Message",newMessage,"modified")

                if(this.roomId == message.roomId){
                    this.updateData(result)
                    this.callListener(EventTypes.update,result)
                }
            })

    }

    delete = async (newMessage) => {

        console.log('delete',newMessage)

        let message = await this.getMessage(newMessage.id)
        console.log('delete message',message)
        let realm = await getRealm()

        realm.write(()=>{

            let result = realm.create("Message",newMessage,"modified")

            if(this.roomId == message.roomId){
                this.updateData(result)
                this.callListener(EventTypes.update,result)
            }
        })

    }


    getMessage = async (messageId) => {

        console.log('getMessage',messageId)

        let realm = await getRealm()
        let messages = realm.objects('Message').filtered('id = $0',messageId)

        if(messages.length > 0){
            return messages[0]
        }else{
            return null
        }
    }

    // sent to server
    send = (data,save = true) => {

        console.log('send',data)

        let client = stompContext.getStompClient()
        let body = JSON.stringify({createdDate: DateUtil.getIsoDate(),...data})

        try {
            client.publish({destination: "/app/chat.send", body: body});
            console.log('stomp send')
            return true
        }catch (e) {
            console.log('send error',e)
            save && this.save({
                ...data,
                status: Status.failed
            })
            return false
        }

    }


    sendSeen = async (message) => {

        let fromUserId = await this.getUserId()

        if(message.fromUserId == fromUserId){
            return
        }

        if(await this.isSeen(message)){
            return
        }

        if(this.send({
            id: message.id,
            type: MessageTypes.SEEN,
            fromUserId: fromUserId,
            roomId: message.roomId
        },false)){
            console.log('send seen', message.content)
            this.seenDictionary[message.id] = true
        }else{
            console.log('seen failed', message.content)
        }
    }

    isSeen = async (message) => {


        if(!message || this.seenDictionary[message.id]){
            return true
        }

        if(!message.seenUserList){
            return false
        }

        for(let i = 0; i < message.seenUserList.length; i++){

            let user = message.seenUserList[i]

            if(user && user.userId == await this.getUserId()){
                return true
            }
        }

        return false;
    }


    getLastChat = () => {

        let lastChat = this.data.length > 0 ? this.data[this.data.length - 1] : null
        return lastChat
    }


    initData = async () => {

        let realm = await getRealm()
        let messages = realm.objects('Message').filtered("roomId = $0 SORT(time DESC) LIMIT("+PAGE_SIZE+")",this.roomId).map((item)=>item)

        this.setData(messages)

        let lastChat =  this.getLastChat()

        // console.log('first chat',messages[0])
        // console.log('first chat options',messages[0].options)
        // console.log('init last chat',messages[messages.length - 1])
        // console.log('local chat',messages)

        if(!lastChat || messages.length < PAGE_SIZE){
            this.callListener(EventTypes.fetch,true)
        }

        if(messages.length < PAGE_SIZE){
            this.fetchData({})
        }else{
            this.fetchData({before: lastChat})
        }
    }

    onConnect (instance){

        instance.subscribeStomp()
        instance.fetchData({before: this.getLastChat()})
    }



    nextData = async () => {

        if(this.lasted){
            return
        }

        let lastChat = this.getLastChat()
        if(!lastChat){
            return
        }

        let realm = await getRealm()

        let messages = realm.objects('Message').filtered('roomId = $0 AND time <  $1 SORT(time DESC) LIMIT('+PAGE_SIZE+')',this.roomId, lastChat.time).map((item)=>item)

        if(messages.length > 0){
            this.setData([...this.data.concat(messages)])
        }else{
            this.callListener(EventTypes.fetch,true)
            // хамгийн сүүлийн буюу хамгийн эхэнд бичсэн чат id-д хүрсэн гэж үзнэ
            this.lasted = true
        }

        this.fetchData({after: lastChat})
    }

    fetchData = async ({before, after}) => {

        // return

        if(this.fetching){
            return
        }

        // if(after && this.lasted){
        //     return
        // }

        this.fetching = true

        // chatId -gaas hoishih buyu umnu ni bichigdsen chat-g awna
        let chatId = after ? after.id : null
        let firstChatId = before ? before.id : null


        try{

            let url = Constants.WSURL + "wsock/chat/findByRoomId?chatId=" + chatId +"&firstChatId=" + firstChatId + "&rowNumber=" + PAGE_SIZE + "&roomId=" + this.roomId + "&userId=" + await this.getUserId()

            console.log('message fetch url',url)

            let response = await fetch(url, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            })


            let json = await response.json()

            // console.log('message fetched json',json)

            if(json.status == "000"){

                if(after && json.entity.length == 0){
                    this.callListener(EventTypes.fetch,false)
                    this.fetching = false
                    // хамгийн сүүлийн буюу хамгийн эхэнд бичсэн чат id-д хүрсэн гэж үзнэ
                    this.lasted = true
                    return
                }

                this.lasted = false

                // console.log('fetched data length', json.entity.length)
                // console.log('fetched first chat',json.entity[0])
                // console.log('fetched last chat',json.entity[json.entity.length - 1])
                this.saveMultiple(json.entity,(data)=>{

                    this.reloadData(data)
                })


            }else{

                // this.listeners.error && this.listeners.error(json.message)
            }

        }catch (e) {

            console.log('fetch error',e)
            // this.listeners.error && this.listeners.error(e.message)
        }

        this.callListener(EventTypes.fetch,false)

        this.fetching = false


    }

    prependData = (message) => {

        // console.log('prependData message',message)
        // console.log('prependData data',this.data)
        this.data.unshift(message)
        // console.log('prependData data',this.data)
        this.setData(this.data)
    }

    updateData = (message) => {

        console.log('update',message)

        for(let i = 0; i < this.data.length; i++){
            if(this.data[i].id == message.id){
                this.data[i] = message
                this.setData(this.data)
                return
            }
        }
    }


    reloadData = async (data) => {


        if(data.length == 0){
            return
        }

        let lastChat = data[data.length - 1]

        let realm = await getRealm()
        let messages = realm.objects('Message').filtered('roomId = $0 AND time >= $1 SORT(time DESC)',this.roomId, lastChat.time).map((item)=>item)
        this.setData(messages)

    }


}

