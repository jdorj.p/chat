import ChatUtil from '../chat/ChatUtil';
import Constants from '../common/Constants';
import getRealm from './Realm';
import AsyncStorage from '@react-native-community/async-storage';
import {stompContext, StompEventTypes} from '../chat/Stomp';

class Room {

    getData = async () => {
        this.fetchData()
        let realm = await getRealm()
        let rooms = realm.objects('Room').sorted('lastDate',true)
        return rooms
    }

    listeners = {}

    addListener = (name, listener) => {
          this.listeners[name] = listener
    }

    removeListener = (name) => {
        this.listeners[name] = undefined
    }

    removeListeners = () => {
          this.listeners = {}
    }

     fetchData = async () => {

        this.listeners.fetching && this.listeners.fetching()

        let userId = await AsyncStorage.getItem("userId")

        try{

            let response = await fetch(Constants.WSURL + "wsock/room/findByUserId?userId=" + userId, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            })

            let json = await response.json()

            if(json.status == "000") {

                await this.saveDatas(json.entity)

            }else{
                console.log('error',json)
                this.listeners.error && this.listeners.error(json.message)
                return new Error(json.message)
            }

        }catch (e) {
            console.log(e)
            this.listeners.error && this.listeners.error(error.message)
            return e
        }
    }

     saveDatas = async (datas) => {

        this.listeners.update && this.listeners.update(datas)

        ChatUtil.setBadgeNumber(datas)

        for(let i = 0; i < datas.length; i++){
            await this.saveData(datas[i])
        }
    }

     saveData = async (data) => {

        let realm = await getRealm()

        realm.write(()=>{

            realm.create("Room", {
                id: data.id,
                userIdList: data.userIdList,
                userList: data.userList,
                lastDate: data.lastDate,
                lastChatMessage: data.lastChatMessage,
                name: data.name,
                createdDate: data.createdDate,
                isGroup: data.isGroup,
            },"modified");
        })
    }

    getRoom = async (roomId) => {

        this.fetchRoom(roomId)
        let realm = await getRealm()
        let rooms = realm.objects('Room').filtered("id = $0",roomId)
        return rooms.length > 0 ? rooms[0] : null
    }

    fetchRoom = async (roomId) => {

          try{
              let response = await fetch(Constants.WSURL + "wsock/room/findById", {
                  method: 'POST',
                  headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                  body: JSON.stringify({
                      id: roomId,
                      userId: await AsyncStorage.getItem('userId')
                  })
              })

              let json = await response.json()
              this.listeners.roomFetched && this.listeners.roomFetched(json)
              this.saveData(json.entity)

          }catch (e) {

              console.log(error)
          }

    }

    createRoom = async (users,callback) => {

        let userIdList = users.map((user) => user.userId)
        let userId = await AsyncStorage.getItem("userId")
        userIdList.push(userId)

        try{

          let response = await fetch(Constants.WSURL + "wsock/room", {
              method: 'POST',
              headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
              body: JSON.stringify({userIdList: userIdList, userId: userId})
          })

          let json = await response.json()
          callback({room: json})
          this.saveData(json.entity)

        }catch (e) {

            console.log(error)
            callback({error: e.message})
        }

    }

    open = () => {

        stompContext.getStompClient()

        stompContext.addStompEventListener(
            StompEventTypes.Connect,
            this.onConnect)

        stompContext.addStompEventListener(
            StompEventTypes.Disconnect,
            this.onDisconnect
        )

        stompContext.addStompEventListener(
            StompEventTypes.WebSocketClose,
            this.onWebSocketClose
        )
    }

     onConnect = () => {
         console.log('onConnect')
         this.clearOfflineTimeout()
         this.listeners.online && this.listeners.online()
         this.subscribe()
         this.fetchData()
    }

     onDisconnect = () => {
         this.startOfflineTimeout()
    }

     onWebSocketClose = () => {
          this.startOfflineTimeout()
    }

    offlineTimeout

    startOfflineTimeout = () => {

          if(this.offlineTimeout){
              return
          }

          console.log("startOfflineTimeout")


          this.offlineTimeout = setTimeout(()=>{
              this.listeners.offline && this.listeners.offline()
              this.offlineTimeout = null
          },10000)
    }

    clearOfflineTimeout = () => {
        if(this.offlineTimeout){
            clearTimeout(this.offlineTimeout)
        }
    }

    subscribed

    subscribe = async () => {

        this.unsubscribe()

        try {

            let userId = await AsyncStorage.getItem("userId")

            this.subscribed = stompContext.getStompClient().subscribe('/queue/private.room.' + userId,
                (message) => {

                    let data = JSON.parse(message.body)
                    // console.log('stomp data',data)
                    this.saveDatas(data)

                })
        }catch (e) {

            console.log('room subscribe error',e)
        }

    }

    unsubscribe = () => {

        this.subscribed && this.subscribed.unsubscribe()
    }

    close = () => {
        this.removeListeners()
        this.unsubscribe();
        stompContext.removeStompEventListener(StompEventTypes.Connect,this.onConnect)
        stompContext.removeStompEventListener(StompEventTypes.Disconnect,this.onDisconnect)
        stompContext.removeStompEventListener(StompEventTypes.WebSocketClose,this.onWebSocketClose)

    }


}

export default new Room()

