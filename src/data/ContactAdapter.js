import getRealm from './Realm';

export default class ContactAdapter{

    constructor() {

    }


    getData = async () => {

        let realm = await getRealm()
        let contacts = realm.objects('Contact').sorted('firstName',true).map(contact=>contact)

        return contacts

    }

    save = async (contacts) => {

        if(!contacts){
            return []
        }

        let realm = await getRealm()
        let userIds = []

        realm.write(()=>{

            for(let i = 0; i < contacts.length; i++) {

                let contact = contacts[i]
                userIds[contact.userId] = true

                realm.create("Contact", {
                    userId: contact.userId,
                    contactName: contact.contactName,
                    firstName: contact.firstName,
                    lastName: contact.lastName,
                    phone: contact.phone,
                    imageUrl: contact.imageUrl,
                    systemName: contact.systemName,
                }, "modified");
            }

        })

        this.deleteOthers(userIds)

        return await this.getData()
    }

    /**
     * дамжуулсан userId-с бусад contact-г устгана
     * @param userIds
     * @returns {Promise<void>}
     */
    deleteOthers = async (userIds) => {

        let realm = await getRealm()
        let contacts = realm.objects('Contact')

        realm.write(()=>{

            for(let i = 0; i < contacts.length; i++) {
                let contact = contacts[i]
                if(!userIds[contact.userId]){
                    realm.delete(contact)
                }
            }

        })
    }




}
