
// https://api.minu.mn/wsock/room/findByUserId?userId=101491186865685
// 101491186865685

export const Room = {
    name: "Room",
    primaryKey: "id",
    properties: {
        id: "string",
        userIdList: "string[]",
        muteUserList: "string[]",
        userList: "User[]",
        lastDate: "string?",
        lastChatMessage: "Message?",
        name: "string?",
        createdDate: "string?",
        isGroup: "bool"
    }
};

export const User = {
    name: "User",
    primaryKey: "userId",
    properties: {
        userId: "string",
        name: "string?",
        lastName: "string?",
        phone: "string?",
        avatar: "string?",
        lastOnlineDate: "string?",
        status: "string?",
    }
};

export const Option = {
    name: "Option",
    // primaryKey: "id",
    embedded: true,
    properties: {
        id: "string",
        name: "string?",
        type: "string?",
        order: "int?",
    }
};

export const Message = {
    name: "Message",
    primaryKey: "id",
    properties: {
        id: "string",
        roomId: "string?",
        fromUserId: "string?",
        fromUser: "User?",
        type: "string?",
        typeId: "string?",
        content: "string?",
        contentObjects: "string?",
        seenUserList: "User[]",
        owner: "User?",
        options: {type: "list", objectType: "Option"},
        // options: "Option[]",
        status: "string",
        createdDate: "string?",
        time: "int",
        amount: "string?",
        optionChatId: "string?",
        replyId: "string?",
        replyChat: "Message?",
        forwardId: "string?",
    }
};

export const Contact = {
    name: "Contact",
    primaryKey: "userId",
    properties: {
        userId: "string",
        contactName: "string?",
        firstName: "string?",
        lastName: "string?",
        phone: "string?",
        imageUrl: "string?",
        systemName: "string?"
    }
};

export default [Room,User,Message,Option,Contact]
