import {stompContext, StompEventTypes} from '../chat/Stomp';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import getRealm from './Realm';
import ChatUtil from '../chat/ChatUtil';


class Message {

    getMessages = async (roomId) => {

         let realm = await getRealm()
         let messages = realm.objects('Message').filtered('roomId = $0 SORT(createdDate DESC) LIMIT(20)',roomId)
         return messages
    }

    getData = async ({chatId,roomId}) => {

        this.fetchData({chatId,roomId})
        let realm = await getRealm()

        if(chatId){
            let result = realm.objects('Message').filtered('id = $0',chatId)
            if(result.length > 0){
                let createdDate = result[0].createdDate
                let messages = realm.objects('Message').filtered('createdDate < $0 SORT(createdDate DESC) LIMIT(20)',createdDate)
                return messages
            }else{
                return []
            }
        }

        let messages = realm.objects('Message').filtered('SORT(createdDate DESC) LIMIT(20)') //.sorted('createdDate',true)
        return messages
    }

    listeners = {}

    addListener = (name, listener) => {
        this.listeners[name] = listener
    }

    removeListeners = () => {
        this.listeners = {}
    }

    fetchData = async ({chatId,roomId}) => {

        this.listeners.fetching && this.listeners.fetching(chatId)

        let userId = await AsyncStorage.getItem('userId')

        try{

            let response = await fetch(Constants.WSURL + "wsock/chat/findByRoomId?chatId="+ chatId +"&rowNumber=20&roomId="+ roomId + "&userId=" + userId, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            })

            let json = await response.json()

            if(json.status == "000"){

                this.listeners.fetched && this.listeners.fetched()
                this.saveDatas(json.entity)

            }else{

                this.listeners.error && this.listeners.error(json.message)
            }

        }catch (e) {

            this.listeners.error && this.listeners.error(e.message)
        }


    }

    saveDatas = async (datas) => {

        for(let i = 0; i < datas.length; i++){
            await this.saveData(datas[i])
        }
    }

    saveData = async (data) => {

        let realm = await getRealm()

        realm.write(()=>{

            realm.create("Message", {
                id: data.id,
                roomId: data.roomId,
                fromUserId: data.fromUserId,
                fromUser: data.fromUser,
                type: data.type,
                // seenUserList: "User[]",
                owner: data.owner,
                // options: {type: "list", objectType: "Option"},
                synced: true,
                createdDate: data.createdDate,
            },"modified");
        })
    }


    open = () => {

        stompContext.getStompClient()

        stompContext.addStompEventListener(
            StompEventTypes.Connect,
            this.onConnect)

        stompContext.addStompEventListener(
            StompEventTypes.Disconnect,
            this.onDisconnect
        )

        stompContext.addStompEventListener(
            StompEventTypes.WebSocketClose,
            this.onWebSocketClose
        )
    }

    onConnect = () => {
        console.log('onConnect')
        this.clearOfflineTimeout()
        this.listeners.online && this.listeners.online()
        this.subscribe()
    }

    onDisconnect = () => {
        this.startOfflineTimeout()
    }

    onWebSocketClose = () => {
        this.startOfflineTimeout()
    }

    offlineTimeout

    startOfflineTimeout = () => {

        if(this.offlineTimeout){
            return
        }

        console.log("startOfflineTimeout")


        this.offlineTimeout = setTimeout(()=>{
            this.listeners.offline && this.listeners.offline()
            this.offlineTimeout = null
        },10000)
    }

    clearOfflineTimeout = () => {
        if(this.offlineTimeout){
            clearTimeout(this.offlineTimeout)
        }
    }

    subscribed

    subscribe = async () => {

        this.unsubscribe()

        try{

            let userId = await AsyncStorage.getItem("userId")

            this.subscribed = stompContext.getStompClient().subscribe('/queue/private.' + userId,
                (message) => {

                    this.saveData(JSON.parse(message.body))
                    this.listeners.fetching && this.listeners.fetching(message.body)
                })

        }catch (e) {

            console.log(e)
        }
    }

    unsubscribe = () => {

        this.subscribed && this.subscribed.unsubscribe()

    }

    close = () => {
        this.removeListeners()
        this.unsubscribe();
        stompContext.removeStompEventListener(StompEventTypes.Connect,this.onConnect)
        stompContext.removeStompEventListener(StompEventTypes.Disconnect,this.onDisconnect)
        stompContext.removeStompEventListener(StompEventTypes.WebSocketClose,this.onWebSocketClose)

    }
}

export default new Message()
