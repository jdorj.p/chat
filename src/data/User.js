import getRealm from './Realm';

export default class User{

    static getUser = async (userId) =>{

        let realm = await getRealm()
        let users = realm.objects('User').filtered("userId = $0",userId)
        if(users.length > 0){
            return users[0]
        }else{
            return null
        }
    }
}

