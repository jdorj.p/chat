import Adapter, {EventTypes} from './Adapter';
import {stompContext} from '../chat/Stomp';
import DateUtil from '../common/DateUtil';
import ChatUtil from '../chat/ChatUtil';
import getRealm from './Realm';
import Constants from '../common/Constants';
import {MessageTypes, Status} from './MessageAdapter';
import AppCenter from 'appcenter';


export default class RoomAdapter extends Adapter{

    constructor(data,dataSetter) {
        super(data,dataSetter);

        this.fetching = false
        this.initData()
        this.subscribeStomp()

    }

    initData = async () => {

        let realm = await getRealm()
        let rooms = realm.objects('Room').sorted('lastDate',true)

        this.setData(rooms)
        // this.setData([])

        if(rooms.length == 0){
            this.callListener(EventTypes.fetch,true)
        }
        this.fetchData()
    }

    subscribeStomp = async () => {

        this.unSubscribeStomp()

        try{

            // let userId = await this.getUserId()
            let deviceId = await AppCenter.getInstallId()

            this.stompSubscribed = stompContext.getStompClient().subscribe('/queue/private.room.' + deviceId,
                (message) => {

                    let data = JSON.parse(message.body)
                    // this.print(data)
                    // console.log('room stomp',data)
                    // console.log('room stomp',data[0].lastChatMessage)
                    this.save(data)

                })

        }catch (e) {

            console.log('subscribeStomp',e)
        }
    }

    print = (data) => {

        for(let i = 0; i < data.length; i++){

            // if(data[i].userList){
            //     for(let j = 0; j < data[i].userList.length; j++) {
            //         console.log('room', i, 'userId', data[i].userList[j].userId)
            //     }
            // }

            if(data[i].lastChatMessage){
                console.log('lastChatMessage',data[i].lastChatMessage.content)
                if(data[i].lastChatMessage.seenUserList){
                    for(let j = 0; j < data[i].lastChatMessage.seenUserList.length; j++) {
                        // console.log('room', i, 'seenUser', data[i].lastChatMessage.seenUserList[j])
                        console.log('room', i, 'seenUserId', data[i].lastChatMessage.seenUserList[j].userId)
                    }
                }
            }
        }
    }

    onConnect(instance) {

        instance.subscribeStomp()
        instance.fetchData()
        instance.sendOfflineMessages()
    }

    fetchData = async () => {


        try{

            let response = await fetch(Constants.WSURL + "wsock/room/findByUserId?userId=" +  await this.getUserId(), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            })

            let json = await response.json()

            // console.log('fetch room',json)

            if(json.status == "000") {

                this.save(json.entity)

            }else{
                console.log('error',json)
                this.callListener(EventTypes.error,json.message)
            }

        }catch (e) {
            console.log('fetch error',e)
            this.callListener(EventTypes.error,error.message)
            return e
        }

        this.callListener(EventTypes.fetch,false)
    }

    createRoom = async (users,callback) => {

        let userIdList = users.map((user) => user.userId)
        let userId = await this.getUserId()
        userIdList.push(userId)

        try{

            let response = await fetch(Constants.WSURL + "wsock/room", {
                method: 'POST',
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                body: JSON.stringify({userIdList: userIdList, userId: userId})
            })

            let json = await response.json()
            callback({room: json})

        }catch (e) {

            console.log('create room',error)
            callback({error: e.message})
        }

    }


    save = async (rooms) => {

        ChatUtil.setBadgeNumber(rooms)

        let realm = await getRealm()
        let results = []

        let roomIds = []

        realm.write(()=>{

            for(let i = 0; i < rooms.length; i++) {

                let room = rooms[i]
                roomIds[room.id] = true

                let lastChatMessage = room.lastChatMessage
                if(lastChatMessage){
                    // room ирсэн мессэжийг илгээгдсэн гэж үзнэ
                    // if(lastChatMessage.options){
                    //     console.log('lastChatMessage.options',lastChatMessage.options)
                    // }
                    lastChatMessage.status = Status.sent
                    lastChatMessage.time = DateUtil.getTime(lastChatMessage.createdDate)
                }


               let result = realm.create("Room", {
                    id: room.id,
                    userIdList: room.userIdList,
                    userList: room.userList,
                    muteUserList: room.muteUserList,
                    lastDate: room.lastDate,
                    lastChatMessage: lastChatMessage,
                    name: room.name,
                    createdDate: room.createdDate,
                    isGroup: room.isGroup,
                }, "modified");

                results.push(result)
            }

            // console.log('results',results)

            this.setData(results)
        })

        this.deleteOthers(roomIds)
        // this.deleteOthers(rooms)
    }

    /**
     * дамжуулсан room id-с бусад room-г устгана
     * @param roomIds
     * @returns {Promise<void>}
     */
    deleteOthers = async (roomIds) => {

        let realm = await getRealm()
        let rooms = realm.objects('Room')

        realm.write(()=>{

            for(let i = 0; i < rooms.length; i++) {
                let room = rooms[i]
                if(!roomIds[room.id]){
                    realm.delete(room)
                }
            }

        })
    }


    sendOfflineMessages = async () => {

        let realm = await getRealm()
        let messages = realm.objects('Message').filtered("status = $0 OR status = $1",Status.failed, Status.pending)

        // console.log('offline messages',messages)


        for(let i = 0; i < messages.length; i++){

            let message = messages[i]
            if(message.type == MessageTypes.IMAGE){
                let content = JSON.parse(message.content)
                if(!content.cdnId){
                    continue
                }
            }
            console.log('send offline',message)
            this.send(message)
        }
    }


    send = (data) => {

        console.log('room send message',data)

        let client = stompContext.getStompClient()
        let body = JSON.stringify(data)
        try {
            client.publish({destination: "/app/chat.send", body: body});
        }catch (e) {
            console.log('room send error',e)
        }

    }



}
