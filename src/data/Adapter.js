import {stompContext, StompEventTypes} from '../chat/Stomp';
import AsyncStorage from '@react-native-community/async-storage';

const offlineTimeoutLimit = 10000
export const EventTypes = {
    error: 'error',
    fetch: 'fetch',
    offline: 'offline',
    online: 'online',
}

export default class Adapter {

    constructor(data,dataSetter) {


        this.data = data
        this.dataSetter = dataSetter

        this.getUserId()

        this.listeners = {}

        stompContext.getStompClient()

         stompContext.addStompEventListener(
            StompEventTypes.Connect,
            this._onConnect)

        stompContext.addStompEventListener(
            StompEventTypes.Disconnect,
            this.onDisconnect
        )

        stompContext.addStompEventListener(
            StompEventTypes.WebSocketClose,
            this.onWebSocketClose
        )

    }

    getUserId = async () => {
        if(this.userId){
            return this.userId
        }else{
            this.userId = await AsyncStorage.getItem('userId')
            return this.userId
        }
    }

    setData = (data) => {

        this.data = data
        this.dataSetter && this.dataSetter(data)

    }

    _onConnect = () => {

        this.callListener(EventTypes.online)
        this.clearOfflineTimeout()

        this.onConnect && this.onConnect(this)
    }

    onDisconnect = () => {
        this.startOfflineTimeout()
    }

    onWebSocketClose = () => {
        this.startOfflineTimeout()
    }

    startOfflineTimeout = () => {

        if(this.offlineTimeout){
            return
        }

        this.offlineTimeout = setTimeout(()=>{

            this.callListener(EventTypes.offline)

            this.offlineTimeout = null
        },offlineTimeoutLimit)
    }

    clearOfflineTimeout = () => {
        if(this.offlineTimeout){
            clearTimeout(this.offlineTimeout)
        }
    }

    unSubscribeStomp = () => {
        this.stompSubscribed && this.stompSubscribed.unsubscribe()
    }

    addListener = (name, listener) => {
        this.listeners[name] = listener
    }

    callListener = (name, ...params) => {

        this.listeners[name] && this.listeners[name](...params)
    }

    removeListeners = () => {
        this.listeners = {}
    }


    close = () => {
        this.removeListeners()
        this.unSubscribeStomp()
        stompContext.removeStompEventListener(StompEventTypes.Connect,this._onConnect)
        stompContext.removeStompEventListener(StompEventTypes.Disconnect,this.onDisconnect)
        stompContext.removeStompEventListener(StompEventTypes.WebSocketClose,this.onWebSocketClose)
    }

}

