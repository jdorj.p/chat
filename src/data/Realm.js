import schemas from './schemas'
import AsyncStorage from '@react-native-community/async-storage'
import Realm from 'realm'
import RNFS from 'react-native-fs'

let realm

export default async function getRealm (){

    if(realm){
        return realm
    }else{

        console.log('opening realm')

        let userId = await AsyncStorage.getItem("userId")
        realm = await Realm.open( {
            schema: schemas,
            schemaVersion: 9,
            path: `${RNFS.DocumentDirectoryPath}/${userId}`,
            migration: (oldRealm, newRealm) => {
                // only apply this change if upgrading to schemaVersion 2
                if (oldRealm.schemaVersion < 2) {
                    const oldObjects = oldRealm.objects('Contact');
                    const newObjects = newRealm.objects('Contact');
                    // loop through all objects and set the fullName property in the new schema
                    for (const objectIndex in oldObjects) {
                        // const oldObject = oldObjects[objectIndex];
                        const newObject = newObjects[objectIndex];
                        newObject.systemName = null
                    }
                }

                if (oldRealm.schemaVersion < 5) {
                    const oldMessages = oldRealm.objects('Message');
                    const newMessages = newRealm.objects('Message');
                    // loop through all objects and set the fullName property in the new schema
                    for (const messageIndex in oldMessages) {
                        // const oldObject = oldObjects[objectIndex];
                        const newMessage = newMessages[messageIndex];
                        newMessage.optionChatId = null
                    }
                }

                if (oldRealm.schemaVersion < 6) {
                    const oldMessages = oldRealm.objects('Message');
                    const newMessages = newRealm.objects('Message');
                    // loop through all objects and set the fullName property in the new schema
                    for (const messageIndex in oldMessages) {
                        // const oldObject = oldObjects[objectIndex];
                        const newMessage = newMessages[messageIndex];
                        newMessage.replyId = null
                        newMessage.replyChat = null
                    }
                }

                if (oldRealm.schemaVersion < 7) {
                    const oldMessages = oldRealm.objects('Message');
                    const newMessages = newRealm.objects('Message');
                    // loop through all objects and set the fullName property in the new schema
                    for (const messageIndex in oldMessages) {
                        // const oldObject = oldObjects[objectIndex];
                        const newMessage = newMessages[messageIndex];
                        newMessage.forwardId = null
                    }
                }

                if (oldRealm.schemaVersion < 8) {
                    const oldObjects = oldRealm.objects('Room');
                    const newObjects = newRealm.objects('Room');
                    // loop through all objects and set the fullName property in the new schema
                    for (const index in oldObjects) {

                        const newObject = newObjects[index];
                        newObject.muteUserList = []
                    }
                }

                if (oldRealm.schemaVersion < 9) {
                    const oldMessages = oldRealm.objects('Message');
                    const newMessages = newRealm.objects('Message');
                    // loop through all objects and set the fullName property in the new schema
                    for (const messageIndex in oldMessages) {
                        // const oldObject = oldObjects[objectIndex];
                        const newMessage = newMessages[messageIndex];
                        newMessage.contentObjects = null
                    }
                }

            }
        })
        return realm
    }
}

export function closeRealm(){

    if(realm){
        realm.close()
        realm = null
    }
}
