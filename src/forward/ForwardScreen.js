import React from 'react';
import {
    ActivityIndicator,
    FlatList,
    Image,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from '@react-native-community/async-storage';

import colors from '../shared/color';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import {ProfileImage} from '../common/ProfileImage';
import Button from '../components/Button';
import getRealm from '../data/Realm';
import {RoomUtil} from '../room/RoomUtil';
import {MessageTypes} from '../data/MessageAdapter';


let statusBarHeight = getStatusBarHeight(true);

export default class ForwardScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            contacts: [],
            keyword: '',
            searchPhone: '',
            searchInProgress: false,
            searchResult: undefined,
            selectedUsers: [],
            dataVersion: 0,
            exceptUsers: this.props.route.params ? this.props.route.params.exceptUsers : undefined,
            rooms: [],
            sentIds: [],
        };

        this.rowRefs = new Map();
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: 'Илгээх',
        })

        this.initData()
    }

    getRooms = async () => {

            let realm = await getRealm()
            let rooms = realm.objects('Room').sorted('lastDate',true)

            return rooms
    }


    initData = async () => {

        let userId = await AsyncStorage.getItem('userId')

        let rooms = await this.getRooms()

        this.setState({
            rooms: rooms,
            isLoading: rooms.length == 0,
            userId: userId,
        });

    }


    filter = (contacts) => {

        if(this.state.exceptUsers){

            let filtered = contacts.filter((contact) => {

                for(let user of this.state.exceptUsers){

                    if(user == contact.userId){
                        return false
                    }
                }

                return true

            })
            this.setState({
                contacts: filtered,
            });

        }else{

            this.setState({
                contacts: contacts,
            });
        }

    }


    send = (roomId,userId) => {

        let id = roomId ?? userId

        let sentIds = this.state.sentIds
        sentIds.push(id)

        if(roomId){

            let message = this.props.route.params.message

            this.props.route.params.messageAdapter.add({
                forwardId: message.id,
                content: message.content,
                type: message.type,
                new: true,
            },true,roomId)


            this.setState({sentIds: sentIds});
        }else{

            this.setState({isLoading: true});

            this.createRoom([userId]).then( room =>{

                let message = this.props.route.params.message

                this.props.route.params.messageAdapter.add({
                    forwardId: message.id,
                    content: message.content,
                    type: message.type,
                    new: true,
                },true,room.id)

                this.setState({
                    isLoading: false,
                    sentIds: sentIds
                });


            }).catch(e=>{

                CustomAlert.alert("Алдаа гарлаа!",e.message, 'error');
                this.setState({isLoading: false});
            })
        }

    }

    createRoom = (userIds) => {

       return  new Promise((resolve, reject) => {

           userIds.push(this.state.userId)

           let data = {
               userIdList: userIds,
               userId: this.state.userId
           }

           fetch(Constants.WSURL + "wsock/room", {
               method: 'POST',
               headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'
               },
               body: JSON.stringify(data)
           }).then((response) => response.json())
               .then((json) => {

                   console.log('room json',json)

                   resolve(json.entity)

               }).catch((error) => {

                   reject(error)

           });

        })
    }

    isSent = (id) => {

        return this.state.sentIds.includes(id)
    }

    renderContact = ({ item, isSearchResult }) => {

        return (
            <View
                ref={ref => this.rowRefs.set(item.userId, ref)}
                style={{ height: 67 }}>
                <View
                    style={{
                        backgroundColor: colors.bgGray,
                        paddingTop: 8,
                        paddingBottom: 8,
                        paddingHorizontal: 16,
                        height: 72,
                    }}
                >
                    <View
                        style={{ flex: 1, flexDirection: 'row' }}
                    >
                        <ProfileImage url = {item.imageUrl} />

                        <View style={{ flex: 1 , marginLeft: 8, flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View>
                                {item.contactName ?
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            lineHeight: 24,
                                            fontFamily: 'RobotoCondensed-Regular',
                                            color: '#000',
                                            fontSize: 16,
                                        }}>
                                        {item.contactName}
                                    </Text>
                                    :
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontFamily: 'RobotoCondensed-Regular',
                                            fontSize: 16,
                                            lineHeight: 24,
                                            color: '#e3e3e3',
                                        }}>Хоосон</Text>
                                }
                                <Text
                                    style={{
                                        fontFamily: 'Roboto-Regular',
                                        fontSize: 12,
                                        lineHeight: 18,
                                        color: colors.mutedText,
                                    }}>
                                    {item.phone}
                                </Text>
                            </View>
                            {item.systemName == "chat" ? <Button title = {this.isSent(item.userId) ? "Илгээсэн" : "Илгээх"} style = {styles.inviteButton} disabled = {this.isSent(item.userId)}  hideGradient onPress={() => this.send(null,item.userId)} /> : null}
                        </View>
                    </View>
                </View>
            </View>
        );
    };

    renderRoom = ({item}) => {

        let roomUtil = new RoomUtil(this.state.userId)

        return (

                <View
                    style={{
                        backgroundColor: colors.bgGray,
                        paddingTop: 8,
                        paddingBottom: 8,
                        paddingHorizontal: 16,
                        height: 72,
                    }}
                >
                    <View
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}
                    >
                        <ProfileImage user = {roomUtil.roomImage(item)} disabled />

                        <View style={{ flex: 1 , marginLeft: 8, flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style = {{justifyContent: 'center',}}>
                                <Text numberOfLines={1}
                                      style={{
                                        lineHeight: 24,
                                        fontFamily: 'RobotoCondensed-Regular',
                                        color: '#000',
                                        fontSize: 16,
                                    }}>
                                    {roomUtil.roomName(item)}
                                </Text>
                            </View>
                            <Button title = {this.isSent(item.id) ? "Илгээсэн" : "Илгээх"}  style = {styles.inviteButton} disabled = {this.isSent(item.id)}  hideGradient onPress={() => this.send(item.id,null)} />
                        </View>
                    </View>
                </View>
        );
    };

    search = async keyword => {
        try {
            const phone = keyword.replace(/[^0-9]/, '');
            if (phone === this.state.searchPhone) {
                return;
            }

            if (this.searchAbortController) {
                this.searchAbortController.abort();
            }

            if (phone.length !== 8) {
                this.setState({
                    searchPhone: phone,
                    searchInProgress: false,
                    searchResult: undefined,
                });
                return;
            }

            this.setState({
                searchPhone: phone,
                searchInProgress: true,
                searchResult: undefined,
            });

            this.searchAbortController = new AbortController();

            const response = await fetch(Constants.URL + 'redpointapi/user/findByPhone', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': await AsyncStorage.getItem('token'),
                },
                body: JSON.stringify({
                    phone,
                }),
                signal: this.searchAbortController.signal,
            });

            const responseBody = await response.json();

            if (responseBody.status === 'SUCCESS' ||
                (Array.isArray(responseBody.errors) && responseBody.errors[0].code === 2030)) {
                this.setState({
                    searchInProgress: false,
                });

                this.filterSearchResult(responseBody.data)

            } else {
                this.setState({
                    searchInProgress: false,
                    searchResult: undefined,
                });

                console.log('find phone error',responseBody.errors)

                CustomAlert.alert('Алдаа гарлаа!', responseBody.errors, 'error');
            }
        } catch (error) {

            if (error.name !== 'AbortError') {

                CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');

                this.setState({
                    searchInProgress: false,
                    searchResult: undefined,
                });
            }
        }
    };

    filterSearchResult = (searchResult) => {

        if(searchResult && this.state.exceptUsers){

            console.log('exceptUsers',this.state.exceptUsers)

            for(let user of this.state.exceptUsers){

                if(user == searchResult.userId){

                    this.setState({
                        searchResult: undefined,
                    });
                    return
                }
            }

            this.setState({
                searchResult: searchResult,
            });

        }else{

            this.setState({
                searchResult: searchResult,
            });
        }

    }

    filterRooms = (room) => {

        let keyword = this.state.keyword

        let result = false

        room.userList.map(user=>{
            if((user.name && user.name.toLowerCase().includes(keyword.toLowerCase())) ||
            (user.lastName && user.lastName.toLowerCase().includes(keyword.toLowerCase())) ||
            (user.phone && user.phone.includes(keyword))){
                result = true
            }
        })

        return result
    }


    render() {
        const { isLoading, keyword, searchPhone, searchInProgress, searchResult, rooms } = this.state;

        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? 'padding' : null}
                                      keyboardVerticalOffset={(Platform.OS === 'ios') ? 44 + statusBarHeight : 0}>

                    <View style={styles.topContainer}>
                        <View
                            style={{
                                backgroundColor: '#f8f9fb',
                                height: 40,
                                borderRadius: 8,
                                flexDirection: 'row',
                            }}
                        >
                            <TextInput
                                value={keyword}
                                selectionColor = "#000"
                                onChangeText={text => {
                                    this.setState({
                                        keyword: text,
                                    });
                                    this.search(text);
                                }}
                                style={{
                                    fontFamily: 'Roboto-Regular',
                                    fontSize: 16,
                                    height: 40,
                                    padding: 0,
                                    paddingLeft: 20,
                                    flex: 1,
                                }}
                                placeholder="Хайх утас болон нэр оруулна уу"
                                placeholderTextColor={colors.placeholderText}
                            />
                            <TouchableOpacity
                                style={{
                                    width: 36,
                                    height: 40,
                                    paddingLeft: 8,
                                    justifyContent: 'center',
                                }}
                            >
                                <Image source={require('../image/search.png')} resizeMode="contain" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {searchPhone.length === 8 && (
                        <React.Fragment>
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Light',
                                    fontSize: 14,
                                    color: colors.mutedText,
                                    padding: 15,
                                    paddingBottom: 5,
                                }}
                            >
                                Бүх хэрэглэгчдээс хайсан илэрц:
                            </Text>
                            {searchInProgress ? (
                                <View style={{ height: 64, justifyContent: 'center' }}>
                                    <ActivityIndicator />
                                </View>
                            ) : (
                                searchResult != null ? (
                                    this.renderContact({ item: searchResult, isSearchResult: true })
                                ) : (
                                    <Text
                                        style={{
                                            fontFamily: 'Roboto-Regular',
                                            fontSize: 16,
                                            color: colors.mutedText,
                                            paddingVertical: 16,
                                            textAlign: 'center',
                                        }}
                                    >
                                        Илэрц олдсонгүй.
                                    </Text>
                                )
                            )}
                        </React.Fragment>
                    )}
                    <FlatList
                        keyboardShouldPersistTaps = 'handled'
                        data={rooms.filter(this.filterRooms)}
                        refreshing={isLoading}
                        keyExtractor={item => item.userId}
                        extraData = {this.state.dataVersion}
                        renderItem={this.renderRoom}
                        ListHeaderComponent={
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Light',
                                    fontSize: 14,
                                    color: colors.mutedText,
                                    padding: 15,
                                    paddingBottom: 5,
                                }}>
                                Сүүлд бичсэн:
                            </Text>
                        }
                        ListEmptyComponent={
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Regular',
                                    fontSize: 16,
                                    color: colors.mutedText,
                                    paddingVertical: 16,
                                    textAlign: 'center'
                                }}
                            >
                                {rooms.length > 0 ? 'Илэрц олдсонгүй.' : '...'}
                            </Text>
                        }
                        style={{ flex: 1 }}
                    />

                </KeyboardAvoidingView>
                <ActivityIndicatorView animating={this.state.isLoading} light />

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bgGray,
    },

    topContainer: {
        backgroundColor: 'white',
        padding: 8,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.headerLineColor,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },

    inviteButton:{
        height: 36,
        borderRadius: 4,
        backgroundColor: '#FA7268'
    },
});

