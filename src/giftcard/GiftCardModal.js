import React, {forwardRef} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import { StyleSheet } from "react-native";
import Button from '../components/Button';
import Input from '../components/Input';
import {bottomModal} from '../components/BottomModal';
import CounterInput from '../components/CounterInput';
import Util from '../common/Util';

function GiftCardModal(props,ref) {

    const cancel = () => {
        bottomModal.close()
    }

    function onContinue() {

        if(count){

            // props.onContinue(Util.formatCurrency(count * 100000))
            props.onContinue(String(count * 100000))
            bottomModal.close()
        }

    }

    const [count,setCount] = React.useState(0)

    return (<View style={styles.container}>
        <Text style={styles.title} >{props.title ? props.title : 'Үнийн дүн оруулах'}.</Text>
        {(props.text) ? (<Text style={styles.message} >{props.text}</Text>): 'Худалдан авалтын үнийн дүнгээс хамаарч бэлгийн картын үнийн дүнг сонгоно уу!'}

        <View style = {{flexDirection: 'row'}} >
            <View style = {styles.countContainer} >
                <Text style = {styles.amount} >100,000₮</Text>
            </View>
            <CounterInput style = {{flex:1}} value = {count} onChange = {(count)=>setCount(count)} />
        </View>


        <View style = {{flexDirection: 'row',marginTop: 24}} >
            <Button style ={{flex: 1, marginRight: 8}} title = "Болих" hideGradient second onPress = {cancel} />
            <Button style ={{flex: 1, marginLeft: 8}} title = "Үргэлжлүүлэх" onPress = {onContinue} />
        </View>
    </View>);
}

export default GiftCardModal = forwardRef(GiftCardModal);

const styles = StyleSheet.create({


    container: {
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 14,
        marginBottom: 16,
    },

    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 8,
        marginLeft: 0,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: '#666666',
        marginLeft: 8,
    },

    countContainer: {
        flex:1,
        borderRadius: 4,
        backgroundColor: '#E4EAF1',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 16,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#999'
    },

    count: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#000',
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    }
});
