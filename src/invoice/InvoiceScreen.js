import React from 'react';
import {
    BackHandler,
    Dimensions,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Dash from 'react-native-dash';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Input from '../components/Input';
import Button from '../components/Button';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import Util from '../common/Util';
import UserUtil from '../common/UserUtil';
import {createNavigationOptions} from '../common/NavigationOption';

const statusBarHeight = getStatusBarHeight(true);

const {height, width} = Dimensions.get('window');


class InvoiceScreen extends React.Component {

    static navigationOptions = ({navigation}) => {
        return {
            ...createNavigationOptions(navigation, {
                hasBackButton: true,
                onBackButtonPress: navigation.getParam('close'),
            }),
            title: 'Нэхэмжлэх',
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            invoice: this.props.navigation.getParam('invoice'),
            payingPoint: 0,
            payingPointText: '0',
            payingCard: 0,
            userPoints: 0,
        };

        this.props.navigation.setParams({close: this.close});
    }

    componentDidMount() {

        this.props.navigation.setOptions({

            title: 'Нэхэмжлэх',
        })

        Util.getLocation();
        this.updateInvoice();

        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
        );
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    componentDidUpdate(prevProps, prevState) {
        const prevInvoice = prevProps.navigation.getParam('invoice');
        const currentInvoice = this.props.navigation.getParam('invoice');
        if (prevInvoice != null && prevInvoice !== currentInvoice) {
            this.cancelInvoice()
                .finally(() => {
                    this.setState({invoice: currentInvoice}, () => {
                        this.updateInvoice();
                    });
                });
        }
    }

    backAction = () => {
        this.close(false);
        return true;
    }

    updateInvoice = async () => {
        this.setState({isLoading: true});

        if (isInvoiceLoaded(this.state.invoice)) {
            this.calculateAmounts();
        } else {
            fetch(Constants.URL + 'redpointapi/transaction/startInvoice', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': await AsyncStorage.getItem('token'),
                },
                body: JSON.stringify({
                    invoiceId: this.state.invoice.invoiceId,
                }),
            })
                .then(response => response.json())
                .then(json => {
                    if (json.status === 'SUCCESS') {
                        this.setState({invoice: json.data}, () => {
                            this.calculateAmounts();
                        });
                    } else {
                        this.setState({isLoading: false});

                        CustomAlert.alert('Алдаа гарлаа!', json.errors, 'error');

                        this.close(false);
                    }
                })
                .catch(error => {
                    this.setState({isLoading: false});

                    CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');

                    this.close(false);
                });
        }
    }

    calculateAmounts = () => {
        const {invoice} = this.state;
        UserUtil.updateUserData().then(data => {
            if (invoice.redpointFlag) {
                if (invoice.amount > data.redPoint) {
                    CustomAlert.confirm(
                        'Төлбөрийн карт ашиглах уу?',
                        'Таны RedPoint хүрэхгүй байгаа тул үлдэгдэл төлбөрийг төлбөрийн картаар төлөх үү?',
                        (response) => {
                            this.setState({isLoading: false});

                            if (response) {
                                this.setState({
                                    payingPoint: data.redPoint,
                                    payingPointText: data.redPoint.toString(),
                                    payingCard: invoice.amount - data.redPoint,
                                    userPoints: data.redPoint,
                                });
                            } else {
                                this.close(false);
                            }
                        },
                    );
                } else {
                    this.setState({isLoading: false});

                    const integerAmount = Math.ceil(invoice.amount);
                    this.setState({
                        payingPoint: integerAmount,
                        payingPointText: integerAmount.toString(),
                        payingCard: 0,
                        userPoints: data.redPoint,
                    });
                }
            } else {
                CustomAlert.confirm(
                    'Төлбөрийн карт ашиглах уу?',
                    'Та төлбөрийн карт ашиглан энэ гүйлгээг төлөхдөө итгэлтэй байна уу?',
                    (response) => {
                        this.setState({isLoading: false});

                        if (response) {
                            this.setState({
                                payingPoint: 0,
                                payingPointText: '0',
                                payingCard: invoice.amount,
                                userPoints: data.redPoint,
                            });
                        } else {
                            this.close(false);
                        }
                    },
                );
            }
        });
    };

    close = (isPaid = false, surveyId = null) => {
        const {invoice} = this.state;
        const doClose = () => {
            const onClose = this.props.navigation.getParam('onClose');

            this.props.navigation.dismiss();

            if (onClose) {
                onClose({isPaid, invoice, surveyId});
            }
        };

        if (isPaid) {
            doClose();
        } else {
            this.cancelInvoice().finally(doClose);
        }
    };

    pay = () => {
        if (!isInvoiceLoaded(this.state.invoice) ||
            (this.state.payingPoint !== 0 && this.state.payingPoint < this.state.invoice.minPoint)) {
            return;
        }

        const password = this.refs.password.validate();
        if (!password) {
            return;
        }

        const invoice = this.state.invoice;

        this.setState({isLoading: true});

        if (invoice.amount <= this.state.payingPoint) {
            this.sendData({
                invoiceId: invoice.invoiceId,
                password: password,
                ...(invoice.redpointFlag ? {
                    redeemPoint: this.state.payingPoint
                } : null)
            });
        } else {
            const doNavigate = async () => {
                const password = this.refs.password.validate();
                if (!password) {
                    return;
                }

                this.setState({isLoading: true});

                fetch(Constants.URL + 'redpointapi/user/checkPassword', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': await AsyncStorage.getItem('token'),
                    },
                    body: JSON.stringify({password}),
                })
                    .then(response => response.json())
                    .then(json => {
                        this.setState({isLoading: false});

                        if (json.status === 'SUCCESS') {
                            if (json.data.passwordConfirmed) {
                                this.props.navigation.navigate('ChooseCard', {
                                    invoice: {
                                        ...invoice,
                                        redeemPoint: this.state.payingPoint,
                                    },
                                    password: password,
                                    closeStack: this.close
                                });
                            } else {
                                CustomAlert.alert('Амжилтгүй!', json.message, 'success');
                            }
                        } else {
                            CustomAlert.alert('Алдаа гарлаа!', json.errors, 'error');
                        }
                    })
                    .catch(error => {
                        this.setState({isLoading: false});

                        CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');
                    });
            };

            if (invoice.amount < this.state.userPoints && invoice.amount > this.state.payingPoint) {
                CustomAlert.confirm(
                    'Төлбөрийн карт ашиглах уу?',
                    'Таны оруулсан RedPoint нийт дүнд хүрэхгүй байгаа тул үлдэгдэл төлбөрийг төлбөрийн картаар төлөх үү?',
                    (response) => {
                        if (response) {
                            doNavigate();
                        }
                        this.setState({isLoading: false});
                    },
                );
            } else {
                doNavigate();
                this.setState({isLoading: false});
            }
        }
    };

    sendData = async data => {
        this.setState({isLoading: true});

        let formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('log', JSON.stringify(await Util.deviceInfo()));

        fetch(Constants.URL + 'redpointapi/transaction/payInvoice', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization': await AsyncStorage.getItem('token'),
            },
            body: formData,
        })
            .then(response => response.json())
            .then(json => {
                this.setState({isLoading: false});

                if (json.status === 'SUCCESS') {
                    CustomAlert.alert('Амжилттай', 'Гүйлгээ амжилттай.', 'success');

                    this.close(true, json.data.surveyId);
                } else {
                    CustomAlert.alert('Алдаа гарлаа!', json.errors, 'error');
                }
            })
            .catch(error => {
                this.setState({isLoading: false});

                CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');
            });
    };

    cancelInvoice = async () => {
        try {
            this.setState({isLoading: true});

            const response = await fetch(Constants.URL + "redpointapi/transaction/cancelInvoice", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': await AsyncStorage.getItem("token"),
                },
                body: JSON.stringify({
                    invoiceId: this.state.invoice ? this.state.invoice.invoiceId :
                        this.props.navigation.getParam('invoiceId')
                })
            });
            const responseBody = await response.json();

            this.setState({isLoading: false});

            if (responseBody.status !== 'SUCCESS') {
                CustomAlert.alert('Алдаа гарлаа!', json.errors, 'error');
            }
        } catch (error) {
            this.setState({isLoading: false});

            CustomAlert.alert('Алдаа гарлаа!', json.message, 'error');

            throw error;
        }
    };

    renderInvoiceDetails() {
        const {invoice} = this.state;

        return (
            <View>
                {invoice.merchant && (
                    <React.Fragment>
                        <Text style={styles.title}>{invoice.merchant}</Text>
                        <Text style={styles.text}>{invoice.description}</Text>
                    </React.Fragment>
                )}

                <Text style={styles.title}>Нийт төлбөр</Text>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={styles.text}>{Util.formatPoint(invoice.amount)}</Text>
                </View>

                <Dash style={styles.border} dashColor={'#D8D8D8'} dashThickness={1}/>

                {invoice.redpointFlag && (
                    <React.Fragment>
                        <Text style={styles.title}>Хасагдах RedPoint</Text>
                        <Text
                            style={{
                                color: '#999999',
                                fontSize: 12,
                                fontFamily: 'Roboto-Regular',
                                lineHeight: 16,
                                marginTop: 8,
                            }}>
                            Та уг худалдан авалтад оноогоо ашиглахгүй бол 0 гэсэн дүнг оруулна уу:
                        </Text>
                        <View>
                            <TextInput
                                keyboardType="number-pad"
                                returnKeyType={'done'}
                                onSubmitEditing={() => this.refs.password.focus()}
                                value={this.state.payingPointText}
                                onChangeText={text => {
                                    const filteredText = text.replace(/^0+(?=\d)|[^0-9]/g, '');
                                    if (filteredText === '') {
                                        this.setState({
                                            payingPoint: 0,
                                            payingPointText: '',
                                            payingCard: invoice.amount,
                                        });
                                    } else {
                                        const amount = parseInt(filteredText, 10);
                                        let payingPoint = Math.min(
                                            amount,
                                            Math.ceil(invoice.amount),
                                            this.state.userPoints
                                        );
                                        this.setState({
                                            payingPoint,
                                            payingPointText: payingPoint.toString(),
                                            payingCard: invoice.amount - payingPoint,
                                        });
                                    }
                                }}
                                onEndEditing={() => {
                                    if (this.state.payingPointText === '') {
                                        this.setState({
                                            payingPointText: this.state.payingPoint.toString(),
                                        });
                                    }
                                }}
                                style={{
                                    fontFamily: 'Roboto-Regular',
                                    fontSize: 16,
                                    lineHeight: 24,
                                    color: 'black',
                                    paddingVertical: 4,
                                    paddingHorizontal: 8,
                                    marginTop: 4,
                                    borderColor: '#e0e0e0',
                                    borderWidth: 1,
                                    borderRadius: 4,
                                    backgroundColor: '#f8f9fb',
                                }}
                            />
                            <View
                                pointerEvents="none"
                                style={{
                                    position: 'absolute',
                                    top: 10,
                                    right: 10,
                                }}
                            >
                                <Icon color="#6060a0" name="edit" size={20}/>
                            </View>
                            {this.state.payingPoint !== 0 && this.state.payingPoint < invoice.minPoint && (
                                <Text style={styles.error}>Та хамгийн багадаа 100 оноо ашиглах боломжтой</Text>
                            )}
                        </View>
                    </React.Fragment>
                )}

                <Text style={styles.title}>Төлбөрийн картнаас хасагдах мөнгөн дүн</Text>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={styles.text}>{Util.formatPoint(this.state.payingCard)} ₮</Text>
                </View>
            </View>
        );
    }

    render() {
        const {invoice} = this.state;
        return (
            <SafeAreaView style={{flex: 1}}>
                <KeyboardAvoidingView style={{flex: 1}} behavior={(Platform.OS === 'ios') ? 'padding' : null}
                                      keyboardVerticalOffset={(Platform.OS === 'ios') ? 88 + statusBarHeight : 0}>
                    <ScrollView
                        style={{flex: 1}}
                        contentContainerStyle={{flexGrow: 1, padding: 16, justifyContent: 'space-between'}}
                    >
                        {isInvoiceLoaded(invoice) ? (
                            this.renderInvoiceDetails()
                        ) : (
                            <View/>
                        )}
                        <View style={{marginTop: 24}}>
                            <Input ref="password" label="Нууц үг" required hideSecureText biometric
                                   returnKeyType={'done'} secureTextEntry={true} onSubmitEditing={this.pay}/>
                            <View style={styles.border}/>
                            <Button
                                title="Төлөх"
                                onPress={this.pay}
                                disabled={
                                    !isInvoiceLoaded(invoice) ||
                                    (this.state.payingPoint !== 0 && this.state.payingPoint < invoice.minPoint)
                                }
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
                <ActivityIndicatorView animating={this.state.isLoading} title={'Түр хүлээнэ үү'}/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#333333',
        fontSize: 20,
        marginTop: 16,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        color: '#333333',
        fontSize: 14,
        marginTop: 4,
    },

    border: {
        marginTop: 16,
    },

    favicon: {
        height: 12,
        width: 12,
        marginLeft: 8,
        marginTop: 4,
    },

    error:{
        color: '#eb5757',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        marginTop: 4,
    },
});

export default InvoiceScreen;
