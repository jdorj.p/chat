import React, { Component } from "react";
import {View, Text, TouchableOpacity , StyleSheet} from 'react-native';
import {ProfileImage} from '../common/ProfileImage';
import DateUtil from '../common/DateUtil';
import ChatUtil from '../chat/ChatUtil';
import Border from '../components/Border';

export class ContractListItem extends Component {

    constructor(props){
        super(props);

        this.state = {
        }
    }

    componentDidMount() {

    }

    render() {

        let item = this.props.item


        return (
            <TouchableOpacity style = {styles.container} onPress = {this.props.onPress} >
                <View style = {styles.row}>
                    <View style = {{flex: 1, marginLeft: 8, marginRight: 8, justifyContent: 'center'}} >
                        <Text style = {[styles.text]} >
                            {item.userList[0].name}
                        </Text>
                        <View style = {{flexDirection: 'row', marginTop: 4}} >
                            <Text style = {[styles.text]} numberOfLines={1} >{item.name}</Text>
                            <Text style = {[styles.date]} >• {DateUtil.format(item.createdDate)}</Text>
                        </View>
                    </View>
                </View>
                <Border style = {{marginLeft: 8}} />
            </TouchableOpacity>
        );
    }

}


const styles = StyleSheet.create({

    container: {

    },

    row: {
        flexDirection: 'row',
        padding: 8,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
        flex:1,
    },

    date: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
    },


});
