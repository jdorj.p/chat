import React, { Component } from "react";
import { FlatList , StyleSheet, SafeAreaView} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
let statusBarHeight = getStatusBarHeight(true);
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import {ContractListItem} from './ContractListItem';

export default class ContractListScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            data: [],
        }
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: 'Хэлцэлүүд',
        })

        this.getData()
    }

    getData = async () => {

        this.setState({isLoading: true});

        let userId = await AsyncStorage.getItem('userId')
        let url = Constants.WSURL + "wsock/chat/contractListByUserId?userId=" + userId
        let withUserId = this.props.route.params.withUserId

        if(withUserId){
            url += '&withUserId=' + withUserId
        }

        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {

                // console.log(json)

                if(json.status == "000") {

                    this.setState({data: json.entity})

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                }


                this.setState({isLoading: false})

            }).catch((error) => {

            console.log(error)

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false});
        });
    }

    render() {

        return (
            <SafeAreaView style = {styles.safeAreaView} >
                <FlatList
                    style = {styles.flatList}
                    data={this.state.data}
                    renderItem={({item, index}) =>(

                        <ContractListItem
                            key = {index}
                            item = {item}
                            onPress = {()=>{
                                this.props.navigation.navigate('Contract',{contract:item,
                                })
                            }}
                        />)}
                />
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}


const styles = StyleSheet.create({

    safeAreaView:{
        flex:1,
        backgroundColor: '#fff',
    },

    container: {
        flex:1,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 22,
        textAlign: 'center',
        marginLeft: 8,
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
        borderRadius: 18,
        minHeight: 36,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        // borderRadius: 18,
        margin: 8,
        marginRight: 0,
        // backgroundColor: '#4CA2EB',
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        borderRadius: 18,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },

    imageBackground: {
        flex: 1,
        margin: 16,
    },

    scrollView:{
        flex:1,
    },

    text: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
    },

    textBottom: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
        textAlign: 'center',
    },

    exitButton: {
        width: 44,
        height: 44,
        position: 'absolute',
        right: 8,
        top: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },

    exitImage: {
        width: 34,
        height: 34,
    },

});
