import React, { Component } from "react";
import { FlatList , StyleSheet, SafeAreaView, KeyboardAvoidingView} from 'react-native';
import {ChatItem} from '../chat/ChatItem';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from '@react-native-community/async-storage';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import {RoomUtil} from '../room/RoomUtil';
let statusBarHeight = getStatusBarHeight(true);

export default class ContractScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            room: props.route.params.contract,
            messages: [],
            dataVersion: 0,
            isLoading: false,
        }

    }


    componentDidMount() {

        AsyncStorage.getItem("userId").then((userId)=>{
            this.setState({
                userId: userId,
                messages: this.props.route.params.contract.chatList,
                dataVersion: this.state.dataVersion + 1,
            })
        })

        this.setTitle()

    }

    componentWillUnmount(): void {

      }

    setTitle = async () => {

        let room = this.state.room

        if(room){

            let userId = await AsyncStorage.getItem("userId")
            let roomUtil = new RoomUtil(userId)

            this.props.navigation.setOptions({
                title: roomUtil.roomName(room),
                headerBackTitle: " ",
            })
        }else{

            this.props.navigation.setOptions({
                title: "Хэлцэл",
                headerBackTitle: " ",
            })

        }

    }


    getNextItem = (currentIndex) => {

        if((currentIndex + 1) < this.state.messages.length){
            // if((currentIndex + 1) >= 0){
            return this.state.messages[currentIndex + 1]
        }

        return undefined
    }


    render() {

        return (
            <SafeAreaView style = {{flex:1}} >
                <FlatList
                    style = {styles.flatList}
                    inverted
                    extraData = {this.state.dataVersion}
                    data={this.state.messages}
                    renderItem={({item, index}) =>{

                        return (<ChatItem
                            key = {item.id}
                            userId={this.state.userId}
                            nextItem = {this.getNextItem(index)}
                            item = {item}
                            contract
                        />)
                    }}
                />
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}

const styles = StyleSheet.create({

    container: {
        flex:1,
    },

    statusContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        padding: 8,
        alignItems: 'center',
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
        alignItems: 'flex-end'
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        margin: 8,
        marginRight: 0,
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },


});
