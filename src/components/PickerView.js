import React, {forwardRef} from 'react';
import {View, Text, Picker} from 'react-native';
import { StyleSheet } from "react-native";
import Border from './Border';

function PickerView (props,ref) {

    // const [options, setOptions] = React.useState(props.options)
    const [selected, setSelected] = React.useState()
    const [error, setError] = React.useState()

    React.useImperativeHandle(ref, () => ({
        getValue: getValue,
        validate: validate,
    }));


    const getValue = () => {

        return selected
    }

    const validate = () => {

        if(props.required && !selected) {

            if(props.requiredText) {
                setError(props.requiredText)
            }else{
                setError(`${props.label} оруулна уу!`)
            }

            return false
        }

        setError(null)

        return getValue()
    }


    let style = {}

    if(Platform.OS == "ios"){
        style.height = 100
    }

    console.log('options',props.options)

    return (
        <View style = {style} >
            {(selected) ? (<Text style = {styles.label} >{props.label}</Text>) : null}
            <View style = {{flexDirection: 'row',overflow: 'hidden'}}>
                {(selected) ? null : (<Text style = {styles.placeholder} >{props.label}</Text>)}
                <Picker
                    style = {{flex:1}}
                    selectedValue={selected}
                    onValueChange={(itemValue, itemIndex) =>
                        setSelected(itemValue)
                    }>
                    {props.options.map((option,i)=>{ return (<Picker.Item key = {i} label = {option.label} value = {option.value} />) })}
                </Picker>
            </View>

            { (error) ? (<Text style = {styles.error} >{error}</Text>) : null }
            {(props.hideBorder) ? null : (<Border />)}
        </View>

    );
}

export default PickerView = forwardRef(PickerView);

const styles = StyleSheet.create({

    label: {
        color: '#999',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },

    placeholder: {
        color: '#999',
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        minHeight: 24,
    },

    error:{
        color: '#EB5757',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        marginBottom: 8,
    },
});
