import React, { Component } from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';


export default class Border extends React.Component {

    render() {

        return (<View style = {[styles.border,this.props.style]}/>);

    }

}



const styles = StyleSheet.create({

    border: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#999'
    },
});
