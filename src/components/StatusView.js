import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import DateUtil from '../common/DateUtil';

export default class StatusView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: props.user
        };
    }



    render() {

        let user = this.props.user

        return (
            <View style={styles.container}>
                {(user.status == 'Online') ? (<View style = {styles.dot}/>) : (user.lastOnlineDate ? <Text style = {styles.text}>{DateUtil.ago(user.lastOnlineDate)}</Text>: null)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 10,
        borderColor: '#fff',
        borderWidth: 2,
        overflow: 'hidden',
        position: 'absolute',
        bottom: -1,
        right: -1,
    },

    dot: {
        width: 12,
        height: 12,
        backgroundColor: '#79DB16'
    },

    text: {
        color: '#666666',
        backgroundColor: '#D9FFB3',
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 10,
        paddingLeft: 4,
        paddingRight: 4,
    },
});
