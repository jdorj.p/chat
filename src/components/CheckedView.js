import React, { Component } from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";


export default class CheckedView extends React.Component {

    render() {

        return (<View style = {[styles.container,this.props.style]}>
            <Icon
                name = "check"
                color = "#fff"
                size = {12}
            />
        </View>);

    }

}



const styles = StyleSheet.create({

    container: {
        height: 16,
        width: 16,
        borderRadius: 8,
        backgroundColor: '#FA7268',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 0,
        bottom: 8,
    },
});
