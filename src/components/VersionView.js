import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import VersionNumber from 'react-native-version-number';
import CodePush from "react-native-code-push";

export default class VersionView extends React.Component {

    constructor() {
        super();

        this.state = {};
    }

    componentDidMount() {

        CodePush.getUpdateMetadata().then((updateMetadata) =>{

            console.log(updateMetadata);

            this.setState({updateMetadata});

        });

        console.log(VersionNumber)
    }


    render() {

        let jsVersion = (this.state.updateMetadata) ? this.state.updateMetadata.label.replace('v','.') : ''
        let version = VersionNumber.appVersion + jsVersion

        return (<View style = {styles.container} >
                  <Text style = {styles.text} >v{version}</Text>
                </View>);
    }

}


const styles = StyleSheet.create({

    container:{
        margin: 16,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#666666'
    },

});
