import React, {forwardRef} from 'react';
import {View, Text, FlatList} from 'react-native';
import { StyleSheet } from "react-native";

import {bottomModal} from '../components/BottomModal';
import ListButton from './ListButton';


function SelectModal(props, ref) {

    const select = (item) => {
        props.onSelect(item)
        bottomModal.close()
    }

    return (<View style={styles.container}>
        <Text style={styles.title} >{props.label}</Text>
        <FlatList
            style = {styles.flatList}
            data = {props.options}
            renderItem={({item, index}) =>(
                <ListButton
                    key = {index}
                    text = {item.label}
                    onPress = {()=>{
                        select(item)
                    }}
                />)

            }
        />
    </View>);
}

export default SelectModal = forwardRef(SelectModal);

const styles = StyleSheet.create({

    container: {
        height: 400,
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

});
