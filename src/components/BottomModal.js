import React, {forwardRef} from 'react';
import {
    Animated,
    Dimensions,
    StyleSheet,
    View,
    ScrollView,
    Platform,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    BackHandler
} from 'react-native';
import Border from './Border';
import {SafeAreaView} from 'react-native-safe-area-context';
const screen = Dimensions.get("screen");

export let bottomModal
export function setBottomModal(ref){
    bottomModal = ref
}

function BottomModal(props,ref) {

    const fadeBackground = React.useRef(new Animated.Value(0)).current
    const footerSlide = React.useRef(new Animated.ValueXY({x: 0, y: screen.height})).current
    const slide = React.useRef(new Animated.ValueXY({x: 0, y: screen.height})).current

    const [isVisible,setIsVisible] = React.useState(false)
    const [content,setContent] = React.useState(null)


    React.useImperativeHandle(ref, () => ({
        open: (content) => open(content),
        close: () => close(),
        show: () => show(),
        hide: () => hide(),
    }));

    React.useEffect(()=>{

        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            ()=>{
                close()
                return true
            }
        );

        return () => backHandler.remove();

    },[])

    const open = (content) => {
        setContent(content)
        setIsVisible(true)
        show()
    }

    const close = () => {
        setContent(null)
        hide()
        // setIsVisible(false)
    }

    const show = () => {
        Animated.parallel([
            Animated.timing(
                fadeBackground,
                {
                    toValue: 1,
                    duration: 200,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                slide,
                {
                    toValue: {x:0, y: 0 },
                    duration: 200,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                footerSlide,
                {
                    toValue: {x:0, y: screen.height - 64 },
                    duration: 200,
                    useNativeDriver: true
                }
            )
        ],{
            useNativeDriver: true
        }).start()
    }

    const hide = () => {

        Animated.parallel([
            Animated.timing(
                fadeBackground,
                {
                    toValue: 0,
                    duration: 200,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                slide,
                {
                    toValue: {x:0, y: screen.height },
                    duration: 200,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                footerSlide,
                {
                    toValue: {x:0, y: screen.height },
                    duration: 200,
                    useNativeDriver: true
                }
            )
        ],{
            useNativeDriver: true
        }).start(()=>{
            setIsVisible(false)
        });
    }

    if(!isVisible){
        return null
    }

    return (
        <View style = {styles.root}>
            <Animated.View style = {[styles.background,{opacity: fadeBackground}]} />
            {(Platform.OS == 'ios') ? (<Animated.View style = {[styles.footer,{transform :[{translateY: footerSlide.y}]}]} />) : null}
                <KeyboardAvoidingView
                    behavior= {(Platform.OS === 'ios') ? "padding" : null}
                    style = {{flex:1}}
                >
            <SafeAreaView style =  {{flex: 1}} >
                    <TouchableWithoutFeedback onPress = {close} >
                        <View style = {{flex:1}} />
                    </TouchableWithoutFeedback>
                    <Animated.View style = {[styles.slideView,{transform: [{ translateY: slide.y }]}]} >
                        <ScrollView
                            keyboardShouldPersistTaps = 'handled'
                            keyboardDismissMode = "interactive"
                            bounces = {false}
                        >
                            <View style = {styles.container} >
                                {content}
                            </View>
                        </ScrollView>
                        {/*<Border/>*/}
                    </Animated.View>
            </SafeAreaView>
                </KeyboardAvoidingView>
        </View>
    );
}

export default BottomModal = forwardRef(BottomModal);

const styles = StyleSheet.create({

    root: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    },


    background: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.6)',
    },

    footer:{
        height: 64,
        backgroundColor: '#fff',
    },

    slideView:{

    },


    container: {
        marginTop: 20,
        backgroundColor: '#fff',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        overflow: 'hidden',
        padding: 16,
    },

});
