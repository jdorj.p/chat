import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity,
} from 'react-native';

import Icon from "react-native-vector-icons/MaterialIcons";
import Cicon from "react-native-vector-icons/MaterialCommunityIcons";

export default class ListButton extends React.Component {


    constructor(props) {
        super(props);

        this.state = {text: this.props.text}
    }

    renderImage(){

        if(this.props.image == null){
            return null;
        }

        return (
            <Image
                style = {styles.image}
                source = {this.props.image}
                resizeMode={"center"}
            />);

    }


    renderIcon(){

        if(this.props.icon == null){
            return null;
        }

        return (
            <Icon
                style = {styles.icon}
                name = {this.props.icon}
                color = {this.props.color ?? "#000"}
                size = {20}
            />);

    }

    renderCicon(){

        if(this.props.cicon == null){
            return null;
        }

        return (
            <Cicon
                style = {styles.icon}
                name = {this.props.cicon}
                color = {this.props.color ?? "#000"}
                size = {20}
            />);

    }

    renderText(){

        if(this.props.text == null){
            return null;
        }


        return (
            <Text
                style={[styles.text,{color: this.props.color ?? styles.text.color}]}>
                {this.props.text}
            </Text>);

    }


    onPress = () => {

        this.props.onPress && this.props.onPress(this)
    }

    render() {

        return (<TouchableOpacity
            onPress={this.onPress}
            style={[styles.touchableOpacity,this.props.style]}
            disabled={this.props.disabled}
        >


            <View style = {styles.container}>
                {this.renderImage()}
                {this.renderIcon()}
                {this.renderCicon()}
                {this.renderText()}

                <View style = {{flexDirection: 'row', justifyContent: 'flex-end' ,paddingRight: 16}} >
                    {this.props.children}
                </View>
                {(this.props.hideChevron) ? null : (<Icon name  = "keyboard-arrow-right" size = {24} color = {this.props.color ?? "#000"} />)}

            </View>

            {(this.props.hideBorder) ? null : (<View style = {styles.border} />)}
        </TouchableOpacity>);

    }

}



const styles = StyleSheet.create({

    touchableOpacity: {
        minHeight: 56,
    },

    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingStart: 16,
        paddingEnd: 16,
    },

    icon: {
      marginRight: 16,
    },

    image: {
      marginRight: 16,
        width: 24,
        height: 24,
    },

    text: {
        flex:1,
        fontFamily: 'Roboto-Regular',
        color: '#333333',
        fontSize: 16,
        marginTop: 16,
        marginBottom: 16,
    },

    border: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: "#dedede",
        marginLeft: 16,
    },
});
