import React from 'react';

export default class Error {

   static message = (error) => {

       if(Array.isArray(error)){

           let messageArray = []


           for(let i in error){
               messageArray.push(error[i].message)
           }

           return messageArray.join(" ")

       }

       let message = (error.message) ? error.message : error

        switch (message) {
            case 'Network request failed':
               return "Холбогдож чадсангүй. Интернетээ шалгана уу."
               break
            case "JSON Parse error: Unrecognized token '<'":
                return "Холбогдож чадсангүй. Интернетээ шалгана уу."
                break
            default:
                return message

        }
    }



}
