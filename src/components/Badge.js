import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

export default function Badge({style,count}) {

    return <View style={[styles.border,style]}>
        <View style={styles.badge}>
            <Text
                numberOfLines={1}
                style={[styles.text, {minWidth: 10 + count.toString().length * 4}]}
            >
                {count}
            </Text>
        </View>
    </View>
}

const styles = StyleSheet.create({

    border: {
        backgroundColor: 'white',
        height: 16,
        borderRadius: 8,
        padding: 1,
    },

    badge: {
        backgroundColor: '#FA7268',
        borderRadius: 7,
        height: 14,
        justifyContent: 'center',
        alignItems: 'center',
    },

    text: {
        color: '#fff',
        fontSize: 8,
        lineHeight: 14,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});
