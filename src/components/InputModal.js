import React, { Component } from "react";
import {View, Text, Platform, KeyboardAvoidingView} from 'react-native';
import Modal from "react-native-modal";
import { StyleSheet } from "react-native";
import Input from '../components/Input';
import Button from '../components/Button';

export class InputModal extends Component {

    constructor(props){
        super(props);
        this.state = {isModalVisible: false}
    }

    componentDidMount() {

    }


    show = (param = {}) => {

        this.setState({
            title: param.title,
            message: param.message,
            label: param.label,
            value: param.value,
            isModalVisible: true,
        })

        this.callback = param.callback
        this.refs.input && this.refs.input.focus()
    }


    ok = () => {

        let text = this.refs.input.validate()

        if(text){
            this.setState({
                isModalVisible: false,
            })
            this.callback && this.callback(text)
            this.callback = undefined
        }

    };

    cancel = () => {

        this.refs.input.setValue("")

        this.setState({
            isModalVisible: false,
        })

        this.callback && this.callback()
        this.callback = undefined
    }

    renderTitle = () => {

        if(this.state.title){
            return (<Text style={styles.title} >{this.state.title}</Text>)
        }else{
            return null
        }
    }

    renderMessage = () => {

        if(this.state.message){
            return (<Text style={styles.message} >{this.state.message}</Text>)
        }else{
            return null
        }
    }

    render() {

        return (
            <Modal isVisible={this.state.isModalVisible} >
            <KeyboardAvoidingView style = {{flex: 1,justifyContent: 'center'}} behavior= {(Platform.OS === 'ios')? "padding" : null}  >
                <View style={styles.container}>
                    <View style = {{backgroundColor: "white", alignItems: 'center', padding: 16,}} >
                        {this.renderTitle()}
                        {this.renderMessage()}
                        <View style = {{flexDirection: 'row'}} >
                            <Input style = {{flex:1, marginBottom: 16}} ref = "input"  label = {this.state.label} value = {this.state.value}  required returnKeyType = { "done" } onSubmitEditing={() => { this.ok }} hideBorder />
                        </View>
                        <View style = {{flexDirection: 'row'}} >
                            <Button style ={{flex: 1, marginRight: 8}} title = "Болих" hideGradient second onPress = {this.cancel} />
                            <Button style ={{flex: 1 }} title = "Хадгалах" onPress = {this.ok} />
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
            </Modal>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        backgroundColor: '#fff',
        borderRadius: 8,
        overflow: 'hidden',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 14,
        textAlign: 'center',
        marginBottom: 32,
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    }
});
