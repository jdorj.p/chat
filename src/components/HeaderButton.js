import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class HeaderButton extends React.Component {


    constructor() {
        super();

    }

    renderIcon = () => {

        if(this.props.icon){

            return (<Icon
                name = {this.props.icon}
                color = {this.props.iconColor ? this.props.iconColor : "#000"}
                size = {this.props.iconSize ? this.props.iconSize : 24}
            />)
        }

        return null
    }

    renderTitle = () => {

        if(this.props.title){

            return (<Text
                style={[styles.text, this.props.titleStyle]}>
                {this.props.title}
            </Text>)
        }

        return null
    }

    render() {

        return (<TouchableOpacity
            onPress={this.props.onPress}
            style={[styles.container,this.props.style]}>

            {this.renderIcon()}
            {this.renderTitle()}

            {this.props.children}
        </TouchableOpacity>);

    }

}


const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // justifyContent: 'center',
        backgroundColor: 'transparent'

    },

    text: {
        color: '#000',
        fontFamily: 'RobotoCondensed-Regular',
        fontSize: 16,
        marginRight: 4,
    },

});
