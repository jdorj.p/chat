import React, {Component, forwardRef} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import {bottomModal} from './BottomModal';
import SelectModal from './SelectModal';


function Select (props,ref) {

    const [label, setLabel] = React.useState()
    const [value, setValue] = React.useState()
    const [error, setError] = React.useState()

    React.useImperativeHandle(ref, () => ({
        setValue: (value) => setValue(value),
        getValue: getValue,
        validate: validate,
    }));


    const getValue = () => {

        return value
    }


    const validate = () => {
        if(props.required && !value ) {

            if(props.requiredText) {
                setError(props.requiredText)
            }else{
                setError(`${props.label} сонгоно уу!`)
            }

            return false
        }

        setError(null)

        return getValue()
    }


    function select(){

        bottomModal.open(<SelectModal label = {props.label} options = {props.options} onSelect = {(item)=>{
            setValue(item.value)
            setLabel(item.label)
            setError(null)
        }} />)
    }

    return (
            <View style = {[props.style]} >
                <View style = {[styles.container]}>

                    <TouchableOpacity style = {{flex:1, justifyContent: 'center'}} onPress = {select} >
                        {(!value) ? null : <Text style = {styles.label} >{props.label}</Text>}
                        {(label) ? <Text style = {styles.text} >{label}</Text> : <Text style = {styles.placeholder} >{props.label}</Text>}
                        { (error) ? (<Text style = {styles.error} >{error}</Text>) : null }
                    </TouchableOpacity>

                </View>
                {(props.hideBorder) ? null : (<View style = {styles.border} />)}
            </View>
        )


}

export default Select = forwardRef(Select);


const styles = StyleSheet.create({

    container: {
        minHeight: 56,
        flexDirection: 'row',
        alignItems: 'center',
    },

    image: {
        marginRight: 32,
        height: 24,
        width: 24,
    },

    label: {
        color: '#999999',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        lineHeight: 16,
    },

    error:{
        color: '#EB5757',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        marginBottom: 8,
    },

    text: {
        color: '#000',
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },

    placeholder: {
        color: '#999',
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },

    border: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#E0E0E0'
    },

    passwordButton: {
        // marginTop: 4,
        // marginBottom: 4,
        // width: 48,
        // height: 48,
        marginLeft: 16,
        marginRight: 8,
        // borderWidth: 1,
        // borderColor: '#DB373D',
    }
});
