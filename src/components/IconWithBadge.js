import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
} from 'react-native';
import {EventRegister} from 'react-native-event-listeners';

let currentNotificationCount = 0;
EventRegister.addEventListener('setBadge', count => {
    currentNotificationCount = count;
});

export default class IconWithBadge extends Component {

    constructor(props) {
        super(props);

        this.state = {
            badgeCount: currentNotificationCount,
        };
    }

    UNSAFE_componentWillMount() {
        this.listener = EventRegister.addEventListener('setBadge', (count) => {
            this.setState({badgeCount: count});
        });
    }

    componentWillUnmount(): void {
        EventRegister.removeEventListener(this.listener);
    }

    render() {
        const {color} = this.props;
        const {badgeCount} = this.state;
        return (
            <View style={styles.container}>
                <Image source={this.props.image} style={{tintColor: color}}/>
                {this.state.badgeCount > 0 && (
                    <View style={styles.border}>
                        <View style={styles.badge}>
                            <Text
                                numberOfLines={1}
                                style={[styles.text, {minWidth: 10 + badgeCount.toString().length * 4}]}
                            >
                                {badgeCount}
                            </Text>
                        </View>
                    </View>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {},

    border: {
        position: 'absolute',
        backgroundColor: 'white',
        height: 16,
        borderRadius: 8,
        left: 8,
        top: -8,
        padding: 1,
    },

    badge: {
        backgroundColor: '#FA7268',
        borderRadius: 7,
        height: 14,
    },

    text: {
        color: '#fff',
        fontSize: 8,
        lineHeight: 14,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});
