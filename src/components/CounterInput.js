import React from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function CounterInput (props){

    const [value,setValue] = React.useState(props.value ?? 0)

    function increase(){
        changeValue(parseFloat(value) + 1)
    }

    function decrease(){
        if(value > 0){
            changeValue(parseFloat(value) - 1)
        }
    }

    function changeValue(value){

        if(value){
            if(value.length > 0 && value.startsWith("0") && !value.endsWith(".")){
                setValue(parseFloat(value))
            }else {
                setValue(value)
            }
            props.onChange && props.onChange(parseFloat(value))
        }else{
            setValue(0)
            props.onChange && props.onChange(0)
        }
    }

    function select(){

    }

    return <View style = {[styles.container,props.style]} >

        <TouchableOpacity style = {styles.button} onPress = {decrease} >
            <Icon size = {20} color = {"#000"} name = "minus" />
        </TouchableOpacity>

        {/*<TextInput*/}
        {/*    // ref="textInput"*/}
        {/*    style = {styles.text}*/}
        {/*    value={String(value)}*/}
        {/*    onChangeText={(value)=>{*/}
        {/*        // changeValue(parseInt(value))*/}
        {/*        // changeValue(parseFloat(value))*/}
        {/*        changeValue(value)*/}
        {/*    }}*/}
        {/*    selectionColor = "black"*/}
        {/*    keyboardType = "decimal-pad"*/}
        {/*    // allowFontScaling = {false}*/}
        {/*/>*/}

        <Text style = {styles.text} >{value}</Text>

        <TouchableOpacity style = {styles.button} onPress = {increase} >
            <Icon size = {20} color = {"#000"} name = "plus" />
        </TouchableOpacity>

    </View>
}

const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        borderRadius: 4,
        overflow: 'hidden',
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#999',
        alignItems: 'center',
    },

    textContainer:{
        width: 32,
        justifyContent: 'center',
        alignItems: 'center',
    },

    text: {
        width: 32,
        flex:1,
        color: '#000',
        fontSize: 14,
        textAlign: 'center',
        fontFamily: 'RobotoCondensed-Bold',
        padding: 0,
    },

    button:{
        backgroundColor: "#E4EAF1",
        height: 32,
        flex:1,
        // width: 32,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
