import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Cicon from "react-native-vector-icons/MaterialCommunityIcons";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Button extends Component {


    constructor(props) {
        super(props);

        this.state = {
            title: this.props.title,
            style: this.props.style,
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps){

        if(nextProps.title != this.state.title){

            this.setState({title: nextProps.title})
        }

        if(nextProps.style != this.state.style){

            this.setState({style: nextProps.style})
        }
    }


    renderImage(){

        if(this.props.image == null){
            return null;
        }

        return (
            <Image
                source = {this.props.image}
            />);

    }


    renderIcon(){

        if(this.props.icon == null){
            return null;
        }

        return (
            <Icon
                name = {this.props.icon}
                color = {(this.props.iconColor) ? this.props.iconColor : "#fff"}
                size = {20}
            />);

    }

    renderCicon(){

        if(this.props.cicon == null){
            return null;
        }

        return (
            <Cicon
                name = {this.props.cicon}
                color = {(this.props.iconColor) ? this.props.iconColor : "#fff"}
                size = {20}
            />);

    }

    renderRightIcon(){

        if(this.props.rightIcon == null){
            return null;
        }

        return (
            <Icon
                name = {this.props.rightIcon}
                color = {(this.props.iconColor) ? this.props.iconColor : "#fff"}
                size = {20}
            />);

    }

    renderText(){

        if(this.state.title == null){
            return null;
        }

        let style = {
            fontFamily: 'Roboto-Medium',
            color: '#fff',
            fontSize: 14,
            margin: 8,
        }

        if(this.props.second){
            style = styles.text2
        }

        if(this.props.disabled){
            style.color = "#8C8C8C"
        }


        return (
            <Text
                style={[style, this.props.titleStyle]}>
                {this.state.title}
            </Text>);

    }

    renderRightImage(){

        if(this.props.rightImage == null){
            return null;
        }

        return (
            <Image
                source = {this.props.rightImage}
            />);

    }


    setTitle = (title) => {

        this.setState({title: title})
    }

    onPress = () => {

        this.props.onPress && this.props.onPress(this)
    }

    render() {

        let shadow = (this.props.hideGradient || this.props.noShadow) ? {} : {
            shadowColor: '#F02436',
            shadowOpacity: 0.4,
            shadowOffset: {width: 2,height: 2},
            // shadowRadius: 8,
            elevation: 4,
        }

        let style = {}

        if(this.props.second){
            style = styles.style2
        }

        if(this.props.disabled){
            style.backgroundColor = "#E3E3E3"
            shadow.shadowColor = "#8C8C8C"
        }

        return (<TouchableOpacity
            onPress={this.onPress}
            style={[styles.touchableOpacity,shadow,this.state.style,style]}
            disabled={this.props.disabled}
        >

            {(this.props.hideGradient || this.props.disabled) ? null : (<LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#FA7268', '#DB373D',]} style = {styles.linearGradient}/>)}


            <View style = {styles.container}>
                {this.renderImage()}
                {this.renderIcon()}
                {this.renderCicon()}
                {this.renderText()}
                {this.renderRightIcon()}
                {this.renderRightImage()}
            </View>
            <View style = {styles.childrenContainer} >
                {this.props.children}
            </View>
        </TouchableOpacity>);

    }

}



const styles = StyleSheet.create({

    touchableOpacity: {
        borderRadius: 6,
        height: 48,

        // backgroundColor: 'transparent'
    },

    style2: {

        borderWidth: 1,
        borderColor: '#E0E0E0'
    },

    linearGradient: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        borderRadius: 6,
    },

    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    childrenContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    },

    text: {
        fontFamily: 'Roboto-Medium',
        color: '#fff',
        fontSize: 14,
        margin: 8,
    },

    text2: {
        fontFamily: 'Roboto-Medium',
        color: '#DB373D',
        fontSize: 14,
        margin: 8,
    },

});
