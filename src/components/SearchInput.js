import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";

export default class SearchInput extends React.Component {


    constructor(props) {
        super(props);

        // if(this.props.value){
        //     this.state = {value: this.props.value}
        // }
        //
        this.state = {value: this.props.value}
    }

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {

        // if(nextProps.value != this.state.value){
        //     this.setState({value: nextProps.value})
        // }
    }


    onChangeText = (text) => {

         this.setState({value: text})
         this.props.onChangeText && this.props.onChangeText(text.trim())
    }

    setValue = (value) => {

        this.setState({
            value: value
        })
    }

    focus = () => {

        this.refs.textInput.focus()
    }

    clear = () => {
        this.setState({value: ""})
    }

    render() {

        return (
            <View style = {[styles.container,this.props.style]}>
                <View style = {styles.row}>
                    <Icon name = "search" color = "#8E8E93" size = {20} style = {{marginLeft:8}} />
                    <TextInput
                        ref="textInput"
                        style = {styles.textInput}
                        onChangeText={this.onChangeText}
                        value={this.state.value}
                        autoFocus = {this.props.autoFocus}
                        selectionColor = "black"
                        secureTextEntry = {this.props.secureTextEntry}
                        scrollEnabled = {this.props.scrollEnabled}
                        keyboardType = {this.props.keyboardType}
                        placeholder = {this.props.placeholder}
                        returnKeyType = {this.props.returnKeyType}
                        onSubmitEditing={this.props.onSubmitEditing}
                        blurOnSubmit={false}
                    />
                </View>
            </View>)
    }

}



const styles = StyleSheet.create({

    container: {
        height: 36,
        borderRadius: 6,
        backgroundColor: '#F1F1F2',
    },

    row: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
    },

    textInput: {
        flex: 1,
        color: '#000',
        fontSize: 16,
        marginLeft: 8,
    },



});
