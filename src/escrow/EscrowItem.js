import React, { Component } from "react";
import {FlatList, StyleSheet, SafeAreaView, Text, View} from 'react-native';
import Border from '../components/Border';
import DateUtil from '../common/DateUtil';
import Util from '../common/Util';
import Button from '../components/Button';

export default function EscrowItem(props) {

    const escrow = props.escrow

    return(
        <View >
            <View style = {styles.container} >
                <View>
                    <Text style = {styles.title}>Дүн</Text>
                    <Text style = {styles.amount}>{Util.formatCurrency(escrow.amount)}</Text>
                </View>
                <View style = {{alignItems: 'flex-end'}} >
                    <Text style = {styles.title}>Огноо</Text>
                    <Text style = {styles.date} >{DateUtil.format(escrow.updateDate)}</Text>
                </View>
            </View>
            <View style = {styles.container} >
                <View>
                    <Text style = {styles.title}>Хэнээс</Text>
                    <Text style = {styles.text}>{escrow.user.name}</Text>
                </View>
                <View style = {{alignItems: 'flex-end'}} >
                    <Text style = {styles.title}>Утга</Text>
                    <Text style = {styles.text}>{escrow.description}</Text>
                </View>
            </View>
            <View style = {[styles.container,{marginBottom: 16}]} >
                <View style = {{flex:1}} >
                    <Text style = {styles.title}>Төлөв</Text>
                    <Text style = {styles.text}>{escrow.statusText}</Text>
                    {/*<Text style = {styles.text}>{escrow.transferStatus}</Text>*/}
                </View>
                <View style = {{flexDirection: 'row'}} >
                    {props.userId == escrow.payerUserId && escrow.cancellable ? <Button title = "Буцаах" style = {styles.cancelButton} second hideGradient onPress = {props.cancel} /> : null}
                    {escrow.transferStatus == 'new' ? <Button title = "Шилжүүлэх" style = {styles.button} hideGradient onPress = {props.onPress} /> : null }
                </View>
            </View>
            <Border/>
        </View>
    )

}

const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 8,
        marginBottom: 8,
    },
    amount: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 20,
        color: '#000',
    },
    button:{
        height: 36,
        borderRadius: 4,
        backgroundColor: '#FA7268'
    },
    cancelButton:{
        height: 36,
        borderRadius: 4,
        marginRight: 4,
        marginLeft: 8,
    },
    date: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#333',
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: '#333',
    },

    title: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#666',
    },
})

