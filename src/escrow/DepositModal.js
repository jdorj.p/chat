import React, {forwardRef} from 'react';
import {View, Text} from 'react-native';
import { StyleSheet } from "react-native";
import Button from '../components/Button';
import Input from '../components/Input';
import Constants from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import CustomAlert from '../common/CustomAlert';
import {bottomModal} from '../components/BottomModal';
import Util from '../common/Util';

function DepositModal(props, ref) {


    const cancel = () => {
        bottomModal.close()
    }

    const validate = async () => {

        let amount = amountRef.current.validate()
        let descr = descriptionRef.current.validate()

        if(amount && descr){

            bottomModal.close()

            Util.navigation.navigate('ChooseCard',{callback: async ({cardId,confirmAmount,password},callback)=>{

                    let params = {
                        roomId: props.roomId,
                        userId: await AsyncStorage.getItem("userId"),
                        amount: amount,
                        confirmAmount: confirmAmount,
                        cardId: cardId,
                        description: descr,
                        password: password,
                    }

                    fetch(Constants.WSURL + "wsock/payment/chargeAccount", {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': await AsyncStorage.getItem('token'),
                        },
                        body: JSON.stringify(params)
                    })
                        .then((response) => {

                            console.log(response)
                            return response.json()

                        })
                        .then((json) => {


                            if (json.status != "000"){

                                callback && callback(json.message)

                                CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                            } else {

                                callback && callback()
                                CustomAlert.alert("Амжилттай","Гүйлгээ амжилттай.", 'success');
                            }

                        }).catch((error) => {

                        callback && callback(error)
                        console.log(error);
                        CustomAlert.alert("Алдаа гарлаа!","Дахин оролдоно уу", 'error');
                    });

                }})

        }
    }


    const amountRef = React.useRef(null);
    const descriptionRef = React.useRef(null);


    return (<View style={styles.container}>
        <Text style={styles.title} >Данс цэнэглэх</Text>
        <Text style={styles.message} >Эскроу дансанд мөнгө байршуулах</Text>
        <Input ref = {amountRef} label = "Дүн" keyboardType = "number-pad" required returnKeyType = { "next" } onSubmitEditing={() => { descriptionRef.current.focus() }} />
        <Input ref = {descriptionRef} label = "Утга" required returnKeyType = { "done" } onSubmitEditing={validate} />
        <View style = {{flexDirection: 'row',marginTop: 24}} >
            <Button style ={{flex: 1, marginRight: 8}} title = "Болих" hideGradient second onPress = {cancel} />
            <Button style ={{flex: 1, marginLeft: 8}} title = "Үргэлжлүүлэх" onPress = {validate} />
        </View>
    </View>);
}

export default DepositModal = forwardRef(DepositModal);

const styles = StyleSheet.create({


    container: {
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 14,
        marginBottom: 16,
    },

    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 8,
        marginLeft: 0,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: '#666666',
        marginLeft: 8,
    },

    balance: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: '#000',
        marginRight: 8,
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    }
});
