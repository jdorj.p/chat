import React, {forwardRef} from 'react';
import {View, Text, KeyboardAvoidingView, Platform, ScrollView, Animated} from 'react-native';
import { StyleSheet } from "react-native";
import Button from '../components/Button';
import Input from '../components/Input';
import Constants from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import CustomAlert from '../common/CustomAlert';
import {bottomModal} from '../components/BottomModal';
import Toast from 'react-native-simple-toast';
import Util from '../common/Util';
import Select from '../components/Select';

export default function WithdrawScreen(props, ref) {

    const [loading, setLoading] = React.useState(false)
    const [banks, setBanks] = React.useState([])

    React.useEffect(()=>{

        props.navigation.setOptions({
            title: 'Шилжүүлэх хүсэлт',
        })

        fetchBanks()

    },[])

    function fetchBanks(){

        setLoading(true)

        fetch(Constants.WSURL + "wsock/bank/list", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {


                if(json.status == "000") {

                    setBanks(json.entity)

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                }

                setLoading(false)

            }).catch((error) => {

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            setLoading(false)
        });

    }


    const validate = async () => {


        let bankCode = bankCodeRef.current.validate()
        let accountNumber = accountNumberRef.current.validate()
        let accountHolder = accountHolderRef.current.validate()
        let password = passwordRef.current.validate()


        if(bankCode && accountNumber && accountHolder && password){
           sendData({
               userId: await AsyncStorage.getItem('userId'),
                   "id" : props.route.params.escrow.id,
                   "accountNumber" : accountNumber,
                   "accountHolder" : accountHolder,
                   "bankCode" : bankCode,
                   "password" : password,
           })
        }
    }

    const sendData = (params) => {

        console.log('params',params)

        setLoading(true)

        fetch(Constants.WSURL + "wsock/payment/setAccountInfo", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        }).then((response) => response.json())
            .then((json) => {

                console.log('withdraw screen',json)

                if(json.status == "000") {

                    CustomAlert.alert("Хүсэлт илгээгдлээ","Нөгөө тал зөвшөөрсний дараа гүйлгээ хийгдэх болно", 'success');
                    props.navigation.navigate('Chat')

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                }

                setLoading(false)

            }).catch((error) => {

            CustomAlert.alert("Алдаа гарлаа!",error, 'error');

            setLoading(false)
        });
    }



    const bankCodeRef = React.useRef(null);
    const accountNumberRef = React.useRef(null);
    const accountHolderRef = React.useRef(null);
    const passwordRef = React.useRef(null);

    return (<View style={styles.container}>
                <KeyboardAvoidingView behavior= {(Platform.OS === 'ios')? "padding" : null}>
                    <ScrollView
                        contentContainerStyle = {{padding: 16}}
                        keyboardShouldPersistTaps = 'handled'
                        keyboardDismissMode = "interactive"
                        bounces = {false}>
                            <Text style={styles.message} >Эскроу дансан дахь {Util.formatCurrency(props.route.params.escrow.amount)}-г банкны дансруу шилжүүлэх хүсэлт илгээх.</Text>
                            <Text style={styles.warning} >Шимтгэл 1%  буюу {Util.formatCurrency(props.route.params.escrow.amount * 0.01)}, мөн 3 сая төгрөгөөс дээш дүнтэй гүйлгээ алжын өдрүүдэд 09:10-16:30 цагийн хооронд хийгдэхийн анхаарна уу!</Text>

                            <Input label = "Дүн" editable = {false} value = {Util.formatCurrency(props.route.params.escrow.amount)} />
                            <Input label = "Шимтгэл" editable = {false}  value = {Util.formatCurrency(props.route.params.escrow.amount * 0.01)} />
                            <Input label = "Дансанд орох дүн" editable = {false}  value = {Util.formatCurrency(props.route.params.escrow.amount * 0.99)} />
                            <Select ref = {bankCodeRef} label = "Банк" required options = {banks.map(bank=> ({label: bank.bankName, value: bank.bankCode}))} />
                            <Input ref = {accountNumberRef} label = "Дансны дугаар" required keyboardType = "number-pad" required returnKeyType = { "next" } onSubmitEditing={() => { accountHolderRef.current.focus() }} />
                            <Input ref = {accountHolderRef} label = "Дансны нэр" required returnKeyType = { "next" } onSubmitEditing={() => { passwordRef.current.focus() }} />
                            <Input ref = {passwordRef} label = "Нууц үг" required returnKeyType = { "done" } secureTextEntry hideSecureText onSubmitEditing={validate} />
                            <Button style ={{marginTop: 24}} title = "Илгээх" onPress = {validate} />
                    </ScrollView>
                </KeyboardAvoidingView>
                <ActivityIndicatorView animating = {loading} />
    </View>);

}


const styles = StyleSheet.create({


    container: {
        flex:1,
        backgroundColor: '#fff',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 16,
        lineHeight: 22,
        marginBottom: 16,
    },

    warning: {
        fontFamily: 'Roboto-Bold',
        color: '#1F1F1F',
        fontSize: 16,
        lineHeight: 22,
        marginBottom: 16,
    },

    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 8,
        marginLeft: 0,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: '#666666',
        marginLeft: 8,
    },

    balance: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: '#000',
        marginRight: 8,
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    },

    label: {
        color: '#999',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },
});
