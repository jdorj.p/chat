import React, { Component } from "react";
import {FlatList, StyleSheet, SafeAreaView, Text, View} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
let statusBarHeight = getStatusBarHeight(true);
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Button from '../components/Button';
import InvoiceModal from './InvoiceModal';
import {bottomModal} from '../components/BottomModal';
import EscrowItem from './EscrowItem';
import Border from '../components/Border';
import DepositModal from './DepositModal';
import Util from '../common/Util';
import PasswordModal from '../payment/PasswordModal';
import {get} from 'react-native/Libraries/TurboModule/TurboModuleRegistry';
import SimpleToast from 'react-native-simple-toast';

export default function EscrowScreen(props) {

    const [loading, setLoading] = React.useState(false)
    const [data, setData] = React.useState([{}])
    const [empty, setEmpty] = React.useState(false)

    const [userId,setUserId] = React.useState()

    React.useEffect(()=>{

        props.navigation.setOptions({
            title: 'Эскроу данс',
        })

    },[])

    React.useEffect(() => {

        const unsubscribe = props.navigation.addListener('focus', () => {

            getData()
        });

        return unsubscribe;
    }, [props.navigation]);


    const getData = async () => {

        setLoading(true)
        let userId = await AsyncStorage.getItem('userId')

        setUserId(userId)

        let roomId = props.route.params.roomId

        console.log('roomId',roomId)

        let url = Constants.WSURL + "wsock/invoice/list?roomId="+ roomId +"&userId=" + userId

        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {

                console.log(json)

                if(json.status == "000") {

                    if(json.entity.length == 0){
                        setEmpty(true)
                    }
                    setData([{},...json.entity])

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                }


                setLoading( false)

            }).catch((error) => {

            console.log(error)

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            setLoading(false);
        });
    }

    function deposit() {

        bottomModal.open(<DepositModal roomId = {props.route.params.roomId} />)
    }

    function sendInvoice() {

        bottomModal.open(<InvoiceModal receiverName = {props.route.params.receiverName} roomId = {props.route.params.roomId} />)
    }

    function sendWidthraw(escrow) {

        props.navigation.navigate('Withdraw',{escrow: escrow})
    }

    async function cancel(item){

        // console.log('cancel',item)

        bottomModal.open(<PasswordModal text = "Таны эскроу дансанд оруулсан мөнгө таны картруу буцах болно." onContinue={async (password)=>{

            setLoading(true)

            let params = {
                id: item.id,
                userId: await AsyncStorage.getItem('userId'),
                password: password,
            }

            // console.log('cancel',params)

            let url = Constants.WSURL + "wsock/payment/reverseRequest"

            fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': await AsyncStorage.getItem('token'),
                },
                body: JSON.stringify(params)
            }).then((response) => response.json())
                .then((json) => {

                    // console.log(json)

                    if(json.status == "000") {

                        SimpleToast.show("Буцаах хүсэлт илгээлээ")
                        getData()

                    }else{

                        CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                    }


                    setLoading( false)

                }).catch((error) => {

                console.log(error)

                CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

                setLoading(false);
            });

        }} />)

    }

    return (
        <View style = {{flex:1}} >
            <SafeAreaView style = {styles.safeAreaView} >
                <View style = {styles.container} >
                    <FlatList
                        style = {styles.flatList}
                        data = {data}
                        renderItem={({item, index}) =>{

                            if(index == 0){
                                return <View>
                                    <Text style={styles.text} >Эскроу данс нь хоёр талын зөвшөөрлөөр гүйлгээ хийгдэх дундын данс юм. Бараа бүтээгдэхүүн захиалгаар худалдан авах болон нийлүүлэхэд хоёр талын эрсдэлийг бууруулдаг.</Text>
                                    <Text style={styles.warning} >Эскроу данс цэнэглэхэд үйлчилгээний шимтгэл нь 1000₮. Эскроу данснаас гүйлгээ хийхэд 1% шимтгэлтэй болохыг анхаарна уу!</Text>

                                    <Text style = {styles.title}>Дансны жагсаалт</Text>

                                    {(empty) ? <Text style={styles.text} >{props.route.params.receiverName} болон таны дунд эскроу данс байхгүй байна. Аль нэг тал мөнгө шилжүүлснээр данс үүсэх болно.</Text> : null}
                                    <Border/>
                                </View>
                            }

                            return <EscrowItem
                                key = {index}
                                escrow = {item}
                                userId={userId}
                                cancel = {()=>{
                                    cancel(item)
                                }}
                                onPress = {()=>{
                                    sendWidthraw(item)
                                }}
                            />}

                        }
                    />

                    <View style = {{flexDirection: 'row',padding: 16}} >
                        <Button style ={{flex: 1, marginRight: 8}} title = "Данс цэнэглэх" onPress = {deposit} />
                        <Button style ={{flex: 1, marginLeft: 8}} title = "Нэхэмжлэх" onPress = {sendInvoice} />
                    </View>

                </View>
                <ActivityIndicatorView animating = {loading} />
            </SafeAreaView>
        </View>

    );

}


const styles = StyleSheet.create({

    safeAreaView:{
        flex:1,
        backgroundColor: '#fff',
    },

    container: {
        flex:1,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 16,
        marginBottom: 16,
        lineHeight: 22,
    },

    warning: {
        fontFamily: 'Roboto-Bold',
        color: '#1F1F1F',
        fontSize: 16,
        lineHeight: 22,
        marginBottom: 16,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        marginBottom: 16,
    },

    flatList: {
        flex:1,
        padding:16,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
        borderRadius: 18,
        minHeight: 36,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        // borderRadius: 18,
        margin: 8,
        marginRight: 0,
        // backgroundColor: '#4CA2EB',
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        borderRadius: 18,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },

    imageBackground: {
        flex: 1,
        margin: 16,
    },

    scrollView:{
        flex:1,
    },

    exitButton: {
        width: 44,
        height: 44,
        position: 'absolute',
        right: 8,
        top: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },

    exitImage: {
        width: 34,
        height: 34,
    },

});
