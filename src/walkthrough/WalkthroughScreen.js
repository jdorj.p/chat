import React from 'react';
import {SafeAreaView, StatusBar, View, StyleSheet, ScrollView, Dimensions} from 'react-native';
import WalkthroughView from "./WalkthroughView";
import Button from "../components/Button";
import PageControl from 'react-native-page-control';
let WIDTH = Dimensions.get('window').width

export default class WalkthroughScreen extends React.Component {
    constructor() {
        super();
    }

    state = {
        currentPage: 0,
        data: [
            {image: require('../image/chat.png'), title: "MINU CHAT гэж юу вэ?", text: "Чатын апп бөгөөд гэрээ, хэлцэл хийх, дундын данс үүсгэх, зээлийн үйлчилгээ үзүүлэх апп юм."},
            {image: require('../image/contract.png'), title: "Хэлцэл", text: "Чатаар хийсэн хэлцлийг дахин өөрчлөх боломжгүйгээр блокчэйн-д хадгална"},
            {image: require('../image/escrow.png'), title: "Эскроу данс", text: "Дундын данс бөгөөд 2 талууд зөвшөөрсөн тохиолдолд гүйлгээ хийгдэнэ. Бараа захиалах, захиалгаар нийлүүлэх зэрэг үед 2 талыг эрсдэлээс хамгаална."},
            // {image: require('../image/lend.png'), title: "Зээл", text: "Хэрэглэгчид хоорондоо мөнгө зээлэх бөгөөд. Зээлсэн тал мөнгөө буцааж төлөхгүй бол банкнаас дахин зээл авах эрхгүй болно."},
        ]

    }

    onScroll = (e) => {

        var offset = e.nativeEvent.contentOffset;
        if(offset) {
            let page = Math.round(offset.x / WIDTH);
            this.setState({currentPage: page});

        }
    }

    skip = () => {

        this.start()
    }

    start = () => {
        this.props.navigation.navigate('Login')
    }

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="dark-content"/>
                <ScrollView style = {styles.scrollView}
                            ref = "scrollView"
                            horizontal = {true}
                            pagingEnabled = {true}
                            showsHorizontalScrollIndicator = {false}
                            onScroll = {this.onScroll}
                >
                    {this.state.data.map((item,index)=><WalkthroughView key = {index} image = {item.image} title = {item.title} text = {item.text} />)}
                </ScrollView>

                <View style = {styles.bottomContainer} >

                    <PageControl
                        style = {{backgroundColor: 'white'}}
                        numberOfPages={this.state.data.length}
                        currentPage={this.state.currentPage}
                        hidesForSinglePage
                        pageIndicatorTintColor='#D8D8D8'
                        currentPageIndicatorTintColor='#DB373D'
                        indicatorStyle={{borderRadius: 5}}
                        currentIndicatorStyle={{borderRadius: 5}}
                        indicatorSize={{width:10, height:10}}
                        onPageIndicatorPress={this.onItemTap}
                    />
                    {
                        (this.state.currentPage + 1 == this.state.data.length)
                            ? (<Button style = {styles.start} title = "Эхлэх" onPress={this.start} rightImage = {require('../image/right_arrow.png')} />)
                        : (<Button style = {styles.skip} titleStyle = {{fontFamily: 'Roboto-Medium', color: '#DB373D', fontSize: 14,}} hideGradient title = "АЛГАСАХ" onPress={this.skip}  />)
                    }

                </View>
            </SafeAreaView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        position: 'relative',
        justifyContent: 'flex-end',
        // alignItems: 'center',
    },
    scrollView : {
        flex:1,
    },

    bottomContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        bottom: 0,
        left: 0,
        right: 0,
        padding: 16,
        paddingLeft: 54,
    },

    register: {
        backgroundColor: 'white',
        marginBottom: 16,
        width: 290,
    },

     skip: {
        backgroundColor: 'white',
         borderRadius: 0,
         height: 56,
         width: 164,
     },

     start: {
         height: 56,
         width: 164,
     }
    }
);
