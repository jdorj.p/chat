import React from 'react';
import { Image, View, StatusBar, StyleSheet, Text, Dimensions } from 'react-native';
let WIDTH = Dimensions.get('window').width

export default class WalkthroughView extends React.Component {
    constructor() {
        super();
    }

    render() {


        return (
            <View style={styles.container}>
                    <Image
                        style={styles.image}
                        source={this.props.image}
                        resizeMode={"contain"}
                    />
                    <Text style={styles.title}>{this.props.title}</Text>
                    <Text style={styles.text}>{this.props.text}</Text>
            </View>
        );
    }
}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        // alignItems: 'center',
        justifyContent: 'center',
        width: WIDTH,
    },


    image: {
        // height: 232,
        // width: 232,
        // flex:1,
        width: WIDTH,
        height: WIDTH,
        // aspectRatio: 1,
        marginBottom: 8,
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        textAlign: 'center',
        fontSize: 20,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        // width: 300,
        margin: 16,
        color: '#000',
        opacity: 0.6,
        textAlign: 'center',
        fontSize: 14,
        lineHeight: 20,
    }
});
