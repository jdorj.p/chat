import React from 'react';
import Badge from '../components/Badge';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../common/Constants';
import {RoomUtil} from '../room/RoomUtil';
import {EventRegister} from 'react-native-event-listeners';

function ChatBadge({room,style},ref) {

    const [count,setCount] = React.useState(0)

    const contractCountRef = React.useRef(null)
    const escrowCountRef = React.useRef(null)

    React.useImperativeHandle(ref, () => ({
        getCount: getCount
    }));

    React.useEffect(()=>{

       let listener = EventRegister.addEventListener('chatBadge', () => {
            getCount()
        });

       return ()=>EventRegister.removeEventListener(listener)

    },[])

    React.useEffect(()=>{
        getCount()
    },[room])


    function getCount() {

        if(room && room.id){
            getContractCount()
            getEscrowCount()
        }
    }

    function sumCounts() {

        if(contractCountRef.current != null && escrowCountRef.current != null){

            let count = contractCountRef.current + escrowCountRef.current
          setCount(count)

        }
    }

    async function getContractCount() {

        let userId = await AsyncStorage.getItem('userId')
        let url = Constants.WSURL + "wsock/chat/contractListByUserId?userId=" + userId

        let withUserId = new RoomUtil(userId).otherUsers(room.userList)[0].userId

        if(withUserId){
            url += '&withUserId=' + withUserId
        }

        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {

                if(json.status == "000") {

                    contractCountRef.current = json.entity.length
                    sumCounts()
                }

            }).catch((error) => {

            console.log(error)
        });
    }

    async function getEscrowCount() {

        let userId = await AsyncStorage.getItem('userId')

        let url = Constants.WSURL + "wsock/invoice/list?roomId="+ room.id +"&userId=" + userId

        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {


                if(json.status == "000") {

                    escrowCountRef.current = json.entity.length
                    sumCounts()
                }

            }).catch((error) => {

            console.log(error)
        });
    }

    if(!count){
        return null
    }

    return <Badge count = {count} style = {style} />
}

export default React.forwardRef(ChatBadge)
