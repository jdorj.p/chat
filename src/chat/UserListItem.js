import React, { Component } from "react";
import {View, Text,TouchableOpacity , StyleSheet} from 'react-native';
import {ProfileImage} from '../common/ProfileImage';

export class UserListItem extends Component {

    constructor(props){
        super(props);

        this.state = {
        }
    }

    componentDidMount() {

    }

    render() {

        let item = this.props.item


        return (
            <TouchableOpacity style = {styles.container} onPress = {this.props.onPress} >
                <ProfileImage url = {item.avatar}  >
                </ProfileImage>
                <View style = {{flex: 1, marginLeft: 8, marginRight: 8, justifyContent: 'center'}} >
                    <Text style = {[styles.text]} >
                        {item.name}
                    </Text>
                    <View style = {{flexDirection: 'row', marginTop: 4}} >
                        <Text style = {[styles.phone]} numberOfLines={1} >{item.phone}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        padding: 8,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
    },

    phone: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#333333',
        fontSize: 14,
    },

    date: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
    },


});
