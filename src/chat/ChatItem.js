import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, Dimensions, Clipboard} from 'react-native';
import DateUtil from '../common/DateUtil';
import {ProfileImage} from '../common/ProfileImage';
import ChatUtil from './ChatUtil';
import {SendView} from './SendView';
import {SeenView} from './SeenView';
import Constants from '../common/Constants';
import axios from 'react-native-axios'
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import RetryView from './RetryView';
import {Status, MessageTypes, OptionTypes} from '../data/MessageAdapter';
import {bottomModal} from '../components/BottomModal';
import InvoiceModal from '../payment/InvoiceModal';
import PasswordModal from '../payment/PasswordModal';
import Toast from 'react-native-simple-toast';
import CustomAlert from '../common/CustomAlert';
const windowWidth = Dimensions.get('window').width;
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs'
import ChatText from './ChatText';
import AmountModal from '../payment/AmountModal';
import Util from '../common/Util';
import GiftCardModal from '../giftcard/GiftCardModal';
import ToolButton from './ToolButton';
import UserUtil from '../common/UserUtil';
import {stompContext} from './Stomp';
import RepliedView from './RepliedView';

export class ChatItem extends Component {

    constructor(props){
        super(props);

        this.state = {
            isLoading: false,
        }
    }

    componentDidMount() {

        let message =  this.props.item

        if(message.type == 'IMAGE'){

            let content = JSON.parse(message.content)
            if(!content.cdnId && content.file){
                this.uploadImage()
            }
        }
    }


    render() {

        let message =  this.props.item

        let align = {}
        let contentStyle = {}

        let sendView = null
        let seenView = null

        let seenUsers = ChatUtil.otherUsers(message.seenUserList, this.props.userId)

        if(this.props.isGroup){

            if(message.fromUserId == this.props.userId) {
                sendView = (<SendView sent={message.status == Status.sent} style={{marginRight: 8, marginBottom: 8}}/>)
            }

            let align = message.fromUserId == this.props.userId ? "flex-end": "flex-start"

            seenView = (<SeenView seenUserList = {seenUsers} align = {align} style = {{marginLeft: 36}} userId = {this.props.userId} fromUserId = {message.fromUserId} />)

        }else{

            if(message.fromUserId == this.props.userId){

                if(seenUsers.length > 1){
                    sendView = (<SeenView seenUserList = {seenUsers} userId = {this.props.userId} align = "flex-end" />)
                }else{
                    sendView = (<SendView sent = {message.status == Status.sent} style = {{marginRight: 8, marginBottom: 8}} />)
                }
            }

        }

        if(this.props.contract){
            seenView = null
            sendView = null
        }

        if(message.fromUserId == this.props.userId){
            align.justifyContent = "flex-end"
            contentStyle.backgroundColor = "#FA7268"

        }

        if(message.type == 'SYSTEM'){

            align.justifyContent = "center"
            contentStyle.backgroundColor = "#999999"
            contentStyle.marginRight = 2
            sendView = null
            seenView = null
        }

        if(!this.props.isLast){
            seenView = null
        }

        let containerStyle = {}

        if(!this.props.nextItem){
            containerStyle.marginTop = 4
        }

        return (
            <TouchableWithoutFeedback style = {[containerStyle]} onLongPress = {this.onPress} >
                <View style = {styles.container} >
                    <View style = {[styles.textContainer,align]} >
                         {this.renderProfile(message)}
                        <View style = {[{flex:1, flexDirection: 'row', alignItems: 'flex-end',},align]} >
                            <RepliedView message = {message.replyChat} userId={this.props.userId} repliedUser = {message.fromUser}
                                         onPress = {()=>{ this.props.chatScreen.scrollToChatItem(message.replyChat.id) }}
                            >
                                {this.renderContent(message,contentStyle)}
                            </RepliedView>
                        </View>
                        <View style = {{justifyContent: 'flex-end'}} >
                        {sendView}
                        </View>
                    </View>
                    {seenView}
                </View>
            </TouchableWithoutFeedback>
        );
    }


    renderContent = (message,style) => {

        let textStyle = {

        }

        // console.log('message.fromUserId',message.fromUserId)
        // console.log('this.props.userId',this.props.userId)
        // console.log('message type',message.type)

        if(message.fromUserId == this.props.userId){
            textStyle.color = "#fff"
        }

        if(message.type == MessageTypes.SYSTEM){
            textStyle.color = "#fff"
            textStyle.fontSize = 14
        }

        if(message.type == MessageTypes.CHAT){

            return this.renderText(message,style,textStyle)

        }else if((message.type == MessageTypes.CONTRACT_REQUEST
            || message.type == MessageTypes.OPTION
            || message.type == MessageTypes.CONTRACT_END_REQUEST
            || message.type == MessageTypes.INVOICE
            || message.type == MessageTypes.WITHDRAW_REQUEST
            || message.type == MessageTypes.INVOICE_CANCEL_REQUEST
            || message.type == MessageTypes.INVOICE_CREATE
            || message.type == MessageTypes.INVOICE_CREATE_WITH_DESC
            || message.type == MessageTypes.CONTRACT_CANCEL_REQUEST
        )
            && !this.props.contract
        ){

            return this.renderOptions(message,style,textStyle)

        }else if(message.type == MessageTypes.IMAGE) {

            return this.renderImage(message,style,textStyle)

        }else if(message.type == MessageTypes.DELETED) {

            return this.renderDeleted(message,style,textStyle)

        }else {
            return this.renderText(message,style,textStyle)

        }

    }

    renderText = (message,style,textStyle) => {

        return (<View style = {[styles.buble,style]} >
            {this.renderName(message,style,textStyle)}
                <ChatText style = {textStyle} navigation = {this.props.navigation} >{message}</ChatText>
            <View style = {styles.dateContainer} >
                {(this.props.item.createdDate) ? (<Text style = {[styles.date,{color: textStyle.color ? textStyle.color : styles.date.color}]}>{DateUtil.format(this.props.item.createdDate)}</Text>) : null}
            </View>
            <ActivityIndicatorView animating = {this.state.isLoading} />
        </View>)

    }

    renderDeleted = (message,style,textStyle) => {

        return (<View style = {[styles.bubleDeleted]} >
            {this.renderName(message,style,textStyle)}
                <Text style = {styles.deleted} >{'Устгасан'}</Text>
            {/*<View style = {styles.dateContainer} >*/}
            {/*    {(this.props.item.createdDate) ? (<Text style = {[styles.date,{color: textStyle.color ? textStyle.color : styles.date.color}]}>{DateUtil.format(this.props.item.createdDate)}</Text>) : null}*/}
            {/*</View>*/}
            {/*<ActivityIndicatorView animating = {this.state.isLoading} />*/}
        </View>)

    }



    renderOptions  = (message,style,textStyle) => {

        let items = []

        if(message.fromUserId != this.props.userId){

            let options = message.options

            if(options){
                for(let i = 0; i < options.length; i++){

                    let option = options[i]
                    items.push(this.renderOption(option))

                }
            }
    }

        return (
            <View style = {[styles.buble,style]} >
                {this.renderName(message,style,textStyle)}
                <Text style = {[styles.text,textStyle]}>{message.content}</Text>
                    {items}
                <View style = {styles.dateContainer} >
                    {(this.props.item.createdDate) ? (<Text style = {[styles.date,{color: textStyle.color}]}>{DateUtil.format(this.props.item.createdDate)}</Text>) : null}
                </View>
            </View>
        );


    }

    renderOption  = (option) => {

        return (
            <TouchableOpacity key = {option.id} onPress = {()=>this.selectOption(option)} style = {styles.option} >
                <Text style = {styles.buttonText}>{option.name}</Text>
            </TouchableOpacity>
        );
    }

    selectOption = (option) => {

        let message =  this.props.item

        console.log('select option',option)


        // if(message.type == MessageTypes.INVOICE){
        if(option.type == OptionTypes.invoice){

            bottomModal.open(<InvoiceModal invoice = {{invoiceId: message.typeId, amount: message.amount,message: message.content,optionChatId: message.id}} />)

        // }else if(message.type == MessageTypes.WITHDRAW_REQUEST){
        }else if(option.type == OptionTypes.withdrawRequest){

            bottomModal.open(<PasswordModal text = {message.content + " Зөвшөөрөх"} onContinue={this.confirmToWithdraw} />)

        // }else if(message.type == MessageTypes.INVOICE_CANCEL_REQUEST){
        }else if(option.type == OptionTypes.reverseRequest){

            bottomModal.open(<PasswordModal text = {message.content + " Зөвшөөрөх"} onContinue={this.confirmToCancel} />)

        // }else if(message.type == MessageTypes.CONTRACT_CANCEL_REQUEST){
        }else if(option.type == OptionTypes.cancel){

            bottomModal.open(<PasswordModal
                // text = {message.content}
                onContinue={this.confirmToCancelContract} />)

        // }else if(message.type == MessageTypes.INVOICE_CREATE){
        }else if(option.type == OptionTypes.amount){

            bottomModal.open(<AmountModal text = {message.content} type = "request" onContinue={this.sendAmount} />)

        // }else if(message.type == MessageTypes.INVOICE_CREATE_WITH_DESC){
        }else if(option.type == OptionTypes.info){

            bottomModal.open(<AmountModal text = {message.content} type = "order" description onContinue={this.sendAmount} />)

        }else if(option.type == OptionTypes.giftCard){

            bottomModal.open(<GiftCardModal text = {message.content} type = "order" onContinue={(count)=> {

                this.props.adapter.add({
                    type: MessageTypes.CHAT,
                    content: count,
                    new: true,
                    typeId: option.id,
                    optionChatId: message.id,
                })
            }} />)

        }else{

            // this.props.adapter.send({
            //     type: MessageTypes.OPTION,
            //     typeId: option.id,
            // },false)

            this.props.adapter.add({
                type: MessageTypes.OPTION,
                content: option.name,
                new: true,
                typeId: option.id,
                optionChatId: message.id,
            })

        }

    }

    renderProfile = (message) => {

        if(!this.props.isGroup){
            return null
        }

        if(this.shouldShowFromUser(message)){
            return (<ProfileImage user = {[message.fromUser]} size = {24} style = {{marginLeft: 8,marginTop: 4}} />)
        }else{
            return (<View style = {{width: 32}} />)
        }

    }

    renderName = (message,style,textStyle) => {

        if(!this.shouldShowFromUser(message)){
            return null
        }

        // if((message.type != 'SYSTEM' && message.fromUserId != this.props.userId && this.props.isGroup) || message.fromUserId == "101497326949014") {
        if((message.type != 'SYSTEM' && message.fromUserId != this.props.userId) || message.fromUserId == "101497326949014") {
            return <Text style={styles.name}>{message.fromUser.name}</Text>
        }else{
            return null
        }

        return null
    }

    shouldShowFromUser = (message) => {

        if(message.fromUserId == this.props.userId){ // || message.type == 'SYSTEM' || this.props.nextItem && this.props.nextItem.fromUserId == message.fromUserId && this.props.nextItem.type != 'SYSTEM'){
            return false
        }else{
            return true
        }
    }

    renderImage = (message,style,textStyle) => {

        let content = JSON.parse(message.content)
        let imageStyle = {
            aspectRatio: content.ratio ?? 1
        }

        let source
        let url


        if(content.cdnId){
            source = {uri: Constants.CDN_URL + 'img/' + content.cdnId + '?w=' + Math.floor(styles.imageContainer.width * 2)}
            url =  Constants.CDN_URL + 'img/' + content.cdnId
        }else if(content.file && content.file.uri){

            // console.log('renderImage',content)

            let file = content.file
            source = {uri: file.uri}
            url = file.uri
        }

        let dateStyle = {
            color: '#fff',
            shadowColor: '#000',
            shadowOpacity: 0.8,
            shadowOffset: {width: 2,height: 2},
            elevation: 1,
        }

        return (<TouchableOpacity style = {[styles.imageContainer,imageStyle]} onPress = {()=>this.props.navigation.navigate('Image',{url: url})} onLongPress = {this.onPress} >
            <Image style = {styles.image} source = {source} />
            <View style = {styles.imageDateContainer} >
                {(this.props.item.createdDate) ? (<Text style = {[styles.date,dateStyle]}>{DateUtil.format(this.props.item.createdDate)}</Text>) : null}
            </View>
            <ActivityIndicatorView animating = {this.state.uploading} />
            {(this.state.errors) ? (<RetryView message = {this.state.error} onPress = {this.uploadImage} />): null}

        </TouchableOpacity>)

    }


    uploadImage = async () => {

        let message =  this.props.item
        let content = JSON.parse(message.content)
        let file = content.file

        let uri = Platform.OS == "android" ? file.path : file.uri

        if(!uri){
            return
        }
        let exists = false
        try{

            exists = await RNFS.exists(uri)
        }catch (e) {
            console.log('exists error',e)
        }

        if(!exists){
            return
        }

        this.setState({ uploading: true });

        let formdata = new FormData();
        formdata.append("fileName", file.fileName ?? 'fileName')
        formdata.append("fileType", 'MINU_CHAT')
        formdata.append("fileFolder", '')
        formdata.append("file", {
            uri: file.uri,
            name: file.fileName ?? 'fileName',
            type: file.type
        })


        axios.request( {
            url: Constants.CDN_URL + "u",
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            data: formdata,
            progress: (p) => {
                console.log('axio progress',p);
            }
        }).then (response => {


            if(response.data.responseResultType == 'SUCCESS'){

                let data = {
                    id: message.id,
                    type: message.type,
                    roomId: message.roomId,
                    fromUserId: message.fromUserId,
                    content: JSON.stringify({cdnId: response.data.data, ratio: content.ratio})
                }

                // console.log('upload data',data)

                this.props.adapter.send(data)

            }else{

                this.setState({error: response.data.message.message})
            }
            this.setState({ uploading: false })

        }).catch(e => {
            this.setState({ uploading: false })
            this.setState({error: 'Алдаа гарлаа.'})
            this.setState({ uploading: false })
        })
    }

    confirmToWithdraw = async (password) => {

        this.setState({isLoading: true})

         let params = {
             id: this.props.item.typeId,
             userId: await AsyncStorage.getItem('userId'),
             password: password,
             optionChatId: this.props.item.id,
         }


        fetch(Constants.WSURL + "wsock/payment/transferOption", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        }).then((response) => response.json())
            .then((json) => {


                if(json.status == "000") {

                    Toast.show('Илгээлээ');

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                }

                this.setState({isLoading: false})

            }).catch((error) => {

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false})
        });
    }

    confirmToCancel = async (password) => {

        this.setState({isLoading: true})

        let params = {
            id: this.props.item.typeId,
            userId: await AsyncStorage.getItem('userId'),
            password: password,
            optionChatId: this.props.item.id,
        }


        fetch(Constants.WSURL + "wsock/payment/reverseOption", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        }).then((response) => response.json())
            .then((json) => {


                if(json.status == "000") {

                    Toast.show('Илгээлээ');

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                }

                this.setState({isLoading: false})

            }).catch((error) => {

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false})
        });
    }

    confirmToCancelContract = async (password) => {

        this.setState({isLoading: true})

        let params = {
            roomId: this.props.item.roomId,
            userId: await AsyncStorage.getItem('userId'),
            password: password,
            optionChatId: this.props.item.id,
        }

        fetch(Constants.WSURL + "wsock/contract/cancelRequest", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        }).then((response) => response.json())
            .then((json) => {

                console.log('contract cancel',json)

                if(json.status == "000") {

                    Toast.show('Цуцаллаа');

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                }

                this.setState({isLoading: false})

            }).catch((error) => {

            console.log('contract cancel error',error)

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false})
        });
    }


    sendAmount = async (amount,type,description) => {

        this.setState({isLoading: true})

        let params = {
            roomId: this.props.item.roomId,
            userId: await AsyncStorage.getItem('userId'),
            amount: amount,
            type: type,
            description: description,
            // redirectUri: "https://chat.minu.mn"
            optionChatId: this.props.item.id,
        }

        console.log('send amount param',params)

        fetch(Constants.WSURL + "wsock/payment/createInvoice", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        }).then((response) => response.json())
            .then((json) => {


                if(json.status == "000") {

                    Toast.show('Илгээлээ');

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                }

                this.setState({isLoading: false})

            }).catch((error) => {

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false})
        });
    }

    deleteMessage = () => {

        bottomModal.close()

        CustomAlert.confirm("Устгах","Мессэжийг устгах уу?",(response)=>{

            if(response){

                let message =  this.props.item

                let data = {
                    id: message.id,
                    type: MessageTypes.DELETE,
                    roomId: message.roomId,
                    fromUserId: this.props.userId,
                }

                this.props.adapter.send(data)

            }

        })
    }

    copyMessage = () => {

        Clipboard.setString(this.props.item.content)
        Toast.show('Хууллаа')
        bottomModal.close()
    }

    replyMessage = () => {

        this.props.chatScreen.reply(this.props.item)
        bottomModal.close()
    }

    forwardMessage = () => {

        this.props.navigation.navigate('Forward',{message: this.props.item, messageAdapter: this.props.chatScreen.messageAdapter})
        bottomModal.close()
    }

    isReplyable = () => {

        let message = this.props.item

        return  message.type == MessageTypes.CHAT || message.type == MessageTypes.IMAGE
    }

    isDeletable = () => {

        let message = this.props.item

        return  message.fromUserId == this.props.userId && (message.type == MessageTypes.CHAT || message.type == MessageTypes.IMAGE)
    }

    isForwardable = () => {

        let message = this.props.item

        return  message.type == MessageTypes.CHAT || message.type == MessageTypes.IMAGE
    }

    onPress = () => {


        bottomModal.open(<View style = {styles.actionContainer} >
            { this.isReplyable() ?
                (<View style = {{flex:1}}>
                    <ToolButton icon= "reply" title = "Хариулах" onPress = {this.replyMessage} />
                </View>) : null}
            <View style = {{flex:1,}}>
                <ToolButton cicon= "content-copy" title = "Хуулах" onPress = {this.copyMessage} />
            </View>
            { this.isForwardable() ?
                (<View style = {{flex:1, marginLeft: 8}}>
                    <ToolButton icon= "forward" title = "Илгээх" onPress = {this.forwardMessage} />
                </View>) : null}
            { this.isDeletable() ?
                (<View style = {{flex:1, marginLeft: 8}}>
                    <ToolButton icon= "delete" title = "Устгах" onPress = {this.deleteMessage} />
                </View>) : null}
        </View>)

    }
}


const styles = StyleSheet.create({

    container: {
    },

    dateContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
        // backgroundColor: 'gray'
    },

    date: {
        fontFamily: 'Roboto-Regular',
        color: '#000',
        fontSize: 10,
        margin: 8,
        marginLeft: 16,
        marginTop: -4,
        marginBottom: 4,
    },

    textContainer: {
        flexDirection: 'row',
        // alignItems: 'flex-end'
    },

    buble: {
        // flex:1,
        backgroundColor: '#E4EAF1',
        // borderRadius: 18,
        borderRadius: 6,
        minHeight: 36,
        justifyContent: 'center',
        // margin: 8,
        marginTop: 0,
        overflow: 'hidden',
    },

    bubleDeleted: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#999',
        borderRadius: 6,
        minHeight: 36,
        justifyContent: 'center',
        marginTop: 0,
        overflow: 'hidden',
    },

    imageContainer: {
        backgroundColor: '#E4EAF1',
        borderRadius: 6,
        justifyContent: 'center',
        marginTop: 0,
        overflow: 'hidden',
        width: Math.floor(windowWidth/2)
    },

    name: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#333333',
        fontSize: 12,
        margin: 8,
        marginBottom: -4,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
        margin: 8,
        // marginLeft: 8,
        // marginRight: 8,
    },

    deleted: {
        fontFamily: 'RobotoCondensed-Italic',
        color: '#999',
        fontSize: 16,
        margin: 8,
    },

    option:{
        backgroundColor: '#fff',
        borderRadius: 4,
        padding: 8,
        margin: 8,
        marginTop: 0,
    },

    buttonText: {
        fontFamily: 'Roboto-Medium',
        color: '#333333',
        fontSize: 14,
    },

    verticalLine:{
        backgroundColor: '#666666',
        width: StyleSheet.hairlineWidth,
    },

    image: {
        flex:1,
    },

    imageDateContainer:{
        position: 'absolute',
        right: 0,
        bottom: 0
    },

    actionContainer: {
        flexDirection: 'row'
    }
});
