import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity, ActivityIndicator,
} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import Cicon from "react-native-vector-icons/MaterialCommunityIcons";

export default class ToolButton extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            title: this.props.title,
            loading: this.props.loading,
            loadingText: this.props.loadingText,
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {

        if(nextProps.title != this.state.title){
            this.setState({title: nextProps.title})
        }

    }

    renderImage(){

        if(this.props.image == null){
            return null;
        }

        return (
            <Image
                source = {this.props.image}
            />);

    }


    renderIcon(){

        if(this.props.icon == null){
            return null;
        }

        let color = "#DB373D"

        if(this.props.iconColor){
            color = this.props.iconColor
        }

        if(this.props.gray){
            color = "#333333"
        }

        return (
            <Icon
                name = {this.props.icon}
                color = {color}
                style = {styles.icon}
                size = {this.props.iconSize ?? 32}
            />);

    }

    renderCicon(){

        if(this.props.cicon == null){
            return null;
        }

        let color = "#DB373D"
        // let color = "#FA7268"

        if(this.props.iconColor){
            color = this.props.iconColor
        }

        return (
            <Cicon
                name = {this.props.cicon}
                color = {color}
                style = {styles.icon}
                size = {this.props.iconSize ?? 32}
            />);

    }

    renderText(){

        if(this.state.title == null){
            return null;
        }

        let style = {}

        return (
            <Text
                style={[styles.text, this.props.titleStyle, style]}>
                {this.state.title}
            </Text>);

    }

    renderIndicator(){

        if(!this.state.loading){
            return null;
        }

        return (
            <View style = {styles.indicatorContainer}>

                {(this.state.loadingText) ? (<Text style = {styles.indicatorText} >{this.state.loadingText}</Text>) : null}

                <ActivityIndicator
                    animating={true}
                    hidesWhenStopped={true}
                    size = 'small'
                    color = 'white'
                    style = {styles.indicator}
                />
            </View>
        );

    }


    render() {

        let style = {}

        if(this.props.gray){
            style.backgroundColor = "#E0E0E0"
        }

        return (
            <View style = {[styles.container, this.props.style, style]} >
                <TouchableOpacity
                    onPress={this.props.onPress}
                    style={styles.touchableOpacity}
                    disabled={this.props.disabled}>
                    {this.renderImage()}
                    {this.renderIcon()}
                    {this.renderCicon()}
                    {this.renderText()}
                    <View style = {styles.childrenContainer} >
                        {this.props.children}
                    </View>
                </TouchableOpacity>
                {this.renderIndicator()}
            </View>
        );

    }

}


const styles = StyleSheet.create({

    container: {
        borderRadius: 8,
        backgroundColor: '#fff',
        height: 72,
        minWidth: 72,
        // paddingLeft: 8,
        // paddingRight: 8,
    },

    touchableOpacity: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    childrenContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    },

    text: {
        color: '#000',
        fontSize: 14,
        fontFamily: 'RobotoCondensed-Regular',
    },

    indicatorContainer: {
        backgroundColor: '#ffffff99',
        position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    },

    indicator: {
        marginRight: 16,
    },

    indicatorText: {
        color: '#fff',
        fontSize: 14,
        marginRight: 16,
    },

    icon: {
        marginBottom: 4,
    }

});
