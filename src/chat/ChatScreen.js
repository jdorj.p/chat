import React, { Component } from "react";
import {View, TouchableOpacity, Text, FlatList , StyleSheet, SafeAreaView,} from 'react-native';
import {ChatItem} from './ChatItem';
import Input from './Input';
import Button from '../components/Button';
import {ToolsView} from './ToolsView';
import ToolButton from './ToolButton';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import ChatUtil from './ChatUtil';
import {RoomUtil} from '../room/RoomUtil';
import {ProfileImage} from '../common/ProfileImage';
import HeaderButton from '../components/HeaderButton';
import 'react-native-get-random-values'
import Border from '../components/Border';
import ImagePicker from 'react-native-image-picker';
import MessageAdapter, {EventTypes, MessageTypes} from '../data/MessageAdapter';
import ChatBadge from './ChatBadge';
import ReplyingView from './ReplyingView';
import MentioningView from './MentioningView';

class ChatScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            room: props.route.params.room,
            roomId: props.route.params.roomId,
            messages: [],
            dataVersion: 0,
            isLoading: false,
            mentioningText: null,
        }


        AsyncStorage.getItem('userId',(error,userId)=>{

            // console.log('userId',userId)

            this.setState({userId: userId},()=>{

                let roomId
                if(this.state.room){
                    roomId = this.state.room.id
                }else{
                    roomId = this.state.roomId
                    this.getRoom(roomId)
                }

                this.initRoom(roomId)
                this.setRoomInfo()
            })
        })
    }


    initRoom = (roomId) => {

        if(!roomId){
            return
        }

        this.messageAdapter && this.messageAdapter.close()

        this.messageAdapter = new MessageAdapter(
            roomId,
            [],(data)=>{
                this.setState({messages: data})
            })

        this.messageAdapter.addListener(EventTypes.roomUpdated,(room)=>{

            this.setState({
                room:room
            },()=>this.setRoomInfo())
        })

        this.messageAdapter.addListener(EventTypes.roomChanged,(room)=>{

            this.setState({
                room:room
            },()=>{
                this.initRoom(this.state.roomId)
                this.setRoomInfo()
            })
        })

        this.messageAdapter.addListener(EventTypes.fetch,(started)=>{

            if(started){
                this.setState({status: 'Уншиж байна',statusColor: Constants.COLORS.INFO});
            }else{
                this.setState({status: null});
            }
        })

    }

    setRoom = (room) => {

        this.setState({room: room,messages: []},()=>{

            this.refs.toolsView.hide()
            this.initRoom(room.id)
            this.setRoomInfo()
        })

    }

    setRoomByUsers = (users) => {

        this.setState({room: undefined,messages: []},()=>{

            this.refs.toolsView.hide()
            this.getRoom(undefined,users)
            this.setRoomInfo()
        })

    }

    componentDidMount() {
        ChatUtil.chatScreen = this
    }

    componentWillUnmount(): void {
        this.messageAdapter.close()
    }

    setRoomInfo = async () => {

        let room = this.state.room

        if(room){
            let userId = await AsyncStorage.getItem("userId")
            let roomUtil = new RoomUtil(userId)
            this.setState({
                roomName: roomUtil.roomName(room),
                roomImage: roomUtil.roomImage(room)
            })
        }else{
            this.setState({roomName: "", roomImage: ""})
        }

    }


    getRoom = async (roomId,users = this.props.route.params.users) => {

        this.setState({isLoading: true});
        let data
        let uri

        if(roomId){
            uri = "wsock/room/findById"
            data = {
                id: roomId,
                userId: await AsyncStorage.getItem('userId'),
            }

        }else{

            uri = "wsock/room"
            let userIdList = users.map((user) => user.userId)

            let userId = await AsyncStorage.getItem("userId");
            userIdList.push(userId)

            data = {
                userIdList: userIdList,
                userId: userId
            }
        }

        fetch(Constants.WSURL + uri, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {

                console.log('room',json)

                this.setState({
                    isLoading: false,
                    room: json.entity,
                },()=>{
                    this.setRoomInfo()
                    this.initRoom(this.state.room.id)
                })

            }).catch((error) => {

            console.log(error)

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false});
        });
    }


    sendMessage = () => {

        let message = this.refs.input.getValue()

        if(message) {

            this.messageAdapter.add({
                content: message.trim(),
                contentObjects: this.refs.input.getObjectValue(),
                type: MessageTypes.CHAT,
                new: true,
                replyId: this.state.replyingMessage ? this.state.replyingMessage.id : null
            })

            this.refs.input.setValue("")
        }

        this.setState({replyingMessage: undefined})

    }


    startContract = () => {
        this.messageAdapter.add({
            content: "Хэлцэл хийх үү?",
            type: MessageTypes.CONTRACT_REQUEST,
            new: true,
        })
    }

    endContract = () => {

        this.messageAdapter.add({
            content: "Хэлцэл дуусгах",
            type: MessageTypes.CONTRACT_END_REQUEST,
            new: true,
        })
    }

    contracts = () => {
        let withUserIds = this.state.room.userIdList.filter((id)=> id != this.state.userId)
        this.props.navigation.navigate('Contracts',{withUserId: withUserIds[0]})
    }

    showTools = () => {
        let showed = this.refs.toolsView.show(this.refs.input)
        this.setState({toolsShowed: showed})
    }

    getNextItem = (currentIndex) => {
        if((currentIndex + 1) < this.state.messages.length){
            return this.state.messages[currentIndex + 1]
        }
        return undefined
    }

    onEndReached = ({ distanceFromEnd }) => {

        // console.log('distanceFromEnd',distanceFromEnd)
        // if(!this.gettingMessage && !this.reachedToEnd){
            // console.log("onEndReached getMessages")
            // this.getMessages(false)
        // }
        this.messageAdapter.nextData()
    }

    isGroup = () => {
        if(this.state.room){
            return this.state.room.isGroup
        }else{
            return false
        }
    }

    showGiftCard = () => {
        if(this.state.room && this.state.userId){
            let roomUtil = new RoomUtil(this.state.userId)
            let users = roomUtil.otherUsers(this.state.room.userList)

            console.log('showGiftCard users',users)

            if(users.filter(user => user.userId == "2").length > 0){
                return true
            }
        }

        return false
    }

    giftCard = () => {

        this.messageAdapter.add({
            content: "Бэлгийн карт үүсгэх",
            type: MessageTypes.GIFTCARD,
            new: true,
        })
    }

    chatInfo = async () => {

        if(this.isGroup()){
            this.props.navigation.navigate('ChatInfo',{room: this.state.room})
        }else{

            let userId = await AsyncStorage.getItem("userId")
            let roomUtil = new RoomUtil(userId)
            let users = roomUtil.otherUsers(this.state.room.userList)
            this.props.navigation.navigate('Profile',{user: users[0],room: this.state.room, messageAdapter: this.messageAdapter})
        }

    }

    pickImage = () => {

        ImagePicker.launchImageLibrary({}, (response) => {

            if(response.error){
                CustomAlert.alert("Алдаа гарлаа!","Зургийн санд хандах эрхгүй байна.", 'error');
            }

            if (response.uri) {
                this.sendImage({...response,data:null})

            }

        });
    }

    sendImage = (image) => {

        console.log('picked image',image)

        let ratio

        if(Platform.OS == 'android'){
            ratio = image.isVertical ? (image.width / image.height) : (image.height / image.width)

        }else{
            ratio = image.width / image.height
        }

        let message = {
            type: MessageTypes.IMAGE,
            content: JSON.stringify({
                ratio: ratio,
                file: {
                    path: image.path,
                    uri: image.uri,
                    fileName: image.fileName,
                    type: image.type,
                },
            }),
        }

        this.messageAdapter.add(message,false)
    }

    reply = (message) => {

        this.setState({
            replyingMessage: message
        })
    }

    replyCancel = () => {

        this.setState({
            replyingMessage: undefined
        })
    }

    scrollToChatItem = (chatId) => {

        let scrollIndex

        this.state.messages.map((chat,index)=>{

            if(chat.id == chatId){
                scrollIndex = index
            }
        })

        if(scrollIndex != undefined){
            this.refs.flatList.scrollToIndex({animated: true,index:scrollIndex, viewPosition: 0.5})
        }
    }

    mention = (user)=> {

        this.refs.input.mention(user)
    }

    render() {

        return (
            <SafeAreaView style = {{flex:1, backgroundColor: '#fff',}} >
                <View style = {styles.header} >
                    <HeaderButton icon = "keyboard-arrow-left" iconSize = {32}  onPress={()=>this.props.navigation.goBack()} style = {{marginLeft: 0, paddingTop: 4, paddingBottom: 4}}  />
                    <View style = {{flexDirection: 'row',flex:1, alignItems: 'center'}} >
                        <TouchableOpacity onPress = {this.chatInfo} style = {{flexDirection: 'row',alignItems: 'center'}} >
                            <ProfileImage disabled size = {32} user = {this.state.roomImage}  />
                            <Text style = {styles.title} >{this.state.roomName}</Text>
                        </TouchableOpacity>
                    </View>
                    <HeaderButton icon = "more-vert"   onPress = {this.chatInfo} >
                        <ChatBadge room = {this.state.room} style = {{position:'absolute',top: 4, left: -12}} />
                    </HeaderButton>
                </View>
                <Border/>

                {(this.state.status) ? (<View style = {[styles.statusContainer,{backgroundColor: this.state.statusColor}]} ><Text style = {styles.status} >{this.state.status}</Text></View>) : null}
                <FlatList
                    style = {styles.flatList}
                    ref = "flatList"
                    inverted
                    data={this.state.messages}
                    extraData = {this.state.dataVersion}
                    onEndReached = {this.onEndReached}
                    onEndReachedThreshold = {0.5}
                    keyExtractor={(item, index) => {
                        return item.id
                    }}
                    renderItem={({item, index}) =>{
                        this.messageAdapter.sendSeen(item)
                        // console.log('chat id',item.id)
                        return (<ChatItem
                            // key = {item.id}
                            userId={this.state.userId}
                            nextItem = {this.getNextItem(index)}
                            isLast = {index == 0}
                            item = {item}
                            adapter = {this.messageAdapter}
                            isGroup = {this.isGroup()}
                            navigation = {this.props.navigation}
                            chatScreen = {this}
                            index = {index}
                        />)
                    }}
                />

                <View style = {{backgroundColor: '#E4EAF1'}} >
                    {this.state.replyingMessage ? <ReplyingView message = {this.state.replyingMessage} cancel = {this.replyCancel} /> : null}
                    {this.isGroup() && this.state.mentioningText != null ? <MentioningView userList = {this.state.room.userList} onSelect = {this.mention} keyword = {this.state.mentioningText} userId={this.state.userId} /> : null}

                    <View style = {styles.inputContainer}>
                        <Button icon = { this.state.toolsShowed ? "close" : "add"} noShadow iconSize = {24} style = {styles.buttonLeft} onPress = {this.showTools} />
                        <Input ref = "input" style = {styles.input} multiline onFocus = {(e)=>this.refs.toolsView.onFocus(e)} onMentioning = {(text)=>this.setState({mentioningText: text}) } room = {this.state.room} />
                        <Button icon = "send" noShadow style = {styles.button} onPress = {this.sendMessage} />
                    </View>
                </View>
                <ToolsView ref = "toolsView" >

                    {(!this.isGroup()) ? (
                        <View style = {{flexDirection: 'row', marginTop: 8,}} >
                            <View style = {{flex:1}}>

                                {this.showGiftCard() ? <ToolButton style = {{flex:1}} cicon= "wallet-giftcard" title = "Бэлгийн карт" onPress = {this.giftCard} /> : <ToolButton cicon= "information" title = "Хэлцэл гэж юу вэ?" onPress = {ChatUtil.aboutContract} />}

                            </View>
                            <View style = {{flex:1, marginLeft: 8}}>
                                <ToolButton cicon= "file-document" title = "Хэлцэлүүд" onPress = {this.contracts} />
                            </View>
                            <View style = {{flex:1, marginLeft: 8}}>
                                <ToolButton icon= "account-balance" title = "Эскроу данс" onPress = {()=>this.props.navigation.navigate('Escrow',{roomId: this.state.room.id, receiverName: this.state.roomName})} />
                            </View>
                            {/*<View style = {{flex:1, marginLeft: 8}} />*/}
                        </View>
                    ) : null}

                    <View style = {{ flexDirection: 'row' }} >
                        <ToolButton style = {{flex:1}} icon= "photo" title = "Зураг илгээх" onPress = {this.pickImage} />
                        {(!this.isGroup()) ? (<ToolButton style = {{flex:1,  marginLeft: 8}} cicon= "file-document-edit" title = "Хэлцэл эхлэх" onPress = {this.startContract} />) : (<View style = {{flex:1, marginLeft: 8}} />)}
                        {(!this.isGroup()) ? (<ToolButton style = {{flex:1, marginLeft: 8}} cicon= "handshake" title = "Хэлцэл дуусгах" onPress = {this.endContract} />) : (<View style = {{flex:1, marginLeft: 8}} />)}
                    </View>


                </ToolsView>
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}

// export default withStomp(ChatScreen)
export default ChatScreen

const styles = StyleSheet.create({

    container: {
        flex:1,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 8,
        backgroundColor: '#fff',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 22,
        // textAlign: 'center',
        marginLeft: 8,
    },

    statusContainer: {
        padding: 8,
        alignItems: 'center',
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        margin: 8,
        marginRight: 0,
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },


});
