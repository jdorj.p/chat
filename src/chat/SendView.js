import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
// import Icon from "react-native-vector-icons/MaterialIcons";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export class SendView extends Component {

    constructor(props){
        super(props);

        this.state = {
        }
    }

    componentDidMount() {

    }


    render() {

        return (
            <View style = {[styles.container,this.props.style]} >
                {(this.props.sent) ? (<Icon name = "check-bold" color = "#999999" size = {8} />) : null}
            </View>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        height: 12,
        width: 12,
        borderRadius: 6,
        borderWidth: 1.2,
        borderColor: '#999999',
        justifyContent: 'center',
        alignItems: 'center'
    },




});
