import React from "react";
import {StyleSheet, Text, View, Image} from 'react-native';
import HeaderButton from '../components/HeaderButton';
import {MessageTypes} from '../data/MessageAdapter';
import Constants from '../common/Constants';

const IMAGE_WIDTH = 70

export default function ReplyingView({message, cancel}){

    function getSource(){

        let source

        let content = JSON.parse(message.content)

        let style = {
            aspectRatio: content.ratio ?? 1
        }

        if(content.cdnId){
            source = {uri: Constants.CDN_URL + 'img/' + content.cdnId + '?w=' + IMAGE_WIDTH * 2}
        }else if(content.file && content.file.uri){

            let file = content.file
            source = {uri: file.uri}
        }

        console.log('message',message)
        console.log('source',source)

        return {style,source}
    }

    return <View style = {styles.container} >
        <View>
            <Text style = {styles.name} >{message.fromUser.name}-рүү хариулж байна</Text>
            <Text style = {styles.message} >{ message.type == MessageTypes.IMAGE ? 'Зураг' : message.content}</Text>
        </View>

        <View style = {{flexDirection: 'row'}} >
            {message.type == MessageTypes.IMAGE ?
                <View style = {[styles.imageContainer,getSource().style]} >
                    <Image style = {styles.image} source = {getSource().source} />
                </View>
                : null}
            <HeaderButton icon = "close" iconSize = {20} iconColor = {'#000'} onPress = {cancel} style = {styles.close}  />
        </View>
    </View>
}

const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        padding: 8,
        justifyContent: 'space-between'
    },

    name:{
        fontFamily: 'RobotoCondensed-Bold',
        color: '#333333',
        fontSize: 12,
    },

    message:{
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
        marginTop: 8,
    },

    imageContainer: {
        backgroundColor: '#666',
        height: 40,
        marginRight: 8,
    },
    image:{
        flex:1,
    },

    close: {
        height: 24,
        width: 24,
        justifyContent: 'center',
    }
})
