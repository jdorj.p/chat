import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native';
import Util from '../common/Util';

export const MENTION_REGEX = /@([\w\d.\-_]+)?$/

export default class Input extends React.Component {


    constructor(props) {
        super(props);

        console.log('input constructor')

        if(this.props.value){
            this.state = {
                value: this.props.value,
                text: this.props.value,
                blurred: true}
        }


        this.state = {
            value: this.props.value,
            text: this.props.value,
            selectedValue: this.props.selectedValue,
            secureTextEntry: this.props.secureTextEntry,
            pattern: this.props.pattern,
            mentionText: null,
        };

    }

    componentDidMount(): void {


    }

    mentions = []


    userName = (user) => {
        let name = "@" + (user.lastName ? (user.lastName + " ") + user.name : "")
        let joinedName = "@" + (user.lastName ? (user.lastName) + user.name : "")
        return {name,joinedName}
    }

    mention = (user) => {

        let text = this.state.value

        // let matches = text.match(MENTION_REGEX)

        // @ urdaa zaita bga esehiig shalgana
        if(text.match(/\s@([\w\d.\-_]+)?$/)){
            text = text.replace(MENTION_REGEX,this.userName(user).name + " ")
        }else{
            text = text.replace(MENTION_REGEX," " + this.userName(user).name + " ")
        }

        // console.log('mention text',text)
        let formattedText = this.formatText(text)
        // console.log('formatted text',formattedText)

        this.setState({
            text: formattedText,
            value: text + " ",
        })

        this.props.onMentioning(null)

    }

    isMention = (word) =>{

        let users = this.props.room.userList

        let result = false
        let realName
        let userId

        users.map(user=>{

            let {name,joinedName} = this.userName(user)

            if(word.trim() == joinedName){
                result = true
                realName = name
                userId = user.userId
            }

        })

        return {result,realName,userId}

    }

    formatText = (value) => {

        // return value
        if(this.props.room && this.props.room.userList){

            let users = this.props.room.userList

            users.map(user=>{

                let {name,joinedName} = this.userName(user)
                value = value.replace(new RegExp(name,'g'),joinedName)

            })

            // console.log('format text',value)

            let words = value.split(/\s/)

            this.objectValue = words.map((word,index)=>{

                let {result,realName,userId} = this.isMention(word)
                let text = (result ? realName : word)

                return {text:text, userId: userId}
            })

            console.log('object value',this.objectValue)

            return words.map((word,index)=>{

                let {result,realName} = this.isMention(word)

                let text = (result ? realName : word)
                text += (index == words.length - 1) ? "" : " "

                // console.log('text',text)

                return <Text style = {result ? styles.mention : null} >{text}</Text>
            })

        }else{
            return value
        }

        // let words = value.split(" ")
        //
        // let formatedWords = words.map((word,index)=>{
        //     return <Text style = {isMention(index) ? styles.mention: {}}>{word} </Text>
        // })
        //
        // return formatedWords
    }

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {

        if(nextProps.pattern != this.props.pattern){
            this.setState({pattern: nextProps.pattern})
        }
    }

    setValue = (value) => {

        this.setState({value: value, text: value})
    }

    getValue = () => {

        // сонголт хийдэг эсэхийг шалгана
        if(this.props.navigation ) {
            return this.state.selectedValue
        }else{
            return this.state.value
        }

    }

    getObjectValue = () => {

        let haveMention = false

         this.objectValue.map(value=>{
             if(value.userId){
                 haveMention = true
             }
        })

        if(haveMention){
            return JSON.stringify(this.objectValue)
        }else{
            return null
        }
    }



    _onChangeText = async (text) => {

        // console.log('text',text)

        let matches = text.match(MENTION_REGEX)

        this.props.onMentioning(matches ? (matches[1] ? matches[1] : "") : null)

        // console.log('matches',matches)

        if(text.length > 0 && text[text.length - 1] == '@'){

            // console.log('mentioning')
        }

        if(this.props.pan){

            text = Util.formatPan(text)

        }else if(this.props.expireDate){

            text = Util.formatExpireDate(text)

        }else if(this.props.tokenizeAmount){

            text = Util.formatTokenizeAmount(text)
        }

        await this.setState({
            text: this.formatText(text),
            value: text,
        })
        // this.validate()
        this.props.onChangeText && this.props.onChangeText(text)

    }

    _onBlur = () => {

        this.props.onBlur && this.props.onBlur()
        this.setState({blurred: true})
    }

    _onFocus = () => {

        this.props.onFocus && this.props.onFocus()

        // сонголт хийдэг эсэхийг шалгана
        if(this.props.navigation ){

            // initialRouteName сонголт хийхэд гарч ирэх дэлгэцийн route нэр.
            // Өөрөөр хэлбэл SelectListScreen доторх stackNavigation дотор байгаа дэлгэцүүдийн route гэсэн үг.
            // Шинээр сонголт хийдэг дэлгэц нэмэх бол тэнд нэмж өгнө
            this.props.navigation.navigate('select',{initialRouteName: this.props.initialRouteName, sendData: this.props.sendData, callback: this.select, value: {value: this.state.selectedValue}})
        }

        this.setState({blurred: false})
    }

    // value-н бүтэц иймэрхүү байна {text: selectedText, value: selectedId}
    select = (value,data) => {

        this.setState({value: value.text, selectedValue: value.value}, () => {
            this.validate();
        })

        // onSelected callback дамжуулсан байвал дуудна
        this.props.onSelected && this.props.onSelected(value,data)
        this.props.onChangeText?.(value.text);
    }

    focus = () => {

        this.refs.textInput.focus()

    }


    onLayout = (event) => {
        const layout = event.nativeEvent.layout;
        this.y = layout.y
    }


     render() {

        return (
            <View style = {[styles.container,this.props.style]} onlayout = {this.onLayout} >
                <TextInput
                    ref="textInput"
                    style = {styles.textInput}
                    onChangeText={this._onChangeText}
                    onBlur = {this._onBlur}
                    onFocus = {this._onFocus}
                    selectedValue={this.state.selectedValue}
                    autoFocus = {this.props.autoFocus}
                    selectionColor = "#000"
                    multiline = { !this.props.secureTextEntry && this.props.multiline}
                    secureTextEntry = {this.state.secureTextEntry}
                    scrollEnabled = {this.props.scrollEnabled}
                    keyboardType = {this.props.keyboardType}
                    placeholder = {this.props.placeholder != null ? this.props.placeholder : this.props.label}
                    returnKeyType = {this.props.returnKeyType}
                    onSubmitEditing={this.props.onSubmitEditing}
                    blurOnSubmit={false}
                    autoCapitalize = {this.props.autoCapitalize}
                    editable = {this.props.editable}
                    maxLength = {this.props.maxLength}
                >
                    <Text>{this.state.text}</Text>
                </TextInput>
            </View>
        )
    }

}



const styles = StyleSheet.create({

    container: {
        // justifyContent: 'center',
        borderRadius: 8,
        minHeight: 36,
        paddingTop: 4,
        paddingBottom: 6,
        paddingLeft:8,
        paddingRight: 8,
    },

    textInput: {
        color: '#000',
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        minHeight: 24,
        padding: 0,
        // backgroundColor: 'gray'
    },

    mention: {
      color: 'blue'
    },

});
