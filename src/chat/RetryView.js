import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import * as Style from '../common/Style';
export default class RetryView extends React.Component {

    render() {


        let backgroundColor = (this.props.light) ? 'transparent' : 'rgba(0,0,0,0.6)'

        if(this.props.noBackground){
            backgroundColor = 'transparent'
        }

        let color = (this.props.light) ? '#666666' : '#fff'
        let textColor = (this.props.light) ? '#000' : '#fff'

        return (
            <View style = {[styles.container, {backgroundColor: backgroundColor}]}>

                <View style = {{justifyContent: 'center'}} >

                    {(this.props.message) ? (<Text style = {[styles.title,{color:textColor}]} >{this.props.message}</Text>) : null}

                    <View style = {{flexDirection: 'row', alignItems: 'center'}} >
                        <TouchableOpacity
                            onPress={this.props.onPress}
                            style={styles.touchableOpacity}>
                            <Icon
                                name = "refresh"
                                color = {color}
                                size = {24}
                            />
                        </TouchableOpacity>
                        <Text style = {[styles.text,{color:textColor}]} >Дахин хуулах</Text>
                    </View>

                </View>
            </View>
        );
    }

}


const styles = StyleSheet.create({

    container:{
        flex: 1,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    },

    touchableOpacity: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 6,
        backgroundColor: Style.COLORS.icon,
        borderRadius: 6,
    },

    text: {
        marginLeft: 8,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
    },

    title: {
        margin: 8,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
    }

});
