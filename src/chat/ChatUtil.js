import PushNotificationIOS from '@react-native-community/push-notification-ios'
import {Platform} from 'react-native';
import CustomAlert from '../common/CustomAlert';
import UserUtil from '../common/UserUtil';
import AsyncStorage from '@react-native-community/async-storage';

export default class ChatUtil {

    static chatScreen

    static getMessageIndex = (messages, messageId) => {

        for(let i = 0; i < messages.length; i++){

            if(messages[i].id == messageId){
                return i
            }
        }

        return -1;
    }

    static updateMessage = (messages, message) => {

        let index = ChatUtil.getMessageIndex(messages, message.id)

        if(index){
            let type = messages[index].type
            messages[index] = message
            messages[index].type = type
        }

        return messages
    }

    static updateSeenUserList = (messages, message) => {

        let index = ChatUtil.getMessageIndex(messages, message.id)

        console.log('update seen list',index)

        if(index >= 0){
            messages[index].seenUserList = message.seenUserList
            console.log(messages[index])
            console.log(message.seenUserList)
        }

        return messages
    }

    static handleMessage = (messages, message) => {


        if(message.type == "SEEN"){

            messages = ChatUtil.updateSeenUserList(messages, message)

        }else if (message.type == "UPDATE"){

            messages = ChatUtil.updateMessage(messages, message)

        }else{

            let index = ChatUtil.getMessageIndex(messages, message.id)

            if(index == -1){

                if(messages.length >= 40){
                    return [message].concat(messages.slice(0,38))
                }else{
                    return [message].concat(messages)
                }
            }else{

                // messages[index].new = false
                // console.log(message)
                messages[index] = message
            }

        }

        return messages
    }


    static setBadgeNumber = async (rooms) => {

        let number = await ChatUtil.getUnreadRoomsNumber(rooms)
        if(Platform.OS == 'ios') {
            PushNotificationIOS.setApplicationIconBadgeNumber(number);
        }
    }

    static getUnreadRoomsNumber =  async (rooms) => {

        let number = 0

        let userId = await AsyncStorage.getItem('userId')

        for(let i = 0 ; i < rooms.length; i ++){
            let room = rooms[i]

            if(!ChatUtil.isSeen(room.lastChatMessage,userId)){
                number += 1
            }
        }

        return number
    }

    static aboutContract = () => {

        CustomAlert.confirm("Хэлцэл хийх үү?","Хэлцэл модуль нь хэрэглэгч хоорондын тохиролцоог Blockchain технологид суурилсан Smart Contract хэлбэрт хувирган хадгална. Blockchain технологийн онцлог нь өгөгдлийг аль нэг тал бие даан солих, өөрчлөх боломжгүй бөгөөд өнөөгийн ертөнц дэхь мэдээллийг хадгалах хамгийн найдвартай аргад тооцогддог.",(response)=>{

            if(response){


            }

        })
    }

    static isSeen = (message,userId,seenDictionary) => {

        if(seenDictionary && seenDictionary[message.id]){
            return true
        }

        if(!message){
            return true
        }

        if(!message.seenUserList){
            return false
        }

        for(let i = 0; i < message.seenUserList.length; i++){

            let user = message.seenUserList[i]

            if(user && user.userId == userId){
                return true
            }
        }

        return false;
    }

    static otherUsers = (userList, userId) => {

        if(!userList){
            return []
        }

        return userList.filter((user)=> user.rpUserId != userId)
    }

}
