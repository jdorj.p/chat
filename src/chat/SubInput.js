import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import Util from '../common/Util';

export default class SubInput extends React.Component {

    constructor(props) {
        super(props);


        if(this.props.value){
            this.state = {value: this.props.value, blurred: true}
        }


        this.state = {
            value: this.props.value,
            selectedValue: this.props.selectedValue,
            secureTextEntry: this.props.secureTextEntry,
            pattern: this.props.pattern
        };

    }

    componentDidMount(): void {


    }


    setValue = (value) => {

        this.setState({value})
    }

    getValue = () => {

        // сонголт хийдэг эсэхийг шалгана
        if(this.props.navigation ) {
            return this.state.selectedValue
        }else{
            return this.state.value
        }


    }


    _onChangeText = async (text) => {

        if(this.props.pan){

            text = Util.formatPan(text)

        }else if(this.props.expireDate){

            text = Util.formatExpireDate(text)

        }else if(this.props.tokenizeAmount){

            text = Util.formatTokenizeAmount(text)
        }

        await this.setState({value: text})
        this.props.onChangeText && this.props.onChangeText(text)

    }

    _onBlur = () => {

        this.props.onBlur && this.props.onBlur()
        this.setState({blurred: true})
    }

    _onFocus = () => {

        this.props.onFocus && this.props.onFocus()

        this.setState({blurred: false})
    }

    focus = () => {

        this.refs.textInput.focus()

    }

    onLayout = (event) => {
        const layout = event.nativeEvent.layout;
        this.y = layout.y
    }


    render() {

        return (
                <TextInput
                    ref="textInput"
                    style = {[styles.textInput,this.props.style]}
                    onChangeText={this._onChangeText}
                    onBlur = {this._onBlur}
                    onFocus = {this._onFocus}
                    value={this.state.value}
                    selectedValue={this.state.selectedValue}
                    autoFocus = {this.props.autoFocus}
                    selectionColor = "#000"
                    multiline = { !this.props.secureTextEntry && this.props.multiline}
                    secureTextEntry = {this.state.secureTextEntry}
                    scrollEnabled = {this.props.scrollEnabled}
                    keyboardType = {this.props.keyboardType}
                    placeholder = {this.props.placeholder != null ? this.props.placeholder : this.props.label}
                    returnKeyType = {this.props.returnKeyType}
                    onSubmitEditing={this.props.onSubmitEditing}
                    blurOnSubmit={false}
                    autoCapitalize = {this.props.autoCapitalize}
                    editable = {this.props.editable}
                    maxLength = {this.props.maxLength}
                />
        )
    }

}



const styles = StyleSheet.create({

    container: {
        // justifyContent: 'center',
        borderRadius: 8,
        minHeight: 36,
        paddingTop: 4,
        paddingBottom: 6,
        paddingLeft:8,
        paddingRight: 8,
    },

    textInput: {
        color: '#000',
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        minHeight: 24,
        padding: 0,
        // backgroundColor: 'gray'
    },

});
