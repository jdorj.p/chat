import React from "react";
import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';
import colors from '../shared/color';
import {ProfileImage} from '../common/ProfileImage';

const MAX_SHOW = 3
const ROW_HIEGHT = 48

export default function MentioningView({userList, keyword, onSelect, userId}){

    console.log('keyword',keyword)

    function renderUser({ item }){

        // console.log('user',item)

        return (
                <TouchableOpacity
                    style={{
                        backgroundColor: colors.bgGray,
                        paddingTop: 8,
                        paddingBottom: 8,
                        paddingHorizontal: 8,
                        height: ROW_HIEGHT,
                    }}

                    onPress = {()=>{ onSelect(item) }}
                >
                    <View
                        style={{ flex: 1, flexDirection: 'row' }}
                    >
                        <ProfileImage url = {item.avatar} size = {32} />

                        <View style={{ flex: 1 , marginLeft: 8, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                            <View>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            lineHeight: 24,
                                            fontFamily: 'RobotoCondensed-Regular',
                                            color: '#000',
                                            fontSize: 16,
                                        }}>
                                        {item.lastName} {item.name}
                                    </Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
        );
    };

    function getData() {

        return userList.filter(user=>{

            if(user.userId == userId){
                return false
            }
            let name = user.lastName ? (user.lastName.toLowerCase()) + " "  : "" + user.name.toLowerCase()
            return name.startsWith(keyword.toLowerCase())

        })
    }

    return <View style = {[styles.container,{height: getData().length > MAX_SHOW ? MAX_SHOW * ROW_HIEGHT : getData().length * ROW_HIEGHT}]} >
        <FlatList
            keyboardShouldPersistTaps = 'handled'
            data={getData()}
            keyExtractor={item => item.userId}
            renderItem={renderUser}
            style={{ flex: 1 }}
        />
    </View>
}

const styles = StyleSheet.create({

    container: {
    },

    name:{
        fontFamily: 'RobotoCondensed-Bold',
        color: '#333333',
        fontSize: 12,
    },

    message:{
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
        marginTop: 8,
    },

    imageContainer: {
        backgroundColor: '#666',
        height: 40,
        marginRight: 8,
    },
    image:{
        flex:1,
    },

    close: {
        height: 24,
        width: 24,
        justifyContent: 'center',
    }
})
