import React, { Component } from "react";
import {View, Text, FlatList , StyleSheet, SafeAreaView,} from 'react-native';
import {ChatItem} from './ChatItem';
import {StompEventTypes, stompContext } from './Stomp'
import Input from './Input';
import Button from '../components/Button';
import {ToolsView} from './ToolsView';
import ToolButton from './ToolButton';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import ChatUtil from './ChatUtil';
import {RoomUtil} from '../room/RoomUtil';
import {ProfileImage} from '../common/ProfileImage';
import HeaderButton from '../components/HeaderButton';
import 'react-native-get-random-values'
import { v4 as uuid } from "uuid";
import Border from '../components/Border';
import ImagePicker from 'react-native-image-picker';
import TopNotification from './TopNotification';
import Message from '../data/Message'
import Room from '../data/Room';

 class ChatScreen extends Component {

    constructor(props){
        super(props);

        props.navigation.setOptions({
            headerShown: false,
        })

        Message.open()

        this.state = {
            room: props.route.params.room,
            roomId: props.route.params.roomId,
            messages: [],
            dataVersion: 0,
            isLoading: true,
        }

        this.start()
    }

     start = async () => {

         await this.getData()

         Messsage.addListener('fetching',(chatId)=>{

             if(chatId){
                 this.setState({status: 'Уншиж байна',statusColor: Constants.COLORS.INFO});
             }else if(this.state.messages.length == 0 ){
                 this.setState({isLoading: true})
             }
         })

         Message.addListener('fetched',(messages)=>{


             this.setState({isLoading: false})
         })

         Message.addListener('new',(message)=>{
             this.addMessage(message)
         })

         Message.addListener('error',(message)=>{
             this.setState({isLoading: false})
             CustomAlert.alert("Алдаа гарлаа!",message, 'error');
         })

         Room.addListener('roomFetched',(room)=>{
             this.setRoom(room,true)
         })
     }


    componentDidMount() {

        ChatUtil.chatScreen = this

        AsyncStorage.getItem("userId").then((userId)=>{
            this.setState({userId: userId})
        })

        this.setRoomInfo()

    }

     componentWillUnmount(): void {

         ChatUtil.chatScreen = undefined
         this.unsubscribe();
         Message.close()
         Room.removeListener("roomFetched")

     }


    setRoomInfo = async () => {

        let room = this.state.room

        if(room){

            let userId = await AsyncStorage.getItem("userId")
            let roomUtil = new RoomUtil(userId)

            this.setState({
                roomName: roomUtil.roomName(room),
                roomImage: roomUtil.roomImage(room)
            })

        }else{

            this.setState({roomName: "", roomImage: ""})
        }

    }

    setRoom = (room,reloadData = true) => {

        this.setState({room: room},()=>{

            this.refs.toolsView.hide()
            this.setRoomInfo()
            if(reloadData) {
                this.getMessages()
            }

        })
    }


    getData = () => {

        if(!this.state.room){
            if(this.state.roomId) {
                let room = Room.getRoom(this.state.roomId)

                if(room){
                    this.setRoom(room,false)
                }else{

                }

            }else{

                this.setState({isLoading: true})
                Room.createRoom(this.props.route.params.users,({room,error})=>{

                    if(error){
                        CustomAlert.alert("Алдаа гарлаа!",error, 'error');
                    }else{
                        this.setRoom(room)
                    }
                    this.setState({isLoading: false})
                })
            }

        }else{
            this.getMessages()
        }

    }

    getMessages = (chatId) => {

        let messages = Message.getData({roomId: this.state.room.id,chatId: chatId})
        this.setState({
            messages: (chatId) ? this.state.messages.concat(messages) : messages
        })
    }

    addMessage = (str) => {

        let message = JSON.parse(str)

        if(message.roomId == this.state.room.id) {

            let messages = this.state.messages

            if(message.type == 'SYSTEM' && message.room){
                this.setRoom(message.room,false)
            }

            let newMessages = ChatUtil.handleMessage(messages,message)

            this.setState({
                messages: newMessages,
                dataVersion: this.state.dataVersion + 1,
            })

        }else{

            if(message.fromUserId != this.state.userId) {

                if(message.type != "SEEN" && message.type != "UPDATE"){

                    let roomUtil = new RoomUtil(this.state.userId)

                    let data =  {
                        image: roomUtil.roomImage(message.room),
                        name: roomUtil.roomName(message.room),
                        content: roomUtil.getContent(message,message.room.isGroup),
                    }

                    TopNotification.show(data,() => {
                        this.setRoom(message.room)
                    })
                }
            }
        }
    }

     send = (data) => {

         let client = stompContext.getStompClient()

         // if(!client){
         //     return
         // }hi

         if(!data.id) {
             data.id = uuid()
         }
         data.fromUserId = this.state.userId
         data.roomId = this.state.room.id

         let jsonString = JSON.stringify(data)

         try {
             client.publish({destination: "/app/chat.send", body: jsonString});
             return jsonString
         }catch (e) {
             return null
         }

     }


    sendMessage = () => {

        let message = this.refs.input.getValue().trim()

        if(message) {

           let jsonString = this.send({
                content: message,
                type: 'CHAT',
                new: true,
            })

            this.addMessage(jsonString)

            this.refs.input.setValue('')
        }

    }

     seenDictionary = {}

     seen = (message) => {

        if(message.fromUserId == this.state.userId){
            return
        }

        if(ChatUtil.isSeen(message,this.state.userId,this.seenDictionary)){
            return
        }

         if(this.send({
             id: message.id,
             type: 'SEEN'
         })){
             this.seenDictionary[message.id] = true
         }


     }

     startContract = () => {
         this.send({
             content: "Хэлцэл хийх үү?",
             type: 'CONTRACT_REQUEST',
         })
     }

     endContract = () => {

         this.send({
             type: 'CONTRACT_END_REQUEST',
         })
     }

     contracts = () => {

         let withUserIds = this.state.room.userIdList.filter((id)=> id != this.state.userId)
         this.props.navigation.navigate('Contracts',{withUserId: withUserIds[0]})
     }

     showTools = () => {

        this.refs.toolsView.show(this.refs.input)
     }

     getNextItem = (currentIndex) => {

        if((currentIndex + 1) < this.state.messages.length){
            return this.state.messages[currentIndex + 1]
        }

        return undefined
     }

     onEndReached = ({ distanceFromEnd }) => {

        if(!this.gettingMessage && !this.reachedToEnd){

            let chatId
            if(this.state.messages.length > 0){
                chatId = this.state.messages[this.state.messages.length - 1].id
            }

            this.getMessages(chatId)
        }
     }

     isGroup = () => {
        if(this.state.room){
            return this.state.room.isGroup
        }else{
            return false
        }
     }

     chatInfo = () => {
         this.props.navigation.navigate('ChatInfo',{room: this.state.room})
     }

     pickImage = () => {

         ImagePicker.launchImageLibrary({}, (response) => {

             if (response.uri) {

                 console.log(response)
                 this.sendImage(response)

             }

         });
     }

     sendImage = (image) => {

        let ratio

        if(Platform.OS == 'android'){
            ratio = image.isVertical ? (image.width / image.height) : (image.height / image.width)
        }else{
            ratio = image.width / image.height
        }

         let message = {
             id: uuid(),
             type: 'IMAGE',
             fromUserId: this.state.userId,
             roomId: this.state.room.id,
             content: JSON.stringify({
                 ratio: ratio,
                 file: image,
             }),
         }

         this.addMessage(JSON.stringify(message))
     }

    render() {

        return (
            <SafeAreaView style = {{flex:1, backgroundColor: '#fff'}} >
                    <View style = {styles.header} >
                        <HeaderButton icon = "keyboard-arrow-left"  onPress={()=>this.props.navigation.goBack()} style = {{marginLeft: 0}} />
                        <View style = {{flexDirection: 'row',flex:1}} >
                            {/*<TouchableOpacity onPress = {()=>this.props.navigation.navigate('User')} >*/}
                                <ProfileImage size = {32} user = {this.state.roomImage}  />
                            {/*</TouchableOpacity>*/}
                            <Text style = {styles.title} >{this.state.roomName}</Text>
                        </View>
                        {this.isGroup() ? (<HeaderButton icon = "info"   onPress = {this.chatInfo} />) : null}
                    </View>
                    <Border/>

                    {(this.state.status) ? (<View style = {[styles.statusContainer,{backgroundColor: this.state.statusColor}]} ><Text style = {styles.status} >{this.state.status}</Text></View>) : null}
                    <FlatList
                        style = {styles.flatList}
                        inverted
                        data={this.state.messages}
                        extraData = {this.state.dataVersion}
                        onEndReached = {this.onEndReached}
                        onEndReachedThreshold = {0.5}
                        renderItem={({item, index}) =>{
                            this.seen(item,this.seenList)
                            // console.log('chat id',item.id)
                            return (<ChatItem
                                key = {item.id}
                                userId={this.state.userId}
                                nextItem = {this.getNextItem(index)}
                                isLast = {index == 0}
                                item = {item}
                                send = {this.send}
                                isGroup = {this.isGroup()}
                                navigation = {this.props.navigation}
                            />)
                        }}
                    />

                    <View style = {styles.inputContainer}>
                        <Button icon = "add" noShadow iconSize = {24} style = {styles.buttonLeft} onPress = {this.showTools} />
                        <Input ref = "input" style = {styles.input} multiline onFocus = {(e)=>this.refs.toolsView.onFocus(e)} />
                        <Button icon = "send" noShadow style = {styles.button} onPress = {this.sendMessage} />
                    </View>
                    <ToolsView ref = "toolsView" >

                            <View style = {{ flexDirection: 'row' }} >
                                <ToolButton style = {{flex:1}} icon= "photo" title = "Зураг илгээх" onPress = {this.pickImage} />
                                {(!this.isGroup()) ? (<ToolButton style = {{flex:1,  marginLeft: 8}} cicon= "hand" title = "Хэлцэл эхлэх" onPress = {this.startContract} />) : (<View style = {{flex:1, marginLeft: 8}} />)}
                                {(!this.isGroup()) ? (<ToolButton style = {{flex:1, marginLeft: 8}} cicon= "handshake" title = "Хэлцэл дуусгах" onPress = {this.endContract} />) : (<View style = {{flex:1, marginLeft: 8}} />)}
                            </View>

                        {(!this.isGroup()) ? (
                            <View style = {{flexDirection: 'row', marginTop: 8,}} >
                                <View style = {{flex:1}}>
                                    <ToolButton cicon= "information" title = "Хэлцэл гэж юу вэ?" onPress = {ChatUtil.aboutContract} />
                                </View>
                                <View style = {{flex:1, marginLeft: 8}}>
                                    <ToolButton icon= "list" title = "Хэлцэлүүд" onPress = {this.contracts} />
                                </View>
                                <View style = {{flex:1, marginLeft: 8}}>
                                    <ToolButton icon= "account-balance" title = "Эскроу данс" onPress = {()=>this.props.navigation.navigate('Escrow',{roomId: this.state.room.id, receiverName: this.state.roomName})} />
                                </View>
                                {/*<View style = {{flex:1, marginLeft: 8}} />*/}
                            </View>
                        ) : null}

                    </ToolsView>
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}

// export default withStomp(ChatScreen)
export default ChatScreen

const styles = StyleSheet.create({

    container: {
        flex:1,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 8,
        backgroundColor: '#fff',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 22,
        // textAlign: 'center',
        marginLeft: 8,
    },

    statusContainer: {
      padding: 8,
      alignItems: 'center',
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
        alignItems: 'flex-end'
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        margin: 8,
        marginRight: 0,
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },


});
