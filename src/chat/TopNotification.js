import React, { Component } from "react";
import {Animated, View, StyleSheet, Text, TouchableOpacity, Vibration} from 'react-native';
import {ProfileImage} from '../common/ProfileImage';
import SafeArea from 'react-native-safe-area';

export default class TopNotification extends Component {

    static instance

    constructor(props){
        super(props);

        TopNotification.instance = this

        this.state = {
            topInset: 8,
            // opacity: new Animated.Value(1),
            opacity: new Animated.Value(0),
            hide: true,
            // hide: false,
            message: {}
        }

        SafeArea.getSafeAreaInsetsForRootView()
            .then((result) => {
                console.log(result)
                this.setState({topInset: result.safeAreaInsets.top + 8})
            })
    }


    componentDidMount() {

    }


    componentWillUnmount() {

    }


    static show = (data,callback) => {

        let instance = TopNotification.instance
        instance.callback = callback
        instance.setState({
            hide: false,
            image: data.image,
            name: data.name,
            content: data.content,
        },() => Vibration.vibrate(200))

        // NotificationSounds.getNotifications().then(soundsList => {
        //     console.log('SOUNDS',soundsList);
        //     /*
        //     Play the notification sound.
        //     pass the complete sound object.
        //     This function can be used for playing the sample sound
        //     */
        //
        //     if(Platform.OS == 'ios') {
        //         playSampleSound(soundsList[266]);
        //     }else{
        //         playSampleSound(soundsList[14]);
        //     }
        //
        // });


        if(instance.timeout){
            clearTimeout(instance.timeout)
        }

        Animated.timing(instance.state.opacity,
            {
                toValue: 1,
                duration: 100,
                useNativeDriver: true,
            }
        ).start();

        instance.timeout = setTimeout(instance.hide, 2000);
    }

    hide = () => {

        this.timeout = undefined
        this.callback = undefined

        this.setState({
            message: {},
        })

        Animated.timing(this.state.opacity,
            {
                toValue: 0,
                duration: 100,
                useNativeDriver: true,
            }
        ).start();

        setTimeout(()=>this.setState({hide: true}), 100);
    }

    onPress = () => {

        TopNotification.instance.callback && TopNotification.instance.callback()
        this.hide()
    }

    render (){

        if(this.state.hide){
            return null
        }

        return (
            <Animated.View style = {[styles.container,{top: this.state.topInset, opacity: this.state.opacity}]} >
                <TouchableOpacity
                    style = {{flexDirection: 'row',flex:1, padding: 8}}
                    onPress = {this.onPress} >
                    <ProfileImage user = {this.state.image} />
                    <View style = {{flex:1, marginLeft: 8, justifyContent: 'center'}} >
                        <Text style = {styles.name} >{this.state.name}</Text>
                        <Text style = {styles.text} >{this.state.content}</Text>
                    </View>
                </TouchableOpacity>
            </Animated.View>);
    }

}


const styles = StyleSheet.create({

    container: {
        position: 'absolute',
        left: 8,
        top: 8,
        right: 8,

        backgroundColor: '#fff',
        borderRadius: 8,

        flexDirection: 'row',

        shadowColor: '#F02436',
        shadowOpacity: 0.4,
        shadowOffset: {width: 2,height: 2},
        elevation: 4,
    },

    name: {
        fontFamily: 'Roboto-Bold',
        color: '#000',
        fontSize: 14,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
    },

});
