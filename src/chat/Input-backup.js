import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
} from 'react-native';
import Util from '../common/Util';


export default class Input extends React.Component {


    constructor(props) {
        super(props);

        console.log('input constructor')

        if(this.props.value){
            this.state = {value: this.props.value, blurred: true}
        }


        this.state = {
            value: this.props.value,
            selectedValue: this.props.selectedValue,
            secureTextEntry: this.props.secureTextEntry,
            pattern: this.props.pattern
        };

    }

    componentDidMount(): void {


    }

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {

        if(nextProps.pattern != this.props.pattern){
            this.setState({pattern: nextProps.pattern})
        }
    }

    setValue = (value) => {

        this.setState({value},()=>this.validate())
    }

    getValue = () => {

        // сонголт хийдэг эсэхийг шалгана
        if(this.props.navigation ) {
            return this.state.selectedValue
        }else{
            return this.state.value
        }


    }

    getValues = () => {

        if(this.validate() == false){

            return false
        }


        let values = {
            id: this.state.selectedValue,
            text: this.state.value,
        };

        return values;
    };


    validate = () => {
        if(this.props.required && (!this.state.value || (this.state.value && this.state.value.trim().length == 0))) {

            if(this.props.requiredText) {
                this.setState({error: this.props.requiredText})
            }else{
                this.setState({error: `${this.props.label} оруулна уу!`})
            }

            return false
        }

        if(this.state.value && this.state.pattern && !this.state.pattern.test(this.state.value)){

            this.setState({error: this.props.error})
            return false
        }

        this.setState({error: null})

        return this.getValue()
    }

    _onChangeText = async (text) => {

        if(this.props.pan){

            text = Util.formatPan(text)

        }else if(this.props.expireDate){

            text = Util.formatExpireDate(text)

        }else if(this.props.tokenizeAmount){

            text = Util.formatTokenizeAmount(text)
        }

        await this.setState({value: text})
        this.validate()
        this.props.onChangeText && this.props.onChangeText(text)

    }

    _onBlur = () => {

        this.props.onBlur && this.props.onBlur()
        this.setState({blurred: true})
    }

    _onFocus = () => {

        this.props.onFocus && this.props.onFocus()

        // сонголт хийдэг эсэхийг шалгана
        if(this.props.navigation ){

            // initialRouteName сонголт хийхэд гарч ирэх дэлгэцийн route нэр.
            // Өөрөөр хэлбэл SelectListScreen доторх stackNavigation дотор байгаа дэлгэцүүдийн route гэсэн үг.
            // Шинээр сонголт хийдэг дэлгэц нэмэх бол тэнд нэмж өгнө
            this.props.navigation.navigate('select',{initialRouteName: this.props.initialRouteName, sendData: this.props.sendData, callback: this.select, value: {value: this.state.selectedValue}})
        }

        this.setState({blurred: false})
    }

    // value-н бүтэц иймэрхүү байна {text: selectedText, value: selectedId}
    select = (value,data) => {

        this.setState({value: value.text, selectedValue: value.value}, () => {
            this.validate();
        })

        // onSelected callback дамжуулсан байвал дуудна
        this.props.onSelected && this.props.onSelected(value,data)
        this.props.onChangeText?.(value.text);
    }

    focus = () => {

        this.refs.textInput.focus()

    }


    onLayout = (event) => {
        const layout = event.nativeEvent.layout;
        this.y = layout.y
    }


     render() {

        return (
            <View style = {[styles.container,this.props.style]} onlayout = {this.onLayout} >
                        <TextInput
                            ref="textInput"
                            style = {styles.textInput}
                            onChangeText={this._onChangeText}
                            onBlur = {this._onBlur}
                            onFocus = {this._onFocus}
                            value={this.state.value}
                            selectedValue={this.state.selectedValue}
                            autoFocus = {this.props.autoFocus}
                            selectionColor = "#000"
                            multiline = { !this.props.secureTextEntry && this.props.multiline}
                            secureTextEntry = {this.state.secureTextEntry}
                            scrollEnabled = {this.props.scrollEnabled}
                            keyboardType = {this.props.keyboardType}
                            placeholder = {this.props.placeholder != null ? this.props.placeholder : this.props.label}
                            returnKeyType = {this.props.returnKeyType}
                            onSubmitEditing={this.props.onSubmitEditing}
                            blurOnSubmit={false}
                            autoCapitalize = {this.props.autoCapitalize}
                            editable = {this.props.editable}
                            maxLength = {this.props.maxLength}
                        />
                </View>


        )
    }

}



const styles = StyleSheet.create({

    container: {
        // justifyContent: 'center',
        borderRadius: 8,
        minHeight: 36,
        paddingTop: 4,
        paddingBottom: 6,
        paddingLeft:8,
        paddingRight: 8,
    },

    textInput: {
        color: '#000',
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        minHeight: 24,
        padding: 0,
        // backgroundColor: 'gray'
    },

});
