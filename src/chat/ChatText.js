import React from "react";
import {StyleSheet, Text, TouchableOpacity,View, Linking} from 'react-native';
import Pattern from 'url-knife';
import Util from '../common/Util';
import User from '../data/User';

export default function ChatText(props) {

    const [text,setText] = React.useState(()=>{

        if(!props.children){
            return
        }

        return splitNewLine()

    })

    function splitNewLine(){

        let lines

        if(props.children.contentObjects){

            let objects = JSON.parse(props.children.contentObjects)
            return splitWhiteSpace(0,objects)

        }else{
            lines = props.children.content.split('\n')

            let newTexts = []

            for(let i = 0; i < lines.length; i++){

                newTexts = newTexts.concat(splitWhiteSpace(i,lines[i]))

                if(i < lines.length - 1){
                    newTexts.push('\n')
                }
            }
            return newTexts
        }
    }

    function splitWhiteSpace(line,str){

        console.log('type of',typeof str)

        let texts = typeof str == "string" ? str.split(' ') : str
        let newTexts = []

        for(let i = 0; i < texts.length; i++){

            let text = typeof texts[i] == "object" ? texts[i].text : texts[i]

            if(!text){
                newTexts.push("")
                continue
            }

            if(texts[i].userId){
                newTexts.push(<Mention userId = {texts[i].userId} >{text}</Mention>)

            }else{

                let urls = Pattern.TextArea.extractAllUrls(text)
                let emails = Pattern.TextArea.extractAllEmails(text)
                if(urls.length > 0){
                    newTexts.push(<Link type = {"url"} >{text}</Link>)
                }else if (emails.length > 0){
                    newTexts.push(<Link type = {"email"} >{text}</Link>)
                }else{
                    newTexts.push(<View key = {line + "" + i} ><Text style = {[styles.text, props.style]} >{text} </Text></View>)
                }
            }

        }

        return newTexts
    }

    React.useEffect(()=>{


    },[])


    return(
        <Text style = {styles.mainText} >
            {text}
            {/*{`${/n}`}*/}
            {/*{props.children}*/}
        </Text>
    )

}

function Link(props) {

    function onPress() {

        if(props.type == "email"){
            Linking.openURL("mailto:" + props.children)
        }else if(props.type == "url"){

            if(props.children.startsWith("http")){
                Linking.openURL(props.children)
            }else{
                Linking.openURL("http://"+props.children)
            }
        }
    }

    return(
        <TouchableOpacity style = {styles.touchableOpacity} onPress = {onPress} ><Text style = {[styles.link,props.style]} >{props.children} </Text></TouchableOpacity>
    )

}

function Mention(props) {

    async function onPress() {

        let user = await User.getUser(props.userId)
        Util.navigation.navigate('Profile',{user: user})
    }

    return(
        <TouchableOpacity style = {styles.touchableOpacity} onPress = {onPress} ><Text style = {[styles.mention,props.style]} >{props.children} </Text></TouchableOpacity>
    )

}



const styles = StyleSheet.create({

    mainText: {
        margin: 8,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        // color: '#fff',
        fontSize: 16,
        // margin: 8,
    },

    touchableOpacity: {
        flexDirection: 'row',
    },

    link: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#0064bd',
        textDecorationLine:'underline',
        fontSize: 16,
    },

    mention: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#0064bd',
        fontSize: 16,
    },
})

