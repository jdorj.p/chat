import React, { Component } from "react";
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    SafeAreaView,
} from 'react-native';
import HeaderButton from '../components/HeaderButton';
import {ProfileImage} from '../common/ProfileImage';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import ChatUtil from '../chat/ChatUtil';
import {RoomUtil} from '../room/RoomUtil';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Border from '../components/Border';
import ListButton from '../components/ListButton';
import {UserListItem} from './UserListItem';
import {InputModal} from '../components/InputModal';


class ChatInfoScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            room: props.route.params.room,
        }

        this.setRoomInfo()
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            headerShown: false,
        })

        // this.unsubscribeFocus = this.props.navigation.addListener(
        //     'focus',
        //     payload => {
        //
        //     }
        // );

    }


    componentWillUnmount(): void {

        // this.unsubscribeFocus();
    }

    setRoomInfo = async () => {

        let room = this.state.room

        if(room){

            let userId = await AsyncStorage.getItem("userId")
            let roomUtil = new RoomUtil(userId)

            this.setState({
                roomName: roomUtil.roomName(room),
                roomImage: roomUtil.roomImage(room)
            })

        }else{

            this.setState({roomName: "", roomImage: ""})
        }

    }

    changeName = () => {

        this.refs.groupNameModal.show({
            title: "Нэр солих",
            label: "Нэр",
            value: this.state.room.name,
            callback: (value)=>{
                if(value){
                    this.sendData(value)
                }
            }})
    }

    sendData = async (name) => {

        this.setState({isLoading: true});

        let userId = await AsyncStorage.getItem("userId")

        fetch(Constants.URL + "wsock/room/changeName", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "fromUserId": userId,
                "name": name,
                "roomId": this.state.room.id
            })
        }).then((response) => response.json())
            .then((json) => {

                console.log(json)

                if(json.status == "000") {

                    this.setState({room: json.entity},()=>this.setRoomInfo())
                    ChatUtil.chatScreen.setRoom(json.entity)

                }else{
                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                }

                this.setState({isLoading: false})

            }).catch((error) => {

            console.log(error.message)

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false});
        });

    }

    addPeople = () => {

        this.props.navigation.navigate('Contacts',{
            title: 'Хүн нэмэх',
            exceptUsers: this.state.room.userIdList,
            callback: async (users)=> {

                this.setState({isLoading: true});

                let userId = await AsyncStorage.getItem("userId")

                fetch(Constants.URL + "wsock/room/join", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "userIdList": users.map((user) => user.userId),
                        "userId": userId,
                        "id": this.state.room.id
                    })
                }).then((response) => response.json())
                    .then((json) => {

                        console.log(json)

                        if(json.status == "000") {

                            this.setState({room: json.entity},()=>this.setRoomInfo())
                            ChatUtil.chatScreen.setRoom(json.entity)

                        }else{
                            CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                        }

                        this.setState({isLoading: false})

                    }).catch((error) => {

                    console.log(error.message)

                    CustomAlert.alert("Алдаа гарлаа!", error.message, 'error');

                    this.setState({isLoading: false});
                })

            }})
    }

    leaveGroup = () => {

        CustomAlert.confirm("Группээс гарах","Та группээс гарах гэж байна уу?",(response)=>{

            if(response){
                leave()
            }

        })


        let leave = async () => {

            this.setState({isLoading: true});

            let userId = await AsyncStorage.getItem("userId")

            fetch(Constants.URL + "wsock/room/leave", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "userId": userId,
                    "id": this.state.room.id
                })
            }).then((response) => response.json())
                .then((json) => {


                    if(json.status == "000") {

                        this.props.navigation.navigate('RoomList')

                    }else{
                        CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                    }

                    this.setState({isLoading: false})

                }).catch((error) => {

                console.log(error.message)

                CustomAlert.alert("Алдаа гарлаа!", error.message, 'error');

                this.setState({isLoading: false});
            })
        }

    }

    render() {

        return (
            <SafeAreaView style = {styles.safeAreaView} >
                <View style = {styles.header} >
                    <HeaderButton icon = "keyboard-arrow-left" iconSize = {32} onPress={()=>this.props.navigation.goBack()} style = {{marginLeft: 0}} />
                    <View style = {{flexDirection: 'row',flex:1}} >
                        <ProfileImage size = {32} user = {this.state.roomImage}  />
                        <Text style = {styles.title} >{this.state.roomName}</Text>
                    </View>


                </View>
                <Border/>

                <ListButton icon = "edit" text = " Нэр солих"  onPress = {this.changeName} />
                <ListButton icon = "person-add" text = "Хүн нэмэх" onPress = {this.addPeople}  ></ListButton>

                <Text style = {styles.members} >Гишүүд ({this.state.room.userList.length})</Text>
                <Border style={{marginLeft: 16}} />

                <FlatList
                    style = {styles.flatList}
                    data={this.state.room.userList}
                    extraData = {this.state.dataVersion}x
                    renderItem={({item, index}) =>(
                        <UserListItem
                            key = {index}
                            item = {item}
                            onPress = {()=> this.props.navigation.navigate('Profile',{user: item})}
                        />)}
                />
                <Border style = {{marginLeft: 16}} />
                <ListButton icon = "exit-to-app" text = "Группээс гарах" color = "#DF2D43" onPress = {this.leaveGroup} hideBorder ></ListButton>

                <InputModal ref = "groupNameModal" />

                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}

export default ChatInfoScreen

const styles = StyleSheet.create({

    safeAreaView:{
        flex:1,
        backgroundColor: '#fff',
    },

    container: {
        flex:1,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        // backgroundColor: '#fff',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 22,
        textAlign: 'center',
        marginLeft: 8,
    },

    members: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#333333',
        fontSize: 18,
        margin: 16,
    },

    statusContainer: {
        padding: 8,
        alignItems: 'center',
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

    flatList: {
        flex:1,
        margin: 8
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
        borderRadius: 18,
        minHeight: 36,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        // borderRadius: 18,
        margin: 8,
        marginRight: 0,
        // backgroundColor: '#4CA2EB',
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        borderRadius: 18,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },

    imageBackground: {
        flex: 1,
        margin: 16,
    },

    scrollView:{
        flex:1,
    },

    text: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
    },

    textBottom: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
        textAlign: 'center',
    },

    exitButton: {
        width: 44,
        height: 44,
        position: 'absolute',
        right: 8,
        top: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },

    exitImage: {
        width: 34,
        height: 34,
    },

});
