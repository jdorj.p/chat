import React, { Component } from "react";
import {View, Text, FlatList , StyleSheet, SafeAreaView,} from 'react-native';
import {ChatItem} from './ChatItem';
import {StompEventTypes, stompContext } from './Stomp'
import Input from './Input';
import Button from '../components/Button';
import {ToolsView} from './ToolsView';
import ToolButton from './ToolButton';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import ChatUtil from './ChatUtil';
import {RoomUtil} from '../room/RoomUtil';
import {ProfileImage} from '../common/ProfileImage';
import HeaderButton from '../components/HeaderButton';
import 'react-native-get-random-values'
import { v4 as uuid } from "uuid";
import Border from '../components/Border';
import ImagePicker from 'react-native-image-picker';
import TopNotification from './TopNotification';

 class ChatScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            room: props.route.params.room,
            roomId: props.route.params.roomId,
            messages: [],
            dataVersion: 0,
            status: "Холбогдож байна",
            statusColor: Constants.COLORS.INFO,
            isLoading: true,
        }


    }


    componentDidMount() {

        this.props.navigation.setOptions({
            headerShown: false,
        })

        ChatUtil.chatScreen = this

        AsyncStorage.getItem("userId").then((userId)=>{
            this.setState({userId: userId})
        })

        this.connect()
        this.setRoomInfo()
    }

     componentWillUnmount(): void {

         ChatUtil.chatScreen = undefined
         this.unsubscribe();

         stompContext.removeStompEventListener(StompEventTypes.Connect,this.onConnect)
         stompContext.removeStompEventListener(StompEventTypes.Disconnect,this.onDisconnect)
         stompContext.removeStompEventListener(StompEventTypes.WebSocketClose,this.onWebSocketClose)

     }


     connect = () => {

         let client = stompContext.getStompClient()

         // console.log(client)
         // if(client.subscribe){
             this.getData()
         // }

         stompContext.addStompEventListener(
             StompEventTypes.Connect,
             this.onConnect,
             this,
         )

         stompContext.addStompEventListener(
             StompEventTypes.Disconnect,
             this.onDisconnect,
             this
         )

         stompContext.addStompEventListener(
             StompEventTypes.WebSocketClose,
             this.onWebSocketClose,
             this
         )
     }

     onConnect = () => {
         this.setState({status: 'Холбогдлоо',statusColor: Constants.COLORS.SUCCESS})
         this.getData()
     }

     onDisconnect = () => {
         this.setState({status: 'Холболт саллаа',statusColor: Constants.COLORS.ERROR})
     }

     onWebSocketClose = () => {
         this.setState({status: 'Холболт саллаа',statusColor: Constants.COLORS.ERROR})
     }

     subscribe = () => {

        this.unsubscribe()

         try{

             this.subscribed = stompContext.getStompClient().subscribe('/queue/private.' + this.state.userId,
                 (message) => {
                     console.log(JSON.parse(message.body))
                     this.addMessage(message.body)
                 })

         }catch (e) {

            console.log(e)
         }
     }

     unsubscribe = () => {

         if(this.subscribed){

             // console.log(this.subscribed)
             this.subscribed.unsubscribe()
             // console.log('unsubscribed')
         }

     }

    setRoomInfo = async () => {

        let room = this.state.room

        if(room){

            let userId = await AsyncStorage.getItem("userId")
            let roomUtil = new RoomUtil(userId)

            this.setState({
                roomName: roomUtil.roomName(room),
                roomImage: roomUtil.roomImage(room)
            })

        }else{

            this.setState({roomName: "", roomImage: ""})
        }

    }

    setRoom = (room,reloadData = true) => {

        this.setState({room: room},()=>{

            this.refs.toolsView.hide()
            this.setRoomInfo()
            if(reloadData) {
                this.unsubscribe()
                this.getData()
            }

        })
    }

    setStatus = (user) => {

    }

    getData = () => {

        if(!this.state.room){
            if(this.state.roomId) {
                this.getRoom(this.state.roomId)
            }else{
                this.getRoom()
            }
        }else{
            this.getMessages()
        }

    }

     getRoom = async (roomId) => {

         this.setState({isLoading: true});
         let data
         let uri

         if(roomId){
             uri = "wsock/room/findById"
             data = {id: roomId}

         }else{

             uri = "wsock/room"
             let users = this.props.route.params.users
             let userIdList = users.map((user) => user.userId)

             let userId = await AsyncStorage.getItem("userId");
             userIdList.push(userId)

              data = {
                 userIdList: userIdList,
                 userId: userId
             }

             // console.log(uri)
             // console.log(data)
         }

         fetch(Constants.WSURL + uri, {
             method: 'POST',
             headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json'
             },
             body: JSON.stringify(data)
         }).then((response) => response.json())
             .then((json) => {

                 // console.log('get room',json)

                 this.setState({
                     isLoading: false,
                     room: json.entity,
                 },()=>{
                     this.setRoomInfo()
                     this.getMessages()
                 })

             }).catch((error) => {

             console.log(error)

             CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

             this.setState({isLoading: false});
         });
     }

     getMessages = async (isNew = true) => {

        if(isNew){
            this.reachedToEnd = false
        }

        this.gettingMessage = true

         let chatId = null

         if(!isNew && this.state.messages.length > 0){
             chatId = this.state.messages[this.state.messages.length - 1].id
             this.setState({status: 'Уншиж байна',statusColor: Constants.COLORS.INFO});
         }else{
             this.setState({isLoading: true});
         }

         // console.log(this.state.room)
         // console.log("getMessages")

         fetch(Constants.WSURL + "wsock/chat/findByRoomId?chatId="+ chatId +"&rowNumber=20&roomId="+ this.state.room.id + "&userId=" + this.state.userId, {
             headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json'
             },
         }).then((response) => response.json())
             .then((json) => {

                 console.log(json)

                 if(json.status != "000"){
                     CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                     this.setState({
                         isLoading: false,
                         status: null,
                     });
                 }else{

                     let messages = []

                     if(isNew){
                         messages = json.entity
                         // ChatUtil.setCurrentChatStatus(messages)
                         this.seenDictionary = {}
                     }else{
                         messages = this.state.messages.concat(json.entity)
                     }

                     if(json.entity.length == 0){
                         this.reachedToEnd = true
                     }

                     this.setState({
                         isLoading: false,
                         status: null,
                         messages: messages,
                     },()=>this.subscribe())
                 }

                 this.gettingMessage = false

             }).catch((error) => {

             console.log(error)

             CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

             this.setState({
                 isLoading: false,
                 status: null,
             });

             this.gettingMessage = false
         });
     }

    addMessage = (str) => {

        let message = JSON.parse(str)

        // console.log(this.state.room.id)
        // console.log(message.roomId)
        // console.log(message)

        if(message.roomId == this.state.room.id) {

            let messages = this.state.messages

            if(message.type == 'SYSTEM' && message.room){
                this.setRoom(message.room,false)
            }

            let newMessages = ChatUtil.handleMessage(messages,message)

            this.setState({
                messages: newMessages,
                dataVersion: this.state.dataVersion + 1,
            })

        }else{

            if(message.fromUserId != this.state.userId) {

                if(message.type != "SEEN" && message.type != "UPDATE"){

                    let roomUtil = new RoomUtil(this.state.userId)

                    let data =  {
                        image: roomUtil.roomImage(message.room),
                        name: roomUtil.roomName(message.room),
                        content: roomUtil.getContent(message,message.room.isGroup),
                    }

                    TopNotification.show(data,() => {
                        this.setRoom(message.room)
                    })
                }
            }
        }
    }

     send = (data) => {

         let client = stompContext.getStompClient()

         // if(!client){
         //     return
         // }hi

         if(!data.id) {
             data.id = uuid()
         }
         data.fromUserId = this.state.userId
         data.roomId = this.state.room.id

         let jsonString = JSON.stringify(data)

         try {
             client.publish({destination: "/app/chat.send", body: jsonString});
             return jsonString
         }catch (e) {
             return null
         }

     }

     sendTest = (data) => {

         data.fromUserId = '101596525755182' //this.state.userId
         data.roomId = this.state.room.id

         this.addMessage(JSON.stringify(data))

     }

    sendMessage = () => {

        let message = this.refs.input.getValue().trim()

        if(message) {

           let jsonString = this.send({
                content: message,
                type: 'CHAT',
                new: true,
            })

            this.addMessage(jsonString)

            this.refs.input.setValue('')
        }

    }

     seenDictionary = {}

     seen = (message) => {

        if(message.fromUserId == this.state.userId){
            return
        }

        if(ChatUtil.isSeen(message,this.state.userId,this.seenDictionary)){
            return
        }

        // console.log('send seen',message.id)

         if(this.send({
             id: message.id,
             type: 'SEEN'
         })){
             this.seenDictionary[message.id] = true
         }


     }

     startContract = () => {
         this.send({
             content: "Хэлцэл хийх үү?",
             type: 'CONTRACT_REQUEST',
         })
     }

     endContract = () => {

         this.send({
             type: 'CONTRACT_END_REQUEST',
         })
     }

     contracts = () => {

         let withUserIds = this.state.room.userIdList.filter((id)=> id != this.state.userId)
         this.props.navigation.navigate('Contracts',{withUserId: withUserIds[0]})
     }

     showTools = () => {

        this.refs.toolsView.show(this.refs.input)
     }

     getNextItem = (currentIndex) => {

        if((currentIndex + 1) < this.state.messages.length){
            return this.state.messages[currentIndex + 1]
        }

        return undefined
     }

     onEndReached = ({ distanceFromEnd }) => {


        // console.log('distanceFromEnd',distanceFromEnd)

        if(!this.gettingMessage && !this.reachedToEnd){

            // console.log("onEndReached getMessages")
            this.getMessages(false)
        }
     }

     isGroup = () => {
        if(this.state.room){
            return this.state.room.isGroup
        }else{
            return false
        }
     }

     chatInfo = () => {
         this.props.navigation.navigate('ChatInfo',{room: this.state.room})
     }

     pickImage = () => {

         ImagePicker.launchImageLibrary({}, (response) => {

             if (response.uri) {

                 console.log(response)
                 this.sendImage(response)

             }

         });
     }

     sendImage = (image) => {

        let ratio

        if(Platform.OS == 'android'){
            ratio = image.isVertical ? (image.width / image.height) : (image.height / image.width)
        }else{
            ratio = image.width / image.height
        }

         let message = {
             id: uuid(),
             type: 'IMAGE',
             fromUserId: this.state.userId,
             roomId: this.state.room.id,
             content: JSON.stringify({
                 ratio: ratio,
                 file: image,
             }),
         }

         this.addMessage(JSON.stringify(message))
     }

    render() {

        return (
            <SafeAreaView style = {{flex:1, backgroundColor: '#fff'}} >
                    <View style = {styles.header} >
                        <HeaderButton icon = "keyboard-arrow-left"  onPress={()=>this.props.navigation.goBack()} style = {{marginLeft: 0}} />
                        <View style = {{flexDirection: 'row',flex:1}} >
                            {/*<TouchableOpacity onPress = {()=>this.props.navigation.navigate('User')} >*/}
                                <ProfileImage size = {32} user = {this.state.roomImage}  />
                            {/*</TouchableOpacity>*/}
                            <Text style = {styles.title} >{this.state.roomName}</Text>
                        </View>
                        {this.isGroup() ? (<HeaderButton icon = "info"   onPress = {this.chatInfo} />) : null}
                    </View>
                    <Border/>

                    {(this.state.status) ? (<View style = {[styles.statusContainer,{backgroundColor: this.state.statusColor}]} ><Text style = {styles.status} >{this.state.status}</Text></View>) : null}
                    <FlatList
                        style = {styles.flatList}
                        inverted
                        data={this.state.messages}
                        extraData = {this.state.dataVersion}
                        onEndReached = {this.onEndReached}
                        onEndReachedThreshold = {0.5}
                        renderItem={({item, index}) =>{
                            this.seen(item,this.seenList)
                            // console.log('chat id',item.id)
                            return (<ChatItem
                                key = {item.id}
                                userId={this.state.userId}
                                nextItem = {this.getNextItem(index)}
                                isLast = {index == 0}
                                item = {item}
                                send = {this.send}
                                isGroup = {this.isGroup()}
                                navigation = {this.props.navigation}
                            />)
                        }}
                    />

                    <View style = {styles.inputContainer}>
                        <Button icon = "add" noShadow iconSize = {24} style = {styles.buttonLeft} onPress = {this.showTools} />
                        <Input ref = "input" style = {styles.input} multiline onFocus = {(e)=>this.refs.toolsView.onFocus(e)} />
                        <Button icon = "send" noShadow style = {styles.button} onPress = {this.sendMessage} />
                    </View>
                    <ToolsView ref = "toolsView" >

                            <View style = {{ flexDirection: 'row' }} >
                                <ToolButton style = {{flex:1}} icon= "photo" title = "Зураг илгээх" onPress = {this.pickImage} />
                                {(!this.isGroup()) ? (<ToolButton style = {{flex:1,  marginLeft: 8}} cicon= "hand" title = "Хэлцэл эхлэх" onPress = {this.startContract} />) : (<View style = {{flex:1, marginLeft: 8}} />)}
                                {(!this.isGroup()) ? (<ToolButton style = {{flex:1, marginLeft: 8}} cicon= "handshake" title = "Хэлцэл дуусгах" onPress = {this.endContract} />) : (<View style = {{flex:1, marginLeft: 8}} />)}
                            </View>

                        {(!this.isGroup()) ? (
                            <View style = {{flexDirection: 'row', marginTop: 8,}} >
                                <View style = {{flex:1}}>
                                    <ToolButton cicon= "information" title = "Хэлцэл гэж юу вэ?" onPress = {ChatUtil.aboutContract} />
                                </View>
                                <View style = {{flex:1, marginLeft: 8}}>
                                    <ToolButton icon= "list" title = "Хэлцэлүүд" onPress = {this.contracts} />
                                </View>
                                <View style = {{flex:1, marginLeft: 8}}>
                                    <ToolButton icon= "account-balance" title = "Эскроу данс" onPress = {()=>this.props.navigation.navigate('Escrow',{roomId: this.state.room.id, receiverName: this.state.roomName})} />
                                </View>
                                {/*<View style = {{flex:1, marginLeft: 8}} />*/}
                            </View>
                        ) : null}

                    </ToolsView>
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}

// export default withStomp(ChatScreen)
export default ChatScreen

const styles = StyleSheet.create({

    container: {
        flex:1,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 8,
        backgroundColor: '#fff',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 22,
        // textAlign: 'center',
        marginLeft: 8,
    },

    statusContainer: {
      padding: 8,
      alignItems: 'center',
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
        alignItems: 'flex-end'
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        margin: 8,
        marginRight: 0,
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },


});
