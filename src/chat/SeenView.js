import React, {Component} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {ProfileImage} from '../common/ProfileImage';


export class SeenView extends Component {

    constructor(props){
        super(props);

        this.state = {
        }
    }

    componentDidMount() {

    }


    render() {

        if(!this.props.seenUserList){
            return null
        }
        let images = []

        let users = this.props.seenUserList

        let imageStyle = {}
        let style = {}

        if(users.length > 1){
            imageStyle.marginLeft = 4
        }else{
            style = {
                // width: 8,
                // height: 8,
                // flex:1,
                // backgroundColor: 'gray'
            }
        }

        images = users.filter(user=> user.userId != this.props.userId && user.userId != this.props.fromUserId).map((user,i)=> <ProfileImage key = {"user"+i} user = {[user]} size={12} style={imageStyle}/>)


        if(this.props.align == 'flex-start'){
            style.marginLeft = 8
        }

        return (
            <View style = {[styles.container,{justifyContent: this.props.align},style, this.props.style]} >
                {images}
            </View>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // paddingRight: 8,
        margin: 8,
        marginTop: 0,
        marginLeft: 0,
    },
});
