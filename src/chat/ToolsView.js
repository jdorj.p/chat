import React, { Component } from "react";
import {View, StyleSheet, Keyboard} from 'react-native';
import SafeArea from 'react-native-safe-area';

export class ToolsView extends Component {

    constructor(props){
        super(props);

        this.state = {
            hidden: true,
            keyboardHidden: true,
            height: 216,
            bottomInset: 0,
        }

        SafeArea.getSafeAreaInsetsForRootView()
            .then((result) => {
                // console.log(result)
                this.setState({bottomInset: result.safeAreaInsets.bottom})
            })

        // this.props.
    }

    isHidden = () => {
        return  this.state.hidden
    }


    componentDidMount() {

        this.keyboardShowListener = Keyboard.addListener(
            Platform.OS == 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
            this._keyboardShow,
        );

        this.keyboardHideListener = Keyboard.addListener(
            Platform.OS == 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
            this._keyboardHide,
        );
    }

    componentWillUnmount() {

        this.keyboardShowListener.remove();
        this.keyboardHideListener.remove();
    }

    _keyboardShow = (e) => {

        console.log(e)

        this.setState({
            height: e.endCoordinates.height,
            keyboardHidden: false,
            hidden: true,
        })
    }

    _keyboardHide = (e) => {

        this.setState({
            keyboardHidden: true
        })
    }

    onFocus = (e) => {
        //
        // this.setState({
        //     keyboardHidden: false,
        // })
    }

    show = (input) => {

        if(this.state.hidden){

            this.setState({
                hidden:false,
            })

            Keyboard.dismiss()
            // if(!this.state.keyboardHidden){
            //     Keyboard.dismiss()
            // }
            return true
        }else{

            this.setState({
                hidden:true,
                // keyboardHidden: false
            })
            // input && input.focus()
            return false
        }
    }

    hide = () => {
        this.setState({hidden:true})
    }


    render (){

        let height = this.state.height - this.state.bottomInset
        let padding = 8

        if(Platform.OS == 'ios'){

            if(this.state.hidden && this.state.keyboardHidden){
                height = 0
                padding = 0
            }
        }else{

            if(this.state.hidden || !this.state.keyboardHidden) {
                height = 0
                padding = 0
            }
        }


        return (
            <View style = {[styles.container,{height: height, padding: padding}]} >
                {this.state.hidden ? null : this.props.children}
            </View>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        // flexDirection: 'row',
    },


});
