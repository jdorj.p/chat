import React from "react";
import {StyleSheet, Text, View, Image, Dimensions, TouchableOpacity} from 'react-native';
import Constants from '../common/Constants';
import {MessageTypes} from '../data/MessageAdapter';
const windowWidth = Dimensions.get('window').width;
import Icon from "react-native-vector-icons/MaterialIcons";

export default function RepliedView({message,children,repliedUser,userId,onPress}){

    function getSource(){

        let source

        let content = JSON.parse(message.content)

        let style = {
            aspectRatio: content.ratio ?? 1
        }

        if(content.cdnId){
            source = {uri: Constants.CDN_URL + 'img/' + content.cdnId + '?w=' + (styles.imageContainer.width/3)*2}
        }else if(content.file && content.file.uri){

            let file = content.file
            source = {uri: file.uri}
        }

        return {style,source}
    }

    function subjectName() {

        return repliedUser.userId == userId ? 'Та' : repliedUser.name
    }

    function objectName(){

        if(repliedUser.userId == message.fromUser.userId){
            return 'өөртөө'
        }else if(message.fromUser.userId == userId){
            return 'таньд'
        }else{
            return message.fromUser.name + '-д'
        }

    }

    return <View style = {styles.container} >

        { message && message.fromUser ?
            <View style = {styles.titleContainer} >
                <Icon name = "reply" color={"#666"} size = {14} />
                <Text style = {styles.title} > {subjectName()} {objectName()} хариулав</Text>
            </View> : null
        }

           {message ? <TouchableOpacity onPress = {onPress} style = {[styles.buble,{backgroundColor: message.type == MessageTypes.IMAGE ? 'transparent': '#E4EAF1'}]} >
               {message.type == MessageTypes.IMAGE ?
                   <View style = {[styles.imageContainer,getSource().style]} >
                    <Image style = {styles.image} source = {getSource().source} />
                   </View>
               : <Text style = {styles.text} >{message.content}</Text> }

           </TouchableOpacity>: null}
         {children}
    </View>
}

const styles = StyleSheet.create({

    container: {
        margin: 8,
        // justifyContent: 'flex-end'
    },

    buble: {
        backgroundColor: '#E4EAF1',
        opacity: 0.6,
        borderRadius: 6,
        minHeight: 36,
        justifyContent: 'center',
        marginTop: 0,
        overflow: 'hidden',
        marginBottom: -12,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
        margin: 8,
        marginBottom: 24
    },

    titleContainer:{
      flexDirection: 'row'
    },

    title:{
        fontFamily: 'RobotoCondensed-Regular',
        color: '#666',
        fontSize: 14,
        marginBottom: 4,
    },

    message:{
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
        marginTop: 8,
    },

    imageContainer: {
        backgroundColor: '#E4EAF1',
        borderRadius: 6,
        justifyContent: 'center',
        marginTop: 0,
        overflow: 'hidden',
        width: Math.floor(windowWidth/3)
    },

    image:{
        flex:1,
    },

})
