import messaging from '@react-native-firebase/messaging';
import Constants from './Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {Platform} from 'react-native';
import AppCenter from 'appcenter';

export default class PushNotification {

    static handle = (callback) => {

        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log('Notification caused app to open from background state:', remoteMessage,);
            // navigation.navigate(remoteMessage.data.type);
            callback(remoteMessage.data)
        });

        // Check whether an initial notification is available
        messaging().getInitialNotification().then(remoteMessage => {

            if (remoteMessage){
                console.log('Notification caused app to open from quit state:', remoteMessage,);
                callback(remoteMessage.data)
            }
        });

    }

    static requestUserPermission = async () => {

        const authorizationStatus = await messaging().requestPermission({
            // provisional: true,
        });

        if (authorizationStatus) {
            console.log('Permission status:', authorizationStatus);
        }
    }

    static saveToken = () => {

        messaging().getToken().then(token => {
            console.log(token)
            PushNotification.sendDeviceToken(token)
        });

        messaging().onTokenRefresh(token => {
            console.log(token)
            PushNotification.sendDeviceToken(token)
        });

    }

    static sendDeviceToken = async (token) => {

        AsyncStorage.setItem('deviceToken',token)

        let data = {
            rpUserId: await AsyncStorage.getItem('userId'),
            deviceToken: token,
            deviceId: await AppCenter.getInstallId(),
            osType: Platform.OS,
        }

        fetch(Constants.WSURL + "wsock/user/deviceToken", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {

                console.log(json)

            }).catch((error) => {

                console.log(error)
        });
    }

    static deleteDeviceToken = async () => {

        let deviceToken = await AsyncStorage.getItem('deviceToken')

        if(!deviceToken){
            return
        }

        fetch(Constants.WSURL + "wsock/user/deviceToken/" + deviceToken, {
            method: 'DELETE',
        }).then((response)=> {
                console.log(response)

                 AsyncStorage.removeItem('deviceToken')

            }).catch((error) => {

            console.log(error)
        });
    }
}
