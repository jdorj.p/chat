import React from 'react';
import {
    TouchableOpacity,
    StyleSheet,
} from 'react-native';
import IconWithBadge from '../components/IconWithBadge';
import {HeaderBackButton} from '@react-navigation/stack';

const styles = StyleSheet.create({
    button: {
        paddingLeft: 18,
        paddingRight: 18,
        height: '100%',
        justifyContent: 'center',
    },

    headerBackTitleStyle: {
        fontFamily: 'RobotoCondensed-Regular',
    }
});

function BackButton(props) {
    const goBack = () => {
        requestAnimationFrame(() => {
            props.onPress();
        });
    };

    return (
        <HeaderBackButton
            onPress={goBack}
            title={'Буцах'}
            truncatedTitle={''}
            backTitleVisible={false}
            allowFontScaling={false}
            titleStyle={styles.headerBackTitleStyle}
        />
    );
}

const NavigationOption = {
    headerStyle: {
        backgroundColor: '#fff',
    },
    headerTintColor: '#000',
    headerTitleStyle: {
                fontFamily: 'RobotoCondensed-Bold',
                // fontSize: 20,
                // alignSelf: 'center',
                // textAlign: 'center'
            },
    headerBackTitle: "Буцах",
};

export function createNavigationOptions(navigation, options = {}) {
    return {
        headerStyle: {
            backgroundColor: '#fff',
        },
        headerTintColor: '#000',
        headerTitleStyle: {
            fontFamily: 'RobotoCondensed-Bold',
            fontSize: 20,
            alignSelf: 'center',
            textAlign: 'center'
        },
        headerBackTitle: "Буцах",
        headerRight: (
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Notification')}>
                <IconWithBadge image={require('../image/notification.png')} color="#b0b0b0"/>
            </TouchableOpacity>
        ),
        ...(options.hasBackButton ? {
            headerLeft: <BackButton onPress={options.onBackButtonPress || (() => navigation.goBack())}/>,
        } : {})
    };
}

export default NavigationOption;
