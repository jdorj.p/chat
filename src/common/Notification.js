import React, { Component } from 'react';

import {
    Alert,
    AsyncStorage,
    AppState,
    Vibration,
} from 'react-native';

import NotificationSounds, { playSampleSound } from 'react-native-notification-sounds';


import Push from 'appcenter-push';

import { showMessage, hideMessage } from "react-native-flash-message";
import {EventRegister} from 'react-native-event-listeners';



export default class Notification {

    constructor(){


        Push.setListener({ onPushNotificationReceived: (pushNotification) => {


                if(pushNotification.customProperties && pushNotification.customProperties.badgeNumber){
                    EventRegister.emit('setBadge',parseInt(pushNotification.customProperties.badgeNumber));
                }


                // android tapp notification
                if(pushNotification.message === null){

                   this.notificationTapped(pushNotification)
                    return
                }


                if (AppState.currentState === 'active') {

                    this.showNotification(pushNotification)
                }
                else {

                    this.pushNotification = pushNotification
                    this.listener = AppState.addListener('appStateDidChange', this._handleAppStateChange);

                }
            }
        });
    }

    showNotification(pushNotification){

        console.log(pushNotification)

        showMessage({
            message: pushNotification.title,
            description: pushNotification.message ? pushNotification.message.substring(0, 100) + "..." : "",
            type: "default",
            backgroundColor: "#DB373D", // background color
            color: "#fff", // text color
            autoHide: true,
            onPress: () => {
                this.notificationTapped(pushNotification)
            },
        });

        Vibration.vibrate(500);

        NotificationSounds.getNotifications().then(soundsList => {
            console.log('SOUNDS',soundsList);
            /*
            Play the notification sound.
            pass the complete sound object.
            This function can be used for playing the sample sound
            */
            playSampleSound(soundsList[5]);
        });

    }

    notificationTapped(pushNotification){


        if(pushNotification.customProperties && pushNotification.customProperties.id){
            this.navigation.navigate('NotificationDetail',{id: pushNotification.customProperties.id})
        }else{
            this.navigation.navigate('Notification')
        }

    }

    _handleAppStateChange = (response) => {

        if (response.app_state === 'active') {

            this.notificationTapped(this.pushNotification)
            this.pushNotification = null
            this.listener.remove()
        }
    }

}

