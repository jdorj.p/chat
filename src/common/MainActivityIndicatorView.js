import React from 'react';

import ActivityIndicatorView from './ActivityIndicatorView';

export default class MainActivityIndicatorView extends React.Component {

    state = {
        animating: false
    }

    componentDidMount() {

        MainActivityIndicatorViewManager.view = this

    }

    show = (title) => {

        this.setState({
            animating: true,
            title: title
        })
    }

    hide = () => {

        this.setState({
            animating: false,
            title: undefined
        })
    }

    render() {

        return (<ActivityIndicatorView animating = {this.state.animating} title = {this.state.title} />)
    }

}


export class MainActivityIndicatorViewManager{

    static view

    static show = (title) => {
        MainActivityIndicatorViewManager.view.show(title)
    }

    static hide = () => {
        MainActivityIndicatorViewManager.view.hide()
    }
}
