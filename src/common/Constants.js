export default {
    WSURL: "",
    URL: "",
    CDN_URL: "",
    COLORS: {
        ERROR: '#DF2D43',
        SUCCESS: '#79DB16',
        INFO: '#30A9F0'
    }
};
export class Navigation {
    static navigation = undefined
}
