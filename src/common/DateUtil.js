import dateFormat from 'dateformat';
import moment from 'moment';

class DateUtil {

    format = (string) => {


        let now = moment()

        // let dateStr = string.split(".")[0]
        try {

            // let date = new Date(string)
            let date = moment(string)
            // let   date = Date.parse(string)

            if(date.year() == now.year()){

                if(date.month() == now.month()){

                    if(date.day() == now.day()){

                        return dateFormat(date, "HH:MM:ss");
                    }else{

                        return dateFormat(date, "mm сарын dd HH:MM");
                    }
                }else{
                    return dateFormat(date, "mm сарын dd HH:MM");
                }
            }else{
                return dateFormat(date, "yyyy.mm.dd HH:MM");
            }

        }catch (e) {

            console.log(e,string)
            return null
        }

    }


    secondsBetween = (dateStr,nextStr) => {

        if(!nextStr || !dateStr){
            return 0
        }

        // let date = new Date(dateStr.split(".")[0])
        // console.log('date',dateStr)
        let date = moment(dateStr)

        // let nextDate = new Date(nextStr.split(".")[0])
        // console.log('date', nextStr)
        let nextDate = moment(nextStr)

        return (date - nextDate) / 1000
    }

    ago = (dateStr) => {

        let now = moment()
        let date = moment(dateStr)

        let seconds = (now - date) / 1000

        // console.log('dateStr')
        // console.log(date)
        // console.log(now)
        // console.log(seconds)

        if(seconds == NaN){
            return ""
        }else if(seconds < 60){
            return Math.floor(seconds) + "с"
        }else if(seconds < 3600){
            return Math.floor(seconds/60)+"м"
        }else if(seconds < 86400){
            return Math.floor(seconds/3600)+"ц"
        }else{
            return Math.floor(seconds/86400)+"ө"
        }

    }

    getTime = (string) => {

        try {

            let date = moment(string)
             return date.toDate().getTime()

        }catch (e) {
            console.log('date error', e)
            return new Date().getTime()

        }

    }

    getIsoDate = () => {

        let date = moment()
        return dateFormat(date, "yyyy-mm-dd'T'HH:MM:sso");
    }

}

export default new DateUtil();
