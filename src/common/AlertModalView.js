import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import { StyleSheet } from "react-native";
import Input from '../components/Input';
import Button from '../components/Button';
import CustomAlert from './CustomAlert';
import Error from '../components/Error';

export class AlertModalView extends Component {

    constructor(props){
        super(props);

        this.state = {isModalVisible: false}
    }

    componentDidMount() {

        CustomAlert.AlertModalView = this
    }


    show = (title,message,status,okButtonText) => {

        this.setState({
            title: title,
            message: this.messagesToString(message,status),
            status: status,
            isModalVisible: true,
            confirm: false,
            okButtonText: okButtonText,
        })
    }

    confirm = (title,message,callback,status) => {

        this.setState({
            title: title,
            message: this.messagesToString(message,status),
            status: status,
            isModalVisible: true,
            confirm: true,
            okButtonText: null,
        })

        this.callback = callback
    }

    messagesToString = (messages,status) => {

        if(Array.isArray(messages)){

            let messageArray = []

            for(let i = 0; i < messages.length; i++){
                messageArray.push(messages[i].message)
            }

            return messageArray.join(" ")

        }else{

            if(status == "error"){
                return Error.message(messages)
            }else{
                return messages
            }
        }
    }

    ok = () => {

        this.setState({
            isModalVisible: false,
            confirm: false,
        })

        this.callback && this.callback(true)
        this.callback = undefined
    };

    cancel = () => {

        this.setState({
            isModalVisible: false,
            confirm: false,
        })

        this.callback && this.callback(false)
        this.callback = undefined
    }


    renderImage = () => {

        // if(this.state.status == "success"){
        //     return (<Image source = {require('../image/success.png')} />)
        // }else if(this.state.status == "error"){
        //     return (<Image source = {require('../image/failure.png')} />)
        // }else if(this.state.status == "love"){
        //     return (<Image source = {require('../image/love.png')} />)
        // }else{
        //     return null
        // }
    }

    renderTitle = () => {

        if(this.state.title){
            return <Text style={styles.title} >{this.state.title}</Text>
        }else{
            return null
        }
    }

    renderMessage = () => {

        if(this.state.message){
            return (<Text style={styles.message} >{this.state.message}</Text>)
        }else{
            return null
        }
    }

    render() {

        return (
            <Modal isVisible={this.state.isModalVisible} onBackdropPress={this.cancel} >
                <View style={styles.container}>
                    <View style = {{backgroundColor: "#fff", alignItems: 'center', padding: 16,}} >
                    {/*{this.renderImage()}*/}
                    {this.renderTitle()}
                    {this.renderMessage()}

                    <View style = {{flexDirection: 'row'}} >
                        {(this.state.confirm) ? (<Button style ={{flex: 1, marginRight: 8,zIndex: 1,}} title = "Үгүй" hideGradient second onPress = {this.cancel} />) : null}
                        {(this.state.confirm) ? (<Button style ={{flex: 1, marginLeft: 8}} title = "Тийм" onPress = {this.ok} />) : (<Button style ={{flex:1}} title = { this.state.okButtonText ?? "За"} onPress = {this.ok} />)}
                    </View>
                    </View>
                </View>
            </Modal>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        backgroundColor: '#fff',
        borderRadius: 8,
        overflow: 'hidden',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 14,
        textAlign: 'center',
        marginBottom: 32,
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    }
});
