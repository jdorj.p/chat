export default class CustomAlert {
    static AlertModalView;

    static alert = (title, message, status, okButtonText = null) => {
        CustomAlert.AlertModalView.show(title, message, status, okButtonText);
    };

    static confirm = (title, message, callback, status) => {
        CustomAlert.AlertModalView.confirm(title, message, callback, status);
    };
}
