export function extractErrorMessage(responseBody, defaultMessage = 'Үйлдэл хийх явцад алдаа гарлаа.') {
    if (responseBody.errors) {
        if (Array.isArray(responseBody.errors)) {
            let messageArray = [];
            for (let error of responseBody.errors) {
                messageArray.push(error.message);
            }
            return messageArray.join(' ');
        }
    }
    return defaultMessage;
}
