import React, { Component } from 'react';

import {
    Alert,
    Platform,
    PushNotificationIOS,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

// import AppCenter from 'appcenter';
import Constants from './Constants';
import DeviceInfo from 'react-native-device-info';
import VersionNumber from 'react-native-version-number';
import CustomAlert from './CustomAlert';
import {EventRegister} from 'react-native-event-listeners';
import * as AppCenter from 'appcenter';
import PushNotification from './PushNotification';

class UserUtil {

   static instance = null

    constructor(){

       this.saveDeviceId()

        this.state = {};
   }



   static getInstance() {
        if (UserUtil.instance == null) {
            UserUtil.instance = new UserUtil();
        }

        return this.instance;
    }


    saveUserData(data){

        if(data.unread) {
            EventRegister.emit('setBadge', data.unread);
        }

        this.emitEvent(data)

           return AsyncStorage.multiSet([
                ["token",(data.token ?? "")],
                ["userId",(data.userId ?? "")],
                ["user",JSON.stringify(data)],
            ]);

    }


     updateUserData = () => {

         return new Promise(async (resolve,reject)=> {

             fetch(Constants.URL + "redpointapi/user/data/", {
                 headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'application/json',
                     'Authorization': await AsyncStorage.getItem("token"),
             },
         }).
             then((response) => response.json())
                 .then(async (json) => {

                     // console.log('user data', json)

                     if (json.data) {

                         this.emitEvent(json.data)

                        await AsyncStorage.multiSet([
                             ['userId',json.data.userId],
                             ["user", JSON.stringify(json.data)],
                         ])

                         // EventRegister.emit('setBadge', json.data.unread);

                        resolve(json.data)
                     }


                 }).catch((error) => {

                 reject(error)
                 console.log(error)
             });

         })

    }

    emitEvent = (data) => {

       // console.log('emit event',data)
        // EventRegister.emit('alertUserTab', (data.emailConfirmed != true || data.userDataUpdate));
    }

    updateSavedPasswords = (newPassword) => {

        AsyncStorage.getItem('password').then((password)=>{

            if(password){
                AsyncStorage.setItem('password',newPassword)
            }

        })

        AsyncStorage.getItem('bpassword').then((password)=>{

            if(password){
                AsyncStorage.setItem('bpassword',newPassword)
            }

        })


    }


    updateSavedPhones = (phone) => {


        AsyncStorage.getItem('username').then((username)=>{

            if(/^\d{8}$/.test(username)){

                AsyncStorage.setItem('username',phone)
            }

        })

        AsyncStorage.getItem('busername').then((username)=>{

            if(/^\d{8}$/.test(username)){

                AsyncStorage.setItem('busername',phone)
            }

        })

    }


    getData(){

         return new Promise((resolve,reject)=> {

             AsyncStorage.getItem("user").then((data) => {

                 let json = JSON.parse(data)
                 this.emitEvent(json)
                 resolve(json)

             }).catch((error)=> {

                 reject(error)
             })
         })
    }

    saveCredential(username, password){


        return AsyncStorage.multiSet([
            ["username",username],
            ["password",password],
        ]);
    }

    saveBioCredential(username, password){


        return AsyncStorage.multiSet([
            ["busername",username],
            ["bpassword",password],
        ]);
    }

    getCredentials(){

        return new Promise((resolve,reject)=> {

            AsyncStorage.multiGet([
                "username",
                "password",
            ]).then((array) => {

                let object = {}

                for (let i = 0; i < array.length; i++) {

                    let data = array[i]

                    object[data[0]] = data[1] != "" ? data[1] : null
                }

                resolve(object)

            }).catch((error)=> {

                reject(error)
            })
        })
    }

    getBioCredentials(){

        return new Promise((resolve,reject)=> {

            AsyncStorage.multiGet([
                "busername",
                "bpassword",
            ]).then((array) => {

                console.log(array)

                let object = {}

                for (let i = 0; i < array.length; i++) {

                    let data = array[i]

                    object[data[0]] = data[1] != "" ? data[1] : null
                }

                resolve(object)

            }).catch((error)=> {

                reject(error)
            })
        })
    }

    deleteUserData(callback){

       this.deleteDeviceId()
       PushNotification.deleteDeviceToken()

       AsyncStorage.multiRemove([
            "token",
            "userId",
            "user",
        ],callback);

    }


    async saveDeviceId(){

        const token = await AsyncStorage.getItem("token");
        const installId = await AppCenter.getInstallId();

        // console.log(installId)

        if(!token){
            return
        }


        let url = Constants.URL + 'redpointapi/notification/device';

        let data = {
            "deviceId": installId,
            "deviceOs": Platform.OS,
            "type": "add",
        }

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {

            }).catch((error) => {

            console.error(error);
        });
    }

    getDeviceInfo = () => {

        let jsVersion = (this.state.updateMetadata) ? this.state.updateMetadata.label.replace('v','.') : ''
        let version = VersionNumber.appVersion + jsVersion
        // let version = "3.0"

       return DeviceInfo.getBrand() + " " + DeviceInfo.getModel() + " " + DeviceInfo.getSystemName() + " " + DeviceInfo.getSystemVersion() + " " + version
    }

    async deleteDeviceId(){

        const token = await AsyncStorage.getItem("token");
        const installId = await AppCenter.getInstallId();

        if(!token){
            return
        }

        let url = Constants.URL + 'redpointapi/notification/device';

        let data = {
            "deviceId": installId,
            "deviceOs": Platform.OS,
            "type": "remove",
        }

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {

            }).catch((error) => {

            console.error(error);
        });
    }

}

export default UserUtil.getInstance();
