import React, { Component } from "react";
import {View, Text, Image, ImageBackground, TouchableOpacity , StyleSheet} from 'react-native';
import Util from './Util';

const SIZE = 48

export class ProfileImage extends Component {

    constructor(props){
        super(props);

        this.state = {
        }
    }

    componentDidMount() {

    }

    renderAvatar = (user,style,textStyle) => {

        let uri = user.avatar ? user.avatar : user.profileImg

        if(uri){
            return (<Image style = {[styles.image,style]} source = {{uri: uri}} />)
        }

        let name = user.name ? user.name : user.firstName

        if(name){
            return (<View style = {[styles.textContainer,style]} ><Text style = {[styles.text,textStyle]}>{name[0].toUpperCase()}</Text></View>)
        }
    }

    navigateUser = () => {

        // console.log('navigateUser',this.props.user)
        // Util.navigation.navigate('U')
        Util.navigation.navigate('Profile',{user: this.props.user[0]})
    }

    renderAvatars = () => {


        if(this.props.url){

                let source = this.props.url ? {uri:this.props.url} : undefined
                return (<Image style = {[styles.image]} source = {source} />)
        }

        let fontSize = 32
        let quarterFontSize = 14
        let quarterPadding = 4
        let size = this.props.size

        if(size){
            quarterFontSize = Math.floor(size * quarterFontSize/SIZE)
            quarterPadding = Math.floor(size * quarterPadding/SIZE)
            fontSize =  Math.floor(size * fontSize/SIZE)
        }

        if(Array.isArray(this.props.user)){

            let users = this.props.user
            let length = this.props.user.length

            switch (length) {

                case 1:
                    return (<View style = {{flex:1}} onPress = {this.navigateUser} >
                        {this.renderAvatar(users[0],{},{fontSize: fontSize,fontFamily: 'Roboto-Regular'})}
                    </View>)
                    break;
                case 2:
                    return (<View style = {{flex:1,flexDirection: 'row'}} >
                        {this.renderAvatar(users[0],{marginRight: 1},{paddingLeft: quarterPadding,fontSize: quarterFontSize})}
                        {this.renderAvatar(users[1],null, {paddingRight: quarterPadding,fontSize: quarterFontSize})}
                    </View>)
                    break;
                case 3:
                    return (<View style = {{flex:1,backgroundColor: '#fff', flexDirection: 'row'}} >
                            {this.renderAvatar(users[0],null,{paddingLeft: 4,fontSize: quarterFontSize})}
                        <View style = {{flex: 1,marginLeft: 1}} >
                            {this.renderAvatar(users[1],{marginBottom: 1},{paddingRight: quarterPadding, paddingTop: quarterPadding, fontSize: quarterFontSize})}
                            {this.renderAvatar(users[2],null,{paddingRight: quarterPadding, paddingBottom: quarterPadding, fontSize: quarterFontSize})}
                        </View>
                    </View>)
                    break;
                case 4:
                    return (<View style = {{flex:1, backgroundColor: '#fff'}} >
                        <View style = {{flex: 1, flexDirection: 'row'}} >
                            {this.renderAvatar(users[0],{marginRight: 1},{paddingLeft: quarterPadding, paddingTop: quarterPadding, fontSize: quarterFontSize})}
                            {this.renderAvatar(users[1],null,{paddingRight: quarterPadding, paddingTop: quarterPadding, fontSize: quarterFontSize} )}
                        </View>
                        <View style = {{flex: 1, flexDirection: 'row', marginTop: 1}} >
                            {this.renderAvatar(users[2],{marginRight: 1},{paddingLeft: quarterPadding, paddingBottom: quarterPadding, fontSize: quarterFontSize})}
                            {this.renderAvatar(users[3],null,{paddingRight: quarterPadding, paddingBottom: quarterPadding, fontSize: quarterFontSize})}
                        </View>
                    </View>)
                    break;
                default:
                    return (
                        <View style = {{flex:1,backgroundColor: '#fff'}}>
                            <View style = {{flex: 1, flexDirection: 'row'}} >
                                {this.renderAvatar(users[0],{marginRight: 1},{paddingLeft: quarterPadding, paddingTop: quarterPadding, fontSize: quarterFontSize})}
                                {this.renderAvatar(users[1], null, {paddingRight: quarterPadding, paddingTop: quarterPadding, fontSize: quarterFontSize})}
                            </View>
                            <View style = {{flex: 1, flexDirection: 'row', marginTop: 1}} >
                                {this.renderAvatar(users[2],{marginRight: 1},{paddingLeft: quarterPadding, paddingBottom: quarterPadding, fontSize: quarterFontSize})}
                                <View style = {styles.textContainer}><Text style = {[styles.text,{paddingRight: quarterPadding, paddingBottom: quarterPadding, fontSize: quarterFontSize}]} >+{length - 3}</Text></View>
                            </View>
                    </View>
                    )

            }

        }

        return null
    }

    render() {

        let style = {}
        let size = this.props.size
        if(size){
            style.height = size
            style.width = size
            style.borderRadius = Math.floor(size / 2)
        }

        return (
            <TouchableOpacity disabled = {this.props.disabled || !(this.props.user && this.props.user.length == 1)} onPress = {this.navigateUser} >
                <View style = {[styles.imageBackground,style,this.props.style]} >
                        {this.renderAvatars()}
                </View>
                <View style = {styles.childrenContainer} >{this.props.children}</View>
            </TouchableOpacity>
        );
    }

}


const styles = StyleSheet.create({

    imageBackground: {
        // zIndex: 1,
        height: SIZE,
        width: SIZE,
        backgroundColor: '#D8D8D8',
        borderRadius: SIZE/2,
        overflow: 'hidden',
    },


    image: {
        flex:1,
        backgroundColor: '#D8D8D8',
    },

    childrenContainer:{
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    },

    textContainer: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#D8D8D8',
    },

    text: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#999999',
        fontSize: 16,
    },



});
