import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    KeyboardAvoidingView,
    SafeAreaView,
    Platform,
} from 'react-native';
import Input from '../components/Input';
import Button from '../components/Button';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Constants from '../common/Constants';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from "@react-native-community/async-storage";


export default class NewCard extends React.Component {


    constructor(props){
        super(props);

        this.state = {
            isLoading: false,
            secure:  true,
            vision: 'visibility',
        }
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: 'Карт нэмэх',
        })

    }


    validate = async () => {

        // this.props.navigation.navigate("TokenizePayCard",{
        //     // callback: this.props.route.params.callback,
        //     // card: json.data.card,
        // })
        // return

        let card = this.refs.card.validate();
        let expireDate = this.refs.expireDate.validate();
        let cvv = this.refs.cvv.validate();

        if(card && expireDate && cvv){

            this.sendData({
                userId: await AsyncStorage.getItem("userId"),
                cardNumber: card.replace(/\s/g, ""),
                expireDate: expireDate,
                cardCvv: cvv,
            })
        }
    };


    sendData = async (data) => {

        console.log('new card',data)

        this.setState({isLoading: true});

        fetch(Constants.WSURL + "wsock/payment/confirmCard", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((json) => {

                console.log(json)

                if (json.status != "000"){

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                    this.setState({isLoading: false});

                } else {

                    console.log("TokenizePayCard")

                    this.props.navigation.navigate("TokenizePayCard",{
                        callback: this.props.route.params.callback,
                        card: json.entity,
                    })
                }

            }).catch((error) => {

            this.setState({isLoading: false});
        });

    };




    render() {

        return (
            <SafeAreaView style = {{flex: 1,backgroundColor: '#fff'}} >
                <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} >
                    <InvertibleScrollView keyboardDismissMode = "interactive" inverted>
                        <View style={styles.container} >

                            <Text style = {[styles.text,{marginTop: 16}]} >Та картын дугаар, хүчинтэй хугацаа болон картын cvv дугаар (картын ард байрлах 3 оронтой тоо) оруулаад үргэлжлүүлэх товчийг дарна уу.</Text>
                            <Input ref = "card" image = {require('../image/card.png')} maxLength = {23} label = "Картын дугаар" pan keyboardType = "number-pad" required pattern = {/[\d\s]{19,23}$/} error = "16-19 оронтой дугаар оруулна уу!" returnKeyType = { "done" } onSubmitEditing={()=>this.refs.expireDate.focus()} />
                            <Input ref = "expireDate" style = {{marginTop: 16}} maxLength = {5} placeholder = "MM/YY" label = "Картын хүчинтэй хугацаа" expireDate keyboardType = "number-pad" required pattern = {/[\d\/]{5}$/} error = "Картын хүчинтэй хугацаа оруулна уу!" returnKeyType = { "done" } onSubmitEditing={()=>this.refs.cvv.focus()} />
                            <Input ref = "cvv" style = {{marginTop: 16}} maxLength = {3} placeholder = "CVV" label = "Картын арын 3 оронтой тоо" keyboardType = "number-pad" required pattern = {/[\d]{3}$/} error = "Картын ард байрлах 3 оронтой дугаар оруулна уу!" returnKeyType = { "done" } onSubmitEditing={this.validate} />

                            <Button style = {{marginTop: 32, backgroundColor: '#FAB615'}}  title = "Үргэлжлүүлэх" onPress = {this.validate} />

                        </View>
                    </InvertibleScrollView>

                </KeyboardAvoidingView>
                <ActivityIndicatorView animating = {this.state.isLoading}/>
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 16,
        paddingTop: 0,
    },

    pagerContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        alignItems: 'center',
    },

    text: {
        fontFamily: 'Roboto-Regular',
        color: '#000',
        fontSize: 14,
        marginTop: 16,
        marginBottom: 16,
    }
});
