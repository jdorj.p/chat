import React, {forwardRef} from 'react';
import {View, Text} from 'react-native';
import { StyleSheet } from "react-native";
import Button from '../components/Button';
import Input from '../components/Input';
import {bottomModal} from '../components/BottomModal';

function AmountModal(props,ref) {

    const cancel = () => {
        bottomModal.close()
    }

    function onContinue() {

        let amount = amountRef.current.validate();
        let description

        if(descriptionRef.current){
           description = descriptionRef.current.validate();
        }

        if(amount){

            if(!(props.description && description)){
                return
            }

            let type = props.type ? props.type : "request"
            props.onContinue(amount,type,description)
            bottomModal.close()
        }

    }

    const amountRef = React.useRef()
    const descriptionRef = React.useRef()

    return (<View style={styles.container}>
                    <Text style={styles.title} >{props.title ? props.title : 'Мөнгөн дүн оруулна уу'}.</Text>
                    {(props.text) ? (<Text style={styles.message} >{props.text}</Text>): null}
                    <Input ref = {amountRef} label = "Мөнгөн дүн" required keyboardType = "number-pad" />
                    {props.description ? <Input ref = {descriptionRef} label = "Мэдээлэл"  required multiline /> : null}
                    <View style = {{flexDirection: 'row',marginTop: 24}} >
                        <Button style ={{flex: 1, marginRight: 8}} title = "Болих" hideGradient second onPress = {cancel} />
                        <Button style ={{flex: 1, marginLeft: 8}} title = "Үргэлжлүүлэх" onPress = {onContinue} />
                    </View>
    </View>);
}

export default AmountModal = forwardRef(AmountModal);

const styles = StyleSheet.create({


    container: {
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 14,
        marginBottom: 16,
    },

    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 8,
        marginLeft: 0,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: '#666666',
        marginLeft: 8,
    },

    balance: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: '#000',
        marginRight: 8,
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    }
});
