import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';

export default class CardAddButton extends Component {


    constructor(props) {
        super(props);

    }


    onPress = () => {

        this.props.onPress && this.props.onPress(this)
    }

    render() {

        return (<TouchableOpacity
            onPress={this.onPress}
            style={styles.touchableOpacity}
        >
            <View style = {styles.container}>
                <Text style={styles.text}>
                    + Төлбөрийн карт нэмэх
                </Text>
            </View>
        </TouchableOpacity>);

    }

}



const styles = StyleSheet.create({

    touchableOpacity: {

        borderRadius: 8,
        borderStyle: 'dashed',
        borderWidth: 1,
        borderColor: '#DB373D',

        height: 86,
        backgroundColor: '#fff',

    },

    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    text: {
        fontFamily: 'Roboto-Medium',
        color: '#DB373D',
        fontSize: 14,
        margin: 8,
    },

});
