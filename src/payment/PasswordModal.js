import React, {forwardRef} from 'react';
import {View, Text} from 'react-native';
import { StyleSheet } from "react-native";
import Button from '../components/Button';
import Input from '../components/Input';
import {bottomModal} from '../components/BottomModal';

function PasswordModal(props,ref) {

    const cancel = () => {
        bottomModal.close()
    }

    function onContinue() {

        let password = passwordRef.current.validate();

        if(password){
            props.onContinue(password)
            bottomModal.close()
        }

    }

    const passwordRef = React.useRef()

    return (<View style={styles.container}>
                    <Text style={styles.title} >Нууц үгээ оруулна уу.</Text>
                    {(props.text) ? (<Text style={styles.message} >{props.text}</Text>): null}
                    <Input ref = {passwordRef} label = "Нууц үг" required secureTextEntry hideSecureText />
                    <View style = {{flexDirection: 'row',marginTop: 24}} >
                        <Button style ={{flex: 1, marginRight: 8}} title = "Болих" hideGradient second onPress = {cancel} />
                        <Button style ={{flex: 1, marginLeft: 8}} title = "Үргэлжлүүлэх" onPress = {onContinue} />
                    </View>
    </View>);
}

export default PasswordModal = forwardRef(PasswordModal);

const styles = StyleSheet.create({


    container: {
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 14,
        marginBottom: 16,
    },

    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 8,
        marginLeft: 0,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: '#666666',
        marginLeft: 8,
    },

    balance: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: '#000',
        marginRight: 8,
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    }
});
