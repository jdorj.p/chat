import React from 'react';
import {
    StyleSheet,
    SafeAreaView,
    View,
    ScrollView,
    Text, Platform, KeyboardAvoidingView,
} from 'react-native';

import Button from '../components/Button';
import CustomAlert from '../common/CustomAlert';
import Constants from '../common/Constants';
import AsyncStorage from "@react-native-community/async-storage";
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Input from '../components/Input';
import SimpleToast from 'react-native-simple-toast';


export default class TokenizePayCard extends React.Component {


    constructor(props){
        super(props);

        this.state = {isLoading: false}

    }

    componentDidMount(): void {
        this.props.navigation.setOptions({
            title: 'Баталгаажуулах',
        })
    }

    tokenize = async () => {

        let amount = this.refs.amount.validate();
        let password = this.refs.password.validate();

        if(amount && password){

            let card = this.props.route.params.card
            let callback = this.props.route.params.callback

            this.setState({isLoading: true})

            callback && callback({
                cardId: card.id,
                confirmAmount: amount.replace(/\./g, ""),
                password: password,
            }, ()=>{
                this.props.navigation.navigate('Chat')
                this.setState({isLoading: false})
            })

        }
    };

    tokenizeCard = async (data) => {

        this.setState({isLoading: true});

        fetch(Constants.WSURL + "wsock/payment/payInvoice", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((json) => {


                if (json.status != "000"){

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                    this.setState({isLoading: false});

                } else {

                    this.setState({isLoading: false});

                    CustomAlert.alert("Амжилттай","Төлбөр амжилттай.", 'success');
                    this.props.navigation.navigate('Chat')

                }

            }).catch((error) => {

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');
            this.setState({isLoading: false});
        });

    };


    render() {


        return (
            <SafeAreaView style = {{flex: 1, backgroundColor: '#fff'}} >
                <View style = {styles.container}>
                    <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} >
                    <ScrollView>
                        <View style = {{padding: 16}} >
                            <Text style = {styles.text} >Уг картыг баталгаажуулахад 01.00--99.00₮ ийн хооронд гүйлгээ санаандгүй байдлаар таны данснаас хасагдах болно.</Text>
                            <Text style = {[styles.text,{marginTop: 16}]} >Та тухайн гүйлгээний мэдээллээ харилцагч банкны аппликейшн дээрээс шалгаад гүйлгээний дүнгээ доор оруулна уу.</Text>
                            <Text style = {[styles.text,{marginTop: 16}]} >Таны картаас хасагдсан уг баталгаажуулах дүн карт баталгаажсаны дараа буцаж орох болно.</Text>

                            <Input ref = "amount" style = {{marginTop: 16}} maxLength = {5} tokenizeAmount label = "Гүйлгээний дүн" placeholder = "Гүйлгээний дүн" keyboardType = "number-pad" required  returnKeyType = { "done" }  onSubmitEditing={() => { this.refs.password.focus() }} />
                            <Input ref = "password" label = "Нууц үг" required returnKeyType = { "done" } secureTextEntry hideSecureText onSubmitEditing={this.tokenize} />

                            <Button style = {{marginTop: 16}} title = "Баталгаажуулах" onPress = {this.tokenize} />
                        </View>
                    </ScrollView>
                    </KeyboardAvoidingView>
                </View>
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    text:{
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#333333',
    }
});
