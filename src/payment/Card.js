import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import Util from '../common/Util';
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Card extends Component {


    constructor(props) {
        super(props);

    }


    onPress = () => {

        this.props.onPress && this.props.onPress(this.props.item)
    }

    render() {

        let item = this.props.item

        return (<TouchableOpacity
            onPress={this.onPress}
            style={styles.touchableOpacity}
        >
            <View style = {styles.container}>

                <Image style = {styles.image} source={{uri: item.bankLogo}} resizeMode={"contain"} />
                <View style = {{flex:1}} >
                    <Text style={styles.text}>{Util.formatMaskedPan(item.cardNumber)}</Text>
                    {/*<View style = {{flexDirection: "row", marginTop: 4, alignItems: 'center'}} >*/}
                    {/*    {(item.confirmed == "success") ? (<Icon name = "check-circle" color = "#66D15F" size = {24} style = {{marginRight: 8}} />) : (null) }*/}
                    {/*    <Text style={styles.status}>{item.confirmStatus}</Text>*/}
                    {/*</View>*/}
                </View>
            </View>
        </TouchableOpacity>);

    }

}



const styles = StyleSheet.create({

    touchableOpacity: {

        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#e0e0e0',
        padding: 16,
        backgroundColor: '#fff',
        margin: 16,
        marginTop: 8,
        marginBottom: 8,


        minHeight: 86,
    },

    container: {
        flex: 1,
        flexDirection: 'row',

        alignItems: 'center',
        // justifyContent: 'center',
    },

    image: {
        height: 54,
        width: 54,
        borderRadius: 12,
        marginRight: 16,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        color: '#333333',
        fontSize: 16,
    },

    status: {
        fontFamily: 'Roboto-Regular',
        color: '#333333',
        fontSize: 14,
    },

});
