import React, {forwardRef} from 'react';
import {View, Text} from 'react-native';
import { StyleSheet } from "react-native";
import Button from '../components/Button';
import {bottomModal} from '../components/BottomModal';
import Util from '../common/Util';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';

function InvoiceModal(props,ref) {

    const cancel = () => {
        bottomModal.close()
    }

    function pay() {

        bottomModal.close()

        Util.navigation.navigate('ChooseCard',{callback: async ({cardId,confirmAmount,password},callback)=>{

            let params = {
                id: props.invoice.invoiceId,
                cardId: cardId,
                confirmAmount:confirmAmount,
                password: password,
                userId: await AsyncStorage.getItem("userId"),
                optionChatId: props.invoice.optionChatId,
            }

                fetch(Constants.WSURL + "wsock/payment/payInvoice", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': await AsyncStorage.getItem('token'),
                    },
                    body: JSON.stringify(params)
                })
                    .then((response) => {

                        console.log(response)
                        return response.json()

                    })
                    .then((json) => {


                        if (json.status != "000"){
                            callback && callback(json.message)
                            CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                        } else {
                            callback && callback()
                            CustomAlert.alert("Амжилттай","Гүйлгээ амжилттай.", 'success');
                        }

                    }).catch((error) => {

                    console.log(error);
                    callback && callback(error)
                    CustomAlert.alert("Алдаа гарлаа!","Дахин оролдоно уу", 'error');
                });

        }})

    }

    return (<View style={styles.container}>
                    <Text style={styles.title} >Нэхэмжлэх төлөх</Text>
                    {/*<Text style={styles.message} >Эскроу дансруу {Util.formatCurrency(props.invoice.amount)} байршуулах уу?</Text>*/}
                    <Text style={styles.message} >{props.invoice.message}</Text>
                    <View style = {{flexDirection: 'row',marginTop: 24}} >
                        <Button style ={{flex: 1, marginRight: 8}} title = "Үгүй" hideGradient second onPress = {cancel} />
                        <Button style ={{flex: 1, marginLeft: 8}} title = "Тийм" onPress = {pay} />
                    </View>
    </View>);
}

export default InvoiceModal = forwardRef(InvoiceModal);

const styles = StyleSheet.create({


    container: {
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16,
    },

    message: {
        fontFamily: 'Roboto-Regular',
        color: '#1F1F1F',
        fontSize: 14,
        marginBottom: 16,
    },

    textContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 8,
        marginLeft: 0,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: '#666666',
        marginLeft: 8,
    },

    balance: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: '#000',
        marginRight: 8,
    },

    cancel: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#BBBBBB',
        flex: 1,
        marginRight: 8
    }
});
