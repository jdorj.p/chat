import React from 'react';
import {StyleSheet, Text, View,ScrollView, SafeAreaView} from 'react-native';
import HTML from 'react-native-render-html';
import ActivityIndicatorView from "../common/ActivityIndicatorView";
import Button from '../components/Button';
import Border from '../components/Border';

export default class CardTerms extends React.Component {

    constructor(props){
        super(props);

        this.state = { isLoading: false, content: ""};
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: 'Карт нэмэх',
        })

        this.getData()
    }


    getData() {

        if (this.state.isLoading){
            return;
        }

        let state = this.state;
        state.isLoading = true

        this.setState(state);

        // const url = 'https://api.minu.mn/redpointapi/files/card_service_conditions.html'
        const url = 'http://api.minu.mn/wsock/nuutslal.html'

        return fetch(url)
            .then((response) => response.text())
            .then((response) => {

                this.setState({
                    isLoading: false,
                    content: response,
                });

            })
            .catch((error) => {
                console.error(error);
                this.setState({
                    isLoading: false,
                });
            });
    }

    continue = () => {

        if(this.state.confirmTerm == undefined){
            this.setState({confirmTerm: false})
        }

        if(this.state.confirmTerm) {

            this.props.navigation.navigate('NewCard', {
                callback: this.props.route.params.callback
            })

        }

    }


    render() {

        return (
                <SafeAreaView style={{flex: 1,backgroundColor: '#fff'}} >
            <View style={{flex: 1}}>
                <ScrollView style={styles.scrollView}>
                    <View>
                        <HTML html={this.state.content} style = {styles.content} />
                    </View>
                </ScrollView>
                <Border/>
                <View style = {styles.bottomContainer} >
                    <View style = {{flexDirection: 'row', alignItems: 'center'}} >
                        <Button image = {(this.state.confirmTerm == true) ? require('../image/checked.png') : require('../image/check.png')} onPress = {()=>this.setState({confirmTerm: !(this.state.confirmTerm)})} style = {{marginLeft: 3, marginRight: 16}} hideGradient />
                        <Button title = "Үйчилгээний нөхцөлийг зөвшөөрч байна" onPress = {()=>this.setState({confirmTerm: !(this.state.confirmTerm)})} hideGradient titleStyle = {{color: '#000', fontFamily: 'Roboto-Regular', textDecorationLine: 'underline'}} />
                    </View>
                    { (this.state.confirmTerm == false) ? (<Text style = {styles.error} >Үйлчилгээний нөхцөлийг зөвшөөрнө үү!</Text>) : null }
                    <Button title = "Үргэлжлүүлэх" onPress = {this.continue}  />
                </View>
                <ActivityIndicatorView animating = {this.state.isLoading} light/>
            </View>
                 </SafeAreaView>

        );
    }
}


const styles = StyleSheet.create({

    scrollView: {
        flex: 1,
    },

    content:{
        color: '#333333',
        fontSize: 14,
    },

    error:{
        color: '#EB5757',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        marginBottom: 16,
    },

    bottomContainer: {
        padding: 16,
        paddingTop: 0,
        backgroundColor: '#fff',
    },

});
