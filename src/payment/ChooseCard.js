import React from 'react';
import {
    StyleSheet,
    SafeAreaView,
    View,
    Text,
    FlatList,
} from 'react-native';

import ActivityIndicatorView from '../common/ActivityIndicatorView';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import UserUtil from '../common/UserUtil';
import CustomAlert from '../common/CustomAlert';
import CardAddButton from './CardAddButton';
import Card from './Card';
import AsyncStorage from "@react-native-community/async-storage";
import Constants from '../common/Constants';
import Util from '../common/Util';
import {bottomModal} from '../components/BottomModal';
import InvoiceModal from './InvoiceModal';
import PasswordModal from './PasswordModal';

export default class ChooseCard extends React.Component {


    constructor(props){
        super(props);

        this.state = {
            user: {},
            dataSource: [{}],
            isLoading: false,
            dataVersion: 0,
        }

    }


    componentDidMount() {

        this.props.navigation.setOptions({
            title: 'Төлбөрийн карт',
        })

        this.unsubscribeFocus = this.props.navigation.addListener(
            'focus',
            payload => {

                this.getData()
            }
        );

    }

    componentWillUnmount() {

        this.unsubscribeFocus();

    }

    getData = async () =>  {


        this.setState({isLoading: true});

        let userId = await AsyncStorage.getItem("userId");

        let url = Constants.WSURL + "wsock/user/cardListByUserId?userId=" + userId

        fetch(url,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json())
            .then((json) => {

                console.log(json)

                if(json.status == "000"){

                    let dataSource = [{}]



                    if(json.entity) {

                        for (let i = 0; i < json.entity.length; i++) {

                            dataSource.push({key: String(i), value: json.entity[i]})

                        }
                    }

                    if(dataSource.length == 1 && this.state.dataVersion == 0){
                        this.newCard()
                    }


                    this.setState({
                        isLoading: false,
                        dataSource: dataSource,
                        dataVersion: this.state.dataVersion + 1,
                    });

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');

                    this.setState({
                        isLoading: false,
                    });
                }


            })
            .catch((error) => {

                CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

                // console.error(error);
                this.setState({
                    isLoading: false,
                });

            });
    }


    newCard = () => {

        this.props.navigation.navigate('CardTerms',{
            callback: this.props.route.params.callback
        })

    }


    cardSelected  = (card) => {

        bottomModal.open(<PasswordModal onContinue = {(password)=>{

            let callback = this.props.route.params.callback

            this.setState({isLoading: true})

            callback && callback({
                cardId: card.id,
                password: password,
            }, (e)=>{

                if(!e){
                    this.props.navigation.goBack()
                }

                this.setState({isLoading: false})
            })

            // this.pay({
            //     id: this.props.route.params.invoice.invoiceId,
            //     cardId: card.id,
            //     password: password,
            //
            // })

        }} />)
    }


    pay = async (data) => {

        this.setState({
            isLoading: true
        })

        data.userId = await AsyncStorage.getItem("userId");

        fetch(Constants.WSURL + "wsock/payment/payInvoice", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => {

                console.log(response)
                return response.json()

            })
            .then((json) => {

                console.log(json)

                this.setState({
                    isLoading: false,
                })


                if (json.status != "000"){

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                } else {
                    CustomAlert.alert("Амжилттай","Төлбөр амжилттай.", 'success');
                    this.props.navigation.goBack()
                }


            }).catch((error) => {

            console.log(error);

            this.setState({
                isLoading: false,
            })

            CustomAlert.alert("Алдаа гарлаа!","Дахин оролдоно уу", 'error');
        });

    };



    render() {

        return (
            <SafeAreaView style = {{flex: 1, backgroundColor: '#fff'}} >

                <FlatList
                    data={this.state.dataSource}
                    renderItem={({item, index}) =>{

                        if(index == 0){
                            return (
                                <View style = {{margin: 16}} >
                                    <Text style = {styles.text} >Банкны картаар төлбөрөө хийнэ үү.</Text>
                                    <CardAddButton onPress = {this.newCard} />
                                    <Text style = {styles.title} >RedPoint-д бүртгэлтэй картаас сонгох</Text>

                                    {(this.state.dataSource.length == 1 && this.state.isLoading)? ( <View style = {{height: 64}}><ActivityIndicatorView light animating = {true} small title = {"Түр хүлээнэ үү"} /></View>) : null}
                                </View>)
                        }

                        return (<Card item = {item.value} onPress = {() => this.cardSelected(item.value)}/>)
                    }}
                />

                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 24,
        color: '#000',
        marginTop: 16,
        marginBottom: 16,
    },

    text:{
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#333333',
        marginBottom: 16,
    }
});
