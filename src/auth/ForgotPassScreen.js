import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    StatusBar,
    Alert,
    Platform,
} from 'react-native';
import Input from '../components/Input';
import Button from '../components/Button';
import NavigationOption from '../common/NavigationOption';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Constants from '../common/Constants';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import CustomAlert from '../common/CustomAlert';
let statusBarHeight = getStatusBarHeight(true);


export default class ForgotPassScreen extends React.Component {


    constructor(props){
        super(props);

        this.state = {isLoading: false}
    }

    componentDidMount() {

        console.log('statusBarHeight',statusBarHeight)

        this.props.navigation.setOptions({
            title: "Нууц үг сэргээх",
        })

        this.props.navigation.setParams({ close: this.close , help: this.help});
    }


    validate = () => {

        let username = this.refs.username.validate();

        if(username){
            this.sendData({
                username: username,
            })
        }
    };

    sendData = (data) => {

        this.setState({isLoading: true});

        fetch(Constants.URL + "redpointapi/user/forgotPassword", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((json) => {

                console.log(json)

                if (json.status != "SUCCESS"){

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');

                    this.setState({isLoading: false});
                } else {

                    this.setState({isLoading: false})

                    CustomAlert.alert(json.data.message);

                    this.props.navigation.navigate('ResetPass', {phone: data.username})


                }

            }).catch((error) => {

            this.setState({isLoading: false});

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');
        });

    };

    render() {

        return (
            <SafeAreaView style = {{flex: 1, backgroundColor: '#fff',}} >
                {/*<StatusBar backgroundColor="white" barStyle={ (Platform.OS === 'ios')? "light-content" : "dark-content"} />*/}
                <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} keyboardVerticalOffset = {(Platform.OS === 'ios') ? 64 + statusBarHeight : 0}>
                    <InvertibleScrollView keyboardDismissMode = "interactive" inverted>
                        <View style={styles.container}>
                            <Input ref = "username" autoFocus image = {require('../image/user.png')} keyboardType = "email-address" label = "Утас эсвэл имэйл" required returnKeyType = { "next" } onSubmitEditing={this.validate} />
                            <Button style = {{marginTop: 16, backgroundColor: '#FAB615'}}  title = "Үргэлжлүүлэх" onPress = {this.validate}  />
                        </View>
                    </InvertibleScrollView>

                </KeyboardAvoidingView>
                <ActivityIndicatorView animating = {this.state.isLoading}/>
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 16,
        paddingTop: 0,
    },

    text: {
        color: '#757F8C',
        fontSize: 12,
        fontWeight: '500',
        textAlign: 'center',
        margin: 16,
    }
});
