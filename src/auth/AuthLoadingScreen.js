import React from 'react';
import {Image, View, StatusBar, StyleSheet, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import moment from 'moment';

export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            status: 'Түр хүлээнэ үү.',
        }
    }

    render() {
        return (
            <SafeAreaView style = {styles.container} >
                <StatusBar barStyle="default"/>
                <View style = {{alignItems: 'center',marginBottom: 48}} >
                    <Image style = {styles.redpoint} source = {require('../image/logo.png')} resizeMode= "contain" />
                    <Text style = {styles.status}>{this.state.status}</Text>
                </View>

                <View style={{marginBottom: 48, alignItems: 'center'}} >
                    <Image style = {styles.mid} source = {require('../image/mid.png')} resizeMode= "contain" />
                    <Text style = {styles.text} >Бүх эрх хуулиар хамгаалагдсан</Text>
                    <Text style = {styles.text} >© MОНГОЛ АЙ ДИ ХХК {moment().format('YYYY')}</Text>
                </View>
            </SafeAreaView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },

    redpoint: {
        marginTop: 120
    },

    mid: {
        marginBottom: 16
    },

    status:{
        width: 240,
        margin: 16,
        color: '#333333',
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        textAlign: 'center',
    },

    text:{
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        lineHeight: 24,
        color: '#333333',
        textAlign: 'center',
    }

});
