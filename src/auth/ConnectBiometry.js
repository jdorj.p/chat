import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    Alert,
    Dimensions, Platform,
} from 'react-native';

import Button from '../components/Button';
import UserUtil from '../common/UserUtil';
import CustomAlert from '../common/CustomAlert';
import Biometrics from 'react-native-biometrics'
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from "@react-native-community/async-storage";
let statusBarHeight = getStatusBarHeight(true);


const {width: screenWidth} = Dimensions.get('window');

export default class ConnectBiometry extends React.Component {


    constructor(props){
        super(props);

        this.state = {
            type: 'Хурууны хээ',
        }

        Biometrics.isSensorAvailable().then((biometryType)=>{

            if (biometryType === Biometrics.FaceID) {
                this.setState({type: 'Face Id'})
            }

        })
    }


    connect = async () => {

        let biometryType = await Biometrics.isSensorAvailable()

        console.log(biometryType)

        if (biometryType === Biometrics.TouchID) {

        } else if (biometryType === Biometrics.FaceID) {

        } else {

            CustomAlert.alert("Уучлаарай.","Таны төхөөрөмж уншихгүй.","error")
            return
        }


        Biometrics.simplePrompt('Холбох').then(() => {

                let username = this.props.route.params.username
                let password =  this.props.route.params.password

                console.log(username,password)

                UserUtil.saveBioCredential(username,password).then(()=>{

                    CustomAlert.alert('Амжилттай','Таны '+this.state.type+' нууц үгтэй холбогдлоо','success')
                    this.navigateToApp()

                }).catch((e)=>{

                    CustomAlert.alert('Амжилтгүй','Таны '+this.state.type+' нууц үгтэй холбож чадсангүй','error')
                    this.navigateToApp()
                })

            })
            .catch((e) => {

                console.log(e)
                console.log(e.code)

                if(e.code == 'User cancelled fingerprint authorization'){
                    return
                }

                CustomAlert.alert(this.state.type + "-г таньсангүй","Та дахин оролдоно уу!","error")
            })

    }

    navigateToApp = async () => {

        let invoiceId = await AsyncStorage.getItem('invoiceId')

        if(invoiceId) {

            AsyncStorage.removeItem("invoiceId")

            this.props.navigation.navigate("Invoice", {
                invoice: {invoiceId: invoiceId},
                onClose: () => {
                    this.navigation.navigate('Home');
                }
            });
        }else{
            this.props.navigation.navigate('App')
        }
    }

    cancel = () => {

        this.props.navigation.navigate('App')
    }

    render() {

        return (
            <SafeAreaView style = {{flex: 1}} >
                        <View style={styles.container} >

                            <View style = {{flex: 1,justifyContent: 'center', alignItems: 'center'}} >

                                {(this.state.type == 'Face Id') ? (<Image source = {require('../image/faceid-large.png')} />) : (<Image source = {require('../image/fingerprint-large.png')} />)}
                                <Text style = {styles.text} >Та нууц үгээ {this.state.type}-тэй холбох уу?</Text>
                            </View>
                            <Button style = {{marginTop: 24}}  title = "Холбох" onPress = {this.connect} />
                            <Button style = {{marginTop: 16,}} titleStyle = {{color: '#000', fontFamily: 'Roboto-Regular', fontSize: 16}}  hideGradient title = "Үгүй" onPress = {this.cancel} />
                        </View>
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 16,
    },

    text: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        margin: 24,
    },

});
