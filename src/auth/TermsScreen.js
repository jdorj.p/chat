import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView, ImageBackground, TouchableHighlight, Platform, WebView} from 'react-native';
import HTML from 'react-native-render-html';
import NavigationOption from "../common/NavigationOption";
import ActivityIndicatorView from "../common/ActivityIndicatorView";

export default class TermsScreen extends React.Component {

    constructor(props){
        super(props);

        this.state = { isLoading: false, content: ""};
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: "Үйлчилгээний нөхцөл",
        })

        this.getData()
    }


    getData() {

        if (this.state.isLoading){
            return;
        }

        let state = this.state;
        state.isLoading = true

        this.setState(state);

        // const url = 'https://api.minu.mn/redpointapi/public/files/redpoint_service_conditions.html'
        const url = 'http://api.minu.mn/wsock/tos.html'

        return fetch(url)
            .then((response) => response.text())
            .then((response) => {

                this.setState({
                    isLoading: false,
                    content: response,
                });

            })
            .catch((error) => {
                console.error(error);
                this.setState({
                    isLoading: false,
                });
            });
    }


    render() {

        return (
            <View style={{flex: 1}}>
                <ScrollView style={styles.scrollView}>
                    <View style = {{backgroundColor: 'white'}}>
                        <HTML html={this.state.content} style = {styles.content} />
                    </View>
                </ScrollView>
                <ActivityIndicatorView animating = {this.state.isLoading} light/>
            </View>

        );
    }
}


const styles = StyleSheet.create({

    scrollView: {
        flex: 1,
        backgroundColor: 'white'
    },

    content:{
        color: '#333333',
        fontSize: 14,
    },

});
