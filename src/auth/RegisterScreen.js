    import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    KeyboardAvoidingView,
    SafeAreaView,
    Dimensions, Platform,
} from 'react-native';

import Input from '../components/Input';
import Button from '../components/Button';
import Constants from '../common/Constants';
import UserUtil from '../common/UserUtil';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import CustomAlert from '../common/CustomAlert';
import { getStatusBarHeight } from 'react-native-status-bar-height';
let statusBarHeight = getStatusBarHeight(true);


const {width: screenWidth} = Dimensions.get('window');

export default class RegisterScreen extends React.Component {


    constructor(props){
        super(props);

        this.state = {
            isLoading: false,
            saveCredentials: false,
            gender : "",
        }

        UserUtil.getCredentials().then((data) => {

            console.log(data)

            if(data.username && data.password){

                this.refs.username.setValue(data.username)
                this.refs.password.setValue(data.password)
                this.setState({ saveCredentials: true})

            }
        })
    }

    componentDidMount(): void {

        this.props.navigation.setOptions({
            title: "Бүртгүүлэх",
        })
    }


    validate = () => {


        let firstName = this.refs.firstName.validate()
        let phone = this.refs.phone.validate()
        let email = this.refs.email.validate() ?? ""
        let password = this.refs.password.validate()

        if(this.state.confirmTerm == undefined){
            this.setState({confirmTerm: false})
        }



        if(firstName && phone && password && this.state.confirmTerm){

            this.sendData({
                firstName: firstName,
                phone: phone,
                email: email,
                gender: this.state.gender,
                password: password,
                systemName: "chat", // minu chat
                device: Platform.OS,
            })
        }
    }

    sendData = (data) => {

        this.setState({isLoading: true});

        fetch(Constants.WSURL + "wsock/invoice", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {

                console.log(json)

                if(json.status == "SUCCESS") {

                    this.props.navigation.navigate('ConfirmPhone',{userId: json.data.userId, phone: data.phone})

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.message, 'error');
                }

                this.setState({isLoading: false})

            }).catch((error) => {

                console.log('register error',error)

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({isLoading: false});
        });

    }

    register = () => {
        this.props.navigation.navigate('Register')
    };

    forgotPass = () => {
        this.props.navigation.navigate('ForgotPass');
    };


    render() {

        return (
            <SafeAreaView style = {{flex: 1, backgroundColor: "#fff"}} >
                <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} keyboardVerticalOffset = {(Platform.OS === 'ios')? 64 + statusBarHeight : 0} >
                    <InvertibleScrollView keyboardDismissMode = "interactive" inverted keyboardShouldPersistTaps = 'handled' >
                        <View style={styles.container} >

                            <Input ref = "firstName" image = {require('../image/user.png')}  label = "Нэр" required returnKeyType = { "next" } onSubmitEditing={() => { this.refs.phone.focus() }} />
                            <Input ref = "phone" image = {require('../image/phone.png')} label = "Утас" keyboardType = "number-pad" required pattern = {/^\d{8}$/} error = "8 оронтой дугаар оруулна уу!" returnKeyType = { "next" } onSubmitEditing={() => { this.refs.email.focus() }} />

                            <Text style = {styles.text} >Имэйл-тэй бол имэйлээ оруулна уу. Жишээ нь: bataa@gmail.com</Text>
                            <Input ref = "email" image = {require('../image/email.png')} label = "Имэйл" keyboardType = "email-address" pattern = {/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/} error = "Зөв имэйл оруулна уу!" returnKeyType = { "next" } onSubmitEditing={() => { this.refs.password.focus() }} />

                            <View style = {{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}} >
                                <Button image = {this.state.gender == "male" ? require('../image/checked.png') : require('../image/check.png')} onPress = {()=>{ this.setState({gender: "male"})  }} style = {{marginLeft: 3 ,marginRight: 16}} title = "Эрэгтэй" titleStyle = {{color: '#000', marginLeft: 20, fontFamily: 'Roboto-Regular'}} hideGradient />
                                <Button image = {this.state.gender == "female" ? require('../image/checked.png') : require('../image/check.png')} onPress = {()=>{ this.setState({gender: "female"})  }} style = {{marginLeft: 16}} title = "Эмэгтэй" titleStyle = {{color: '#000', marginLeft: 20, fontFamily: 'Roboto-Regular'}} hideGradient />
                            </View>
                            <View style = {styles.border} />

                            <Input ref = "password" image = {require('../image/password.png')} label = "Нууц үг" required pattern = {/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.{6,})/} error = "Багадаа нэг үсэг болон тоо орсон, 6 болон түүнээс дээш урттай нууц үг оруулна уу!" returnKeyType = { "done" } secureTextEntry = {true} onSubmitEditing={this.validate} />


                            <View style = {{flexDirection: 'row', marginTop: 16, alignItems: 'center'}} >
                                <Button image = {(this.state.confirmTerm == true) ? require('../image/checked.png') : require('../image/check.png')} onPress = {()=>{ this.setState({confirmTerm: !(this.state.confirmTerm)})  }} style = {{marginLeft: 3, marginRight: 16}} hideGradient />
                                <Button title = "Үйчилгээний нөхцөлийг зөвшөөрч байна" onPress = {()=>{ this.props.navigation.navigate('Terms') }} hideGradient titleStyle = {{color: '#000', fontFamily: 'Roboto-Regular', textDecorationLine: 'underline'}} />
                            </View>
                            { (this.state.confirmTerm == false) ? (<Text style = {styles.error} >Үйлчилгээний нөхцөлийг зөвшөөрнө үү!</Text>) : null }

                            <View style = {{flexDirection: 'row', marginTop: 16,}} >
                                <Button style = {{flex: 1}}  title = "Бүртгүүлэх" onPress = {this.validate} />
                            </View>

                        </View>
                    </InvertibleScrollView>
                </KeyboardAvoidingView>
                <ActivityIndicatorView animating = {this.state.isLoading} title = {"Түр хүлээнэ үү"} />
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 16,
        backgroundColor: "#fff",
    },

    logo: {
        margin: 16,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        color: '#000',
        fontSize: 14,
        marginTop: 16,
        marginBottom: 8,
    },

    border: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#E0E0E0'
    },

    error:{
        color: '#EB5757',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },
});
