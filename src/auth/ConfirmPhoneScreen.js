import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    StatusBar,
    Platform,
} from 'react-native';
import Input from '../components/Input';
import Button from '../components/Button';
import NavigationOption from '../common/NavigationOption';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Constants from '../common/Constants';
import { getStatusBarHeight } from 'react-native-status-bar-height';
let statusBarHeight = getStatusBarHeight(true);
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import CustomAlert from '../common/CustomAlert';


export default class ConfirmPhoneScreen extends React.Component {


    constructor(props){
        super(props);

        this.state = {
            isLoading: false,
            count: 0,
        }
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: "Баталгаажуулах",
        })

        let phone = this.props.route.params.phone
        this.refs.phone.setValue(phone);

        this.getCode()
    }

    startCount = () => {

        this.setState({count: 60},()=>{
            this.interval = setInterval(this.count, 1000)
        })
    }

    count = () => {
        if(this.state.count == 0){
            clearInterval(this.interval);
            return
        }
        this.setState({
            count: this.state.count - 1
        });
    };

    getCode = () => {

        if(this.state.count > 0) {
            CustomAlert.alert(this.state.count + " секундын дараа дахин код авна уу!");
            return
        }

        this.setState({isLoading: true});

        fetch(Constants.URL + "redpointapi/user/confirmation", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "userId": this.props.route.params.userId
            })
        })
            .then((response) => response.json())
            .then((json) => {

                console.log(json)

                if (json.status != "SUCCESS"){

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                    this.setState({isLoading: false});
                } else {
                    this.setState({isLoading: false});
                    this.startCount()
                }

            }).catch((error) => {

            this.setState({isLoading: false});
        });

    };


    validate = () => {

        let confirmCode = this.refs.code.validate();

        if(confirmCode){

            this.sendData({
                "userId": this.props.route.params.userId,
                "code": confirmCode,
            })
        }
    };



    sendData = (data) => {

        this.setState({isLoading: true});

        fetch(Constants.URL + "redpointapi/user/confirmation", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((json) => {

                console.log(json)

                if (json.status != "SUCCESS"){

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                    this.setState({isLoading: false});
                } else {

                    this.setState({isLoading: false});
                    CustomAlert.alert("Амжилттай!","Таны утас баталгаажлаа, та нэвтэрнэ үү.", 'success');
                    this.props.navigation.navigate('Login')

                }

            }).catch((error) => {

            this.setState({isLoading: false});
        });

    };


    render() {

        return (
            <SafeAreaView style = {{flex: 1}} >
                <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} keyboardVerticalOffset = {(Platform.OS === 'ios')? 64 + statusBarHeight : 0} >
                    <InvertibleScrollView keyboardDismissMode = "interactive" inverted>
                        <View style={styles.container} >

                            <Input ref = "phone" image = {require('../image/phone.png')} label = "Утас"  editable = {false} returnKeyType = { "next" } keyboardType = "number-pad" onSubmitEditing={() => { this.refs.code.focus() }} />

                            <Text style = {styles.text} >Таны утсанд мессежээр ирсэн 6 оронтой кодыг оруулна уу!</Text>
                            <Input ref = "code" image = {require('../image/password-lined.png')} label = "Код"  required returnKeyType = { "next" } keyboardType = "number-pad" onSubmitEditing={() => { this.refs.password.focus() }} />

                            <Button style = {{marginTop: 32, backgroundColor: '#FAB615'}}  title = "Баталгаажуулах" onPress = {this.validate} />

                            <Text style = {styles.text} >Код ирэхгүй бол утасны дугаараа шалгаад дахин код авах дээр дарна уу!</Text>
                            <Button title = {this.state.count == 0 ? "Дахин код авах" : String(this.state.count)} hideGradient second onPress = {this.getCode} />

                        </View>
                    </InvertibleScrollView>

                </KeyboardAvoidingView>
                <ActivityIndicatorView animating = {this.state.isLoading}/>
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 16,
        paddingTop: 0,
    },

    pagerContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        alignItems: 'center',
    },

    text: {
        fontFamily: 'Roboto-Regular',
        color: '#000',
        fontSize: 14,
        marginTop: 24,
        marginBottom: 16,
    }
});
