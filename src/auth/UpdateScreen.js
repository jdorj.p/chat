import React from 'react';
import {Image, View, StatusBar, StyleSheet, Text} from 'react-native';
import * as Progress from 'react-native-progress';
import CodePush from 'react-native-code-push';
import {SafeAreaView} from 'react-native-safe-area-context';
import moment from 'moment';

export default class UpdateScreen extends React.Component {
    constructor(props) {
        super(props);

        this.checkUpdate()

        this.state = {
            status: 'Түр хүлээнэ үү.',
        }
    }

    checkUpdate = () => {

        let options = {
            installMode: CodePush.InstallMode.IMMEDIATE,
            checkFrequency: CodePush.CheckFrequency.MANUAL,
        }

        CodePush.sync(
            options,
            (status)=>{

                let statusDescr

                if(status == 5){
                    statusDescr = 'Update шалгаж байна.'
                }else if(status == 7){
                    statusDescr = 'Update татаж байна.'
                }else if(status == 8){
                    statusDescr = 'Update суулгаж байна.'
                }

                this.setState({status: statusDescr})
            },
            ((downloadProgress) => {

                if (downloadProgress) {

                    let progress = downloadProgress.receivedBytes / downloadProgress.totalBytes
                    this.setState({progress: progress})
                }

            }),
            ((remotePackage)=>{
            })
        ).then((status)=>{

            console.log('sync status',status)

            if(status == 0){
                this.props.stopSyncUpdate && this.props.stopSyncUpdate()
            }


        }).catch((e)=>{

            console.log('sync error',e)
            this.props.stopSyncUpdate && this.props.stopSyncUpdate()
        })
    }

    render() {
        return (
            <SafeAreaView style = {styles.container} >
                <StatusBar barStyle="default"/>
                <View style = {{alignItems: 'center',marginBottom: 48}} >
                    <Image style = {styles.redpoint} source = {require('../image/logo.png')} resizeMode= "contain" />
                    <Text style = {styles.status}>{this.state.status}</Text>
                    {(this.state.progress) ? (<Progress.Bar  progress={this.state.progress} width={240} color = "#333333" />) : null}
                </View>

                <View style={{marginBottom: 48, alignItems: 'center'}} >
                    <Image style = {styles.mid} source = {require('../image/mid.png')} resizeMode= "contain" />
                    <Text style = {styles.text} >Бүх эрх хуулиар хамгаалагдсан</Text>
                    <Text style = {styles.text} >© MОНГОЛ АЙ ДИ ХХК {moment().format('YYYY')}</Text>
                </View>
            </SafeAreaView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },

    redpoint: {
        marginTop: 120
    },

    mid: {
        marginBottom: 16
    },

    status:{
        width: 240,
        margin: 16,
        color: '#333333',
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        textAlign: 'center',
    },

    text:{
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        lineHeight: 24,
        color: '#333333',
        textAlign: 'center',
    }

});
