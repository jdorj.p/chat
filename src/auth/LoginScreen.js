import React, {useContext} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    Alert,
    Dimensions, Platform,
} from 'react-native';

import Input from '../components/Input';
import Button from '../components/Button';
import Constants from '../common/Constants';
import UserUtil from '../common/UserUtil';
import VersionView from '../components/VersionView';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import NavigationOption from '../common/NavigationOption';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import CustomAlert from '../common/CustomAlert';
// import Biometrics from 'react-native-biometrics'
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from "@react-native-community/async-storage";
import {AuthContext} from '../common/Context';
let statusBarHeight = getStatusBarHeight(true);


const {width: screenWidth} = Dimensions.get('window');

export default class LoginScreen extends React.Component {


    constructor(props){
        super(props);

        this.state = {
            isLoading: false,
            saveCredentials: false
        }

        UserUtil.getCredentials().then((data) => {

            console.log(data)

            if(data.username && data.password){

                this.refs.username.setValue(data.username)
                this.refs.password.setValue(data.password)
                this.setState({ saveCredentials: true})
            }
        })
    }

    componentDidMount(): void {

        this.props.navigation.setOptions({
            title: "Нэвтрэх",
        })

        // Biometrics.isSensorAvailable().then((biometryType)=>{
        //
        //     this.setState({biometryType: biometryType})
        // })
    }

    validate = () => {

        let username = this.refs.username.validate()
        let password = this.refs.password.validate()

        if(this.state.confirmTerm == undefined){
            this.setState({confirmTerm: false})
        }

        if(username && password && this.state.confirmTerm){

            this.sendData({
                username: username,
                password: password,
                systemName: "chat",
                device: UserUtil.getDeviceInfo(),
            })
        }
    }

    sendData = (data) => {

        this.setState({isLoading: true});

        fetch(Constants.URL + "redpointapi/user/login", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((json) => {

                console.log(json)

                if(json.status == "SUCCESS") {

                    if(json.data && json.data.userConfirmation == false) {

                        this.confirm(json.data)
                    }else{

                        this.login(json.token, data)
                    }

                }else{

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                }

                this.setState({isLoading: false})

            }).catch((error) => {

                console.log(error.message)

                CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

                this.setState({isLoading: false});
        });

    }


    login = (token,credentials) => {

        AsyncStorage.setItem('token',token).then(() => {

            UserUtil.saveDeviceId()
            UserUtil.updateUserData().then(()=>this.navigateToApp())

            // UserUtil.getBioCredentials().then((data) => {
            //
            //     if (data.busername == null || data.bpassword == null) {
            //
            //         Biometrics.isSensorAvailable().then((biometryType)=>{
            //
            //             if (biometryType === Biometrics.FaceID || biometryType === Biometrics.TouchID) {
            //                 this.props.navigation.navigate('ConnectBiometry',{username: credentials.username,password: credentials.password})
            //             }else{
            //
            //                 this.navigateToApp()
            //             }
            //
            //         })
            //
            //
            //     }else{
            //
            //         this.navigateToApp()
            //     }
            //
            // }).catch((e)=>{
            //
            //     console.log(e)
            //     this.navigateToApp()
            // })


            if (this.state.saveCredentials) {
                UserUtil.saveCredential(credentials.username, credentials.password)
            }

        }).catch((error) => {

            console.log(error)
            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');
        })

    }


    confirm = (data) => {

        this.props.navigation.navigate('ConfirmPhone',{userId: data.userId, phone: data.userPhone})
    }

    register = () => {
        this.props.navigation.navigate('Register')
    };

    forgotPass = () => {
        this.props.navigation.navigate('ForgotPass');
    };

    static contextType = AuthContext

    navigateToApp = () => {

        let isLogged = this.context.isLogged
        isLogged()

    }

    // signWithbiometric = async () => {
    //
    //    let type = undefined
    //
    //    let biometryType = await Biometrics.isSensorAvailable()
    //
    //     console.log(biometryType)
    //
    //     if (biometryType === Biometrics.TouchID) {
    //         type = "Хурууны хээ"
    //     } else if (biometryType === Biometrics.FaceID) {
    //         type = "Face ID"
    //     } else {
    //
    //         CustomAlert.alert("Уучлаарай.","Таны төхөөрөмж хурууны хээ уншихгүй.","error")
    //         return
    //     }
    //
    //
    //     UserUtil.getBioCredentials().then((data) => {
    //
    //         if(data.busername == null || data.bpassword == null){
    //
    //             CustomAlert.alert(type + " холбоогүй байна.","Нууц үгээрээ нэвтэрнэ үү!","error")
    //             return
    //         }
    //
    //         Biometrics.simplePrompt('Нэвтрэх')
    //             .then(() => {
    //
    //                 this.refs.username.setValue(data.busername)
    //                 this.refs.password.setValue(data.bpassword)
    //
    //                 this.validate()
    //
    //             })
    //             .catch((e) => {
    //
    //                 console.log(e)
    //                 console.log(e.code)
    //
    //                 if(e.code == 'User cancelled fingerprint authorization'){
    //                     return
    //                 }
    //
    //                 CustomAlert.alert(type + "-г таньсангүй","Та дахин оролдоно уу!","error")
    //             })
    //
    //
    //     }).catch(()=>{
    //
    //         CustomAlert.alert(type + " холбоогүй байна.","Нууц үгээрээ нэвтэрнэ үү!","error")
    //     })
    //
    // }

    render() {

        return (
                <SafeAreaView style = {{flex: 1,backgroundColor: '#fff'}} >
                    <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} keyboardVerticalOffset = {(Platform.OS === 'ios')? 64 + statusBarHeight : 0} >
                    {/*<KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null}  >*/}
                        <InvertibleScrollView keyboardDismissMode = "interactive" inverted >
                            <View style={styles.container} >
                                <View style = {{alignItems: 'center',justifyContent: 'center', marginBottom: 16 }} >
                                    {/*<Image style={styles.logo} source = {require('../image/logo.png')} />*/}
                                    <Image style={styles.logo} source = {require('../image/redpoint_logo_tom.png')} />
                                </View>

                                <Text style = {styles.text} >RedPoint-д бүртгэлтэй бол шууд нэвтэрнэ үү.</Text>

                                <Input ref = "username" image = {require('../image/user.png')} label = "Утас эсвэл имэйл" keyboardType = "email-address" required returnKeyType = { "next" } onSubmitEditing={() => { this.refs.password.focus() }} />
                                <Input ref = "password" image = {require('../image/password.png')} label = "Нууц үг" required returnKeyType = { "done" } secureTextEntry hideSecureText onSubmitEditing={this.validate} />

                                <View style = {{flexDirection: 'row', justifyContent: 'space-between', marginTop: 16, alignItems: 'center'}} >
                                    <Button image = {this.state.saveCredentials ? require('../image/checked.png') : require('../image/check.png')} onPress = {()=>{ this.setState({saveCredentials: !this.state.saveCredentials})  }} style = {{marginLeft: 3}} title = "Cануулах" titleStyle = {{color: '#000', marginLeft: 20, fontFamily: 'Roboto-Regular'}} hideGradient />
                                    <Button title = "Нууц үг мартсан" onPress = {this.forgotPass} hideGradient titleStyle = {{color: '#000', fontFamily: 'Roboto-Regular', textDecorationLine: 'underline'}} />
                                </View>

                                <View style = {{flexDirection: 'row', marginTop: 16, alignItems: 'center'}} >
                                    <Button image = {(this.state.confirmTerm == true) ? require('../image/checked.png') : require('../image/check.png')} onPress = {()=>{ this.setState({confirmTerm: !(this.state.confirmTerm)})  }} style = {{marginLeft: 3, marginRight: 16}} hideGradient />
                                    <Button title = "Үйчилгээний нөхцөлийг зөвшөөрч байна" onPress = {()=>{ this.props.navigation.navigate('Terms') }} hideGradient titleStyle = {{color: '#000', fontFamily: 'Roboto-Regular', textDecorationLine: 'underline'}} />
                                </View>
                                { (this.state.confirmTerm == false) ? (<Text style = {styles.error} >Үйлчилгээний нөхцөлийг зөвшөөрнө үү!</Text>) : null }

                                <View style = {{flexDirection: 'row', marginTop: 16,}} >
                                    <Button style = {{flex: 1}}  title = "Нэвтрэх" onPress = {this.validate} />
                                </View>


                                <Button style = {{marginTop: 16,}} titleStyle = {{color: '#000', fontFamily: 'Roboto-Regular', fontSize: 16}}  hideGradient title = "Бүртгүүлэх" onPress = {this.register} />

                            </View>
                        </InvertibleScrollView>
                    </KeyboardAvoidingView>
                    <ActivityIndicatorView  animating = {this.state.isLoading} title = {"Түр хүлээнэ үү"}  />
                </SafeAreaView>
    );
    }

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 16,
    },

    logo: {
        margin: 16,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 18,
    },

    error:{
        color: '#EB5757',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },

});
