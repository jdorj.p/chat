import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    StatusBar,
    Platform,
} from 'react-native';
import Input from '../components/Input';
import Button from '../components/Button';
import NavigationOption from '../common/NavigationOption';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Constants from '../common/Constants';
import UserUtil from '../common/UserUtil';
import { getStatusBarHeight } from 'react-native-status-bar-height';
let statusBarHeight = getStatusBarHeight(true);
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import CustomAlert from '../common/CustomAlert';


export default class ResetPassScreen extends React.Component {


    constructor(props){
        super(props);


        this.state = {
            isLoading: false,
            count: 60,
            secure:  true,
            vision: 'visibility',
        }
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: "Нууц үг сэргээх",
        })

        this.refs.phone.select({text: this.props.route.params.phone, value: this.props.route.params.phone});
        this.startCount()
    }

    startCount = () => {

        this.setState({count: 60})
        this.interval = setInterval(this.count, 1000)
    }

    count = () => {
        if(this.state.count == 0){
            clearInterval(this.interval);
            return
        }
        this.setState({
            count: this.state.count - 1
        });
    };


    validate = () => {

        let confirmCode = this.refs.code.validate();
        let newPassword = this.refs.password.validate();

        if(confirmCode && newPassword){

            this.sendData({
                username: this.props.route.params.phone,
                token: confirmCode,
                password: newPassword,
                device: Platform.OS,
            })
        }
    };

    getCode = () => {

        let phone = this.refs.phone.validate();

        if (!phone){
            return
        }

        if(this.state.count > 0) {
            CustomAlert.alert(this.state.count + " секундын дараа дахин код авна уу!");
            return
        }

        this.setState({isLoading: true});

        fetch(Constants.URL + "redpointapi/user/forgotPassword", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "username": phone,
            })
        })
            .then((response) => response.json())
            .then((json) => {

                console.log(json)

                if (json.status != "SUCCESS"){

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                    this.setState({isLoading: false});
                } else {
                    this.setState({isLoading: false});
                    this.startCount()
                }

            }).catch((error) => {

            this.setState({isLoading: false});
        });

    };


    sendData = (data) => {

        this.setState({isLoading: true});

        fetch(Constants.URL + "redpointapi/user/resetPassword", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((json) => {

                console.log(json)

                if (json.status != "SUCCESS"){

                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                    this.setState({isLoading: false});
                } else {

                    this.setState({isLoading: false});
                    CustomAlert.alert("Амжилттай!","Таны нууц үг солигдлоо.", 'success');
                    this.props.navigation.navigate('Login')

                }

            }).catch((error) => {

            this.setState({isLoading: false});
        });

    };


    changeVisible = () => {
        if (this.state.secure){
            this.setState({
                secure: false,
                vision: 'visibility-off',
            })
        } else {
            this.setState({
                secure: true,
                vision: 'visibility',
            })
        }
    };


    render() {

        return (
            <SafeAreaView style = {{flex: 1}} >
                {/*<StatusBar backgroundColor="white" barStyle={ (Platform.OS === 'ios')? "light-content" : "dark-content"} />*/}
                <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} keyboardVerticalOffset = {(Platform.OS === 'ios')? 88 + statusBarHeight : 0} >
                    <InvertibleScrollView keyboardDismissMode = "interactive" inverted>
                        <View style={styles.container} >

                            <Input ref = "phone" image = {require('../image/user.png')} keyboardType = "email-address" label = "Утас эсвэл имэйл" required returnKeyType = { "next" } onSubmitEditing={() => { this.refs.code.focus() }} />

                            <Text style = {styles.text} >Таны утсанд мессежээр ирсэн 6 оронтой кодыг оруулна уу!</Text>
                            <Input ref = "code" image = {require('../image/password-lined.png')} label = "Код"  required returnKeyType = { "next" } keyboardType = "number-pad" onSubmitEditing={() => { this.refs.password.focus() }} />

                            <Input ref = "password" image = {require('../image/password-lined.png')} label = "Шинэ нууц үг" required pattern = {/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.{6,})/} error = "Багадаа нэг үсэг болон тоо орсон, 6 болон түүнээс дээш урттай нууц үг оруулна уу!" returnKeyType = { "done" } secureTextEntry = {this.state.secure} vision={this.state.vision} onPress={this.changeVisible} onSubmitEditing={this.validate}/>

                            <Button style = {{marginTop: 32, backgroundColor: '#FAB615'}}  title = "Сэргээх" onPress = {this.validate} />


                            <Text style = {styles.text} >Код ирэхгүй бол утасны дугаараа шалгаад дахин код авах дээр дарна уу!</Text>
                            <Button title = {this.state.count == 0 ? "Дахин код авах" : String(this.state.count)} hideGradient second onPress = {this.getCode} />

                        </View>
                    </InvertibleScrollView>

                </KeyboardAvoidingView>
                <ActivityIndicatorView animating = {this.state.isLoading}/>
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 16,
        paddingTop: 0,
    },

    pagerContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        alignItems: 'center',
    },

    text: {
        fontFamily: 'Roboto-Regular',
        color: '#000',
        fontSize: 14,
        marginTop: 24,
        marginBottom: 16,
    }
});
