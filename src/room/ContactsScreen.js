import React from 'react';
import {
    ActivityIndicator,
    FlatList,
    Image,
    KeyboardAvoidingView,
    PermissionsAndroid,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    ImageBackground,
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Contacts from 'react-native-contacts';
import { SwipeRow } from 'react-native-swipe-list-view';
import AsyncStorage from '@react-native-community/async-storage';

import colors from '../shared/color';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import { extractErrorMessage } from '../common/apiHelpers';
import PermissionDeniedError from '../shared/errors/PermissionDeniedError';
import {ProfileImage} from '../common/ProfileImage';
import CheckedView from '../components/CheckedView';
import {RoomListItem} from './RoomListItem';
import SelectedContact from './SelectedContact';
import Button from '../components/Button';
import {call} from 'react-native-reanimated';
import ContactAdapter from '../data/ContactAdapter';
import SendSMS from 'react-native-sms'

let statusBarHeight = getStatusBarHeight(true);

class ContactsScreen extends React.Component {

    constructor(props) {
        super(props);

        this.contactAdapter = new ContactAdapter()

        this.state = {
            isLoading: false,
            contacts: [],
            keyword: '',
            searchPhone: '',
            searchInProgress: false,
            searchResult: undefined,
            selectedUsers: [],
            dataVersion: 0,
            exceptUsers: this.props.route.params ? this.props.route.params.exceptUsers : undefined
        };

        this.rowRefs = new Map();
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: (this.props.route.params ? this.props.route.params.title : 'Чат бичих') ?? 'Чат бичих',
        })

        this.initData()
    }

    initData = async () => {

        let contacts = await this.contactAdapter.getData()
        this.filter(contacts)

        this.setState({
            isLoading: contacts.length == 0,
        });

        this.sendContacts().finally(() => {

            this.fetchContacts().finally(() => {

                this.setState({ isLoading: false });
            });
        });
    }

    sendContacts = async () => {

        console.log('sendContacts')
        const doSendContacts = () => {
            return new Promise((resolve, reject) => {

                Contacts.getAllWithoutPhotos(async (err, contacts) => {
                    try {
                        if (err === 'denied') {
                            reject(new PermissionDeniedError('Утасны контакт уншихад татгалзсан хариу ирсэн.'));
                            return;
                        }
                        // console.log('contacts', contacts)
                        const phoneNumbersToAdd = [];
                        for (let contact of contacts) {
                            if (contact.phoneNumbers) {
                                for (let phoneNumber of contact.phoneNumbers) {
                                    const sanitizedPhoneNumber = phoneNumber.number.replace(/[^0-9]+/g, '');
                                    if (sanitizedPhoneNumber.length === 8) {
                                        phoneNumbersToAdd.push(sanitizedPhoneNumber);
                                    }
                                }
                            }
                        }

                        const response = await fetch(Constants.URL + 'redpointapi/user/uploadContacts', {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': await AsyncStorage.getItem('token'),
                            },
                            body: JSON.stringify(phoneNumbersToAdd),
                        });
                        const responseBody = await response.json();

                        if (responseBody.status === 'SUCCESS') {
                            console.log('SUCCESS')


                            resolve();
                        } else {
                            reject(new Error(extractErrorMessage(responseBody)));
                        }
                    } catch (error) {
                        reject(error);
                    }
                });
            });
        };

        if (Platform.OS === 'android') {
            console.log('android')
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                // {
                //     title: 'Утасны контактууд',
                //     message: 'Та утасны контактуудаа RedPoint апп-д оруулмаар байна уу?',
                //     buttonPositive: 'Тийм',
                //     buttonNegative: 'Үгүй',
                //     buttonNeutral: 'Дараа асууна уу',
                // },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return await doSendContacts();
            } else {
                console.log('deny')
                throw new PermissionDeniedError('Утасны контакт уншихыг хэрэглэгч зөвшөөрөөгүй.');
            }
        } else {
            return await doSendContacts();
        }
    };

    fetchContacts = async () => {
        try {
            const response = await fetch(Constants.URL + 'redpointapi/user/userContacts', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': await AsyncStorage.getItem('token'),
                },
            });
            const responseBody = await response.json();

            console.log('fetched contacts',responseBody)

            if (responseBody.status === 'SUCCESS') {

                let contacts = await this.contactAdapter.save(responseBody.data)
                this.filter(contacts)
            } else {
                CustomAlert.alert('Алдаа гарлаа!', responseBody.errors, 'error');
            }
        } catch (error) {
            CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');
        }
    };

    filter = (contacts) => {


        if(this.state.exceptUsers){

            let filtered = contacts.filter((contact) => {

                for(let user of this.state.exceptUsers){

                    if(user == contact.userId){
                        return false
                    }
                }

                return true

            })
            this.setState({
                contacts: filtered,
            });

        }else{

            this.setState({
                contacts: contacts,
            });
        }

    }

    addFriend = async contact => {
        try {
            const response = await fetch(Constants.URL + 'redpointapi/user/contact', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': await AsyncStorage.getItem('token'),
                },
                body: JSON.stringify({
                    userId: contact.userId,
                    contactName: contact.contactName,
                }),
            });
            const responseBody = await response.json();

            if (responseBody.status === 'SUCCESS') {
                if (!this.state.contacts.find(item => item.userId === contact.userId)) {
                    this.setState({ contacts: [...this.state.contacts, contact] });
                }
            } else {
                CustomAlert.alert('Алдаа гарлаа!', responseBody.errors, 'error');
            }
        } catch (error) {
            CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');
        }
    };

    deleteFriend = async userId => {
        try {
            const response = await fetch(Constants.URL + 'redpointapi/user/deleteContact', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': await AsyncStorage.getItem('token'),
                },
                body: JSON.stringify({
                    userId,
                }),
            });
            const responseBody = await response.json();

            if (responseBody.status === 'SUCCESS') {
                this.setState({ contacts: this.state.contacts.filter(contact => contact.userId !== userId) });
            } else {
                CustomAlert.alert('Алдаа гарлаа!', responseBody.errors, 'error');
            }
        } catch (error) {
            CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');
        }
    };


    getSelectedIndex = (user) => {

        let selectedUsers = this.state.selectedUsers

        for(let i = 0; i < selectedUsers.length; i++){
            if(selectedUsers[i].userId == user.userId){
                return i
            }
        }

        return -1
    }

    isSelected = (user) => {
        return this.getSelectedIndex(user) > -1
    }

    selectUser = (user) => {

         console.log('user',user)

        let selectedUsers = this.state.selectedUsers

        let index = this.getSelectedIndex(user)

        if(index > -1){
            selectedUsers.splice(index, 1)
        }else{
            selectedUsers.unshift(user)
        }

        this.setState({
            selectedUsers: selectedUsers,
            dataVersion: this.state.dataVersion + 1
        })
    }

    startChat = () => {

        let callback = (this.props.route.params) ? this.props.route.params.callback : undefined

        if(callback){

            callback(this.state.selectedUsers)
            this.props.navigation.goBack()

        }else{

            this.props.navigation.navigate('Chat', {users: this.state.selectedUsers})
        }
    }

    shouldInvite = () => {

        return this.state.searchPhone.length == 8 && !this.state.searchInProgress && !this.state.searchResult
    }

    invite = (phone = this.state.searchPhone) => {

        SendSMS.send({
            body: 'Сайн уу, хэлцэл хийдэг Minu chat апп-г татаарай http://chat.minu.mn/d',
            recipients: [phone],
            successTypes: ['sent', 'queued'],
            allowAndroidSendWithoutReadPermission: true,
        }, (completed, cancelled, error) => {

            console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);

        });

    }




    renderContact = ({ item, isSearchResult }) => {
        const isFriend = isSearchResult
            ? this.state.contacts.find(contact => contact.userId === item.userId) != null
            : true;


        // let imageSource = item.imageUrl ? { uri: item.imageUrl } : null // require('../image/images/img_0.png')/**/

        return (
            <SwipeRow
                ref={ref => this.rowRefs.set(item.userId, ref)}
                rightOpenValue={-64}
                style={{ height: 67 }}
            >
                <TouchableOpacity
                    onPress={() => {
                        if (isFriend) {
                            this.deleteFriend(item.userId);
                        } else {
                            this.addFriend(item);
                        }
                        if (this.rowRefs.has(item.userId)) {
                            this.rowRefs.get(item.userId).closeRow();
                        }
                    }}
                    style={{
                        position: 'absolute',
                        top: 0,
                        right: 0,
                        bottom: 0,
                        width: 64,
                    }}>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: isFriend ? '#fa7268' : '#28a745',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <Text
                            style={{
                                fontFamily: 'Roboto-Regular',
                                color: '#fff',
                                fontSize: 14,
                            }}>
                            {isFriend ? 'Устгах' : 'Нэмэх'}
                        </Text>
                    </View>
                </TouchableOpacity>
                <View
                    style={{
                        backgroundColor: colors.bgGray,
                        paddingTop: 8,
                        paddingBottom: 8,
                        paddingHorizontal: 16,
                        height: 72,
                    }}
                >
                    <TouchableOpacity
                        style={{ flex: 1, flexDirection: 'row' }}
                        // onPress={() => this.props.navigation.navigate('SendPointToContact', {contact: item})}
                        onPress={() => this.selectUser(item)}

                    >
                        {/*<View>*/}
                        {/*    <Image*/}
                        {/*        source={imageSource}*/}
                        {/*        style={{*/}
                        {/*            width: 52,*/}
                        {/*            height: 52,*/}
                        {/*            resizeMode: 'cover',*/}
                        {/*            borderRadius: 26,*/}
                        {/*            marginRight: 15,*/}

                        {/*        }}*/}
                        {/*    />*/}
                        {/*</View>*/}

                        <ProfileImage url = {item.imageUrl} >
                            {(this.isSelected(item)) ? (<CheckedView/>) : null}
                            {/*<CheckedView/>*/}
                        </ProfileImage>

                        <View style={{ flex: 1 , marginLeft: 8, flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View>
                                {item.contactName ?
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            lineHeight: 24,
                                            fontFamily: 'RobotoCondensed-Regular',
                                            color: '#000',
                                            fontSize: 16,
                                        }}>
                                        {item.contactName}
                                    </Text>
                                    :
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontFamily: 'RobotoCondensed-Regular',
                                            fontSize: 16,
                                            lineHeight: 24,
                                            color: '#e3e3e3',
                                        }}>Хоосон</Text>
                                }
                                <Text
                                    style={{
                                        fontFamily: 'Roboto-Regular',
                                        fontSize: 12,
                                        lineHeight: 18,
                                        color: colors.mutedText,
                                    }}>
                                    {item.phone}
                                </Text>
                            </View>
                            {item.systemName != "chat" ? <Button title = "Урих" style = {styles.inviteButton}  hideGradient onPress = {()=>this.invite(item.phone)} /> : null}
                        </View>
                    </TouchableOpacity>
                </View>
            </SwipeRow>
        );
    };

    search = async keyword => {
        try {
            const phone = keyword.replace(/[^0-9]/, '');
            if (phone === this.state.searchPhone) {
                return;
            }

            if (this.searchAbortController) {
                this.searchAbortController.abort();
            }

            if (phone.length !== 8) {
                this.setState({
                    searchPhone: phone,
                    searchInProgress: false,
                    searchResult: undefined,
                });
                return;
            }

            this.setState({
                searchPhone: phone,
                searchInProgress: true,
                searchResult: undefined,
            });

            this.searchAbortController = new AbortController();

            const response = await fetch(Constants.URL + 'redpointapi/user/findByPhone', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': await AsyncStorage.getItem('token'),
                },
                body: JSON.stringify({
                    phone,
                }),
                signal: this.searchAbortController.signal,
            });

            const responseBody = await response.json();

            if (responseBody.status === 'SUCCESS' ||
                (Array.isArray(responseBody.errors) && responseBody.errors[0].code === 2030)) {
                this.setState({
                    searchInProgress: false,
                });

                this.filterSearchResult(responseBody.data)

            } else {
                this.setState({
                    searchInProgress: false,
                    searchResult: undefined,
                });

                console.log('find phone error',responseBody.errors)

                CustomAlert.alert('Алдаа гарлаа!', responseBody.errors, 'error');
            }
        } catch (error) {

            if (error.name !== 'AbortError') {

                CustomAlert.alert('Алдаа гарлаа!', error.message, 'error');

                this.setState({
                    searchInProgress: false,
                    searchResult: undefined,
                });
            }
        }
    };

    filterSearchResult = (searchResult) => {

        if(searchResult && this.state.exceptUsers){

            console.log('exceptUsers',this.state.exceptUsers)

            for(let user of this.state.exceptUsers){

                if(user == searchResult.userId){

                    this.setState({
                        searchResult: undefined,
                    });
                    return
                }
            }

            this.setState({
                searchResult: searchResult,
            });

        }else{

            this.setState({
                searchResult: searchResult,
            });
        }

    }


    render() {
        const { isLoading, contacts, keyword, searchPhone, searchInProgress, searchResult } = this.state;

        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? 'padding' : null}
                    keyboardVerticalOffset={(Platform.OS === 'ios') ? 44 + statusBarHeight : 0}>

                    {(this.state.selectedUsers.length > 0) ?
                        (<View style={{ height: 48}} >
                                <FlatList
                                    data={this.state.selectedUsers}
                                    horizontal
                                    keyExtractor={item => item.userId}
                                    extraData = {this.state.dataVersion}
                                    renderItem={({item, index}) =>(
                                        <SelectedContact
                                            key = {index}
                                            item = {item}
                                            onPress = {()=>{ this.selectUser(item)}}
                                        />)}
                                />
                            </View>) : null}

                    <View style={styles.topContainer}>
                        <View
                            style={{
                                backgroundColor: '#f8f9fb',
                                height: 40,
                                borderRadius: 8,
                                flexDirection: 'row',
                            }}
                        >
                            <TextInput
                                value={keyword}
                                selectionColor = "#000"
                                onChangeText={text => {
                                    this.setState({
                                        keyword: text,
                                    });
                                    this.search(text);
                                }}
                                style={{
                                    fontFamily: 'Roboto-Regular',
                                    fontSize: 16,
                                    height: 40,
                                    padding: 0,
                                    paddingLeft: 20,
                                    flex: 1,
                                }}
                                placeholder="Хайх утас болон нэр оруулна уу"
                                placeholderTextColor={colors.placeholderText}
                            />
                            <TouchableOpacity
                                style={{
                                    width: 36,
                                    height: 40,
                                    paddingLeft: 8,
                                    justifyContent: 'center',
                                }}
                            >
                                <Image source={require('../image/search.png')} resizeMode="contain" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {searchPhone.length === 8 && (
                        <React.Fragment>
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Light',
                                    fontSize: 14,
                                    color: colors.mutedText,
                                    padding: 15,
                                    paddingBottom: 5,
                                }}
                            >
                                Бүх хэрэглэгчдээс хайсан илэрц:
                            </Text>
                            {searchInProgress ? (
                                <View style={{ height: 64, justifyContent: 'center' }}>
                                    <ActivityIndicator />
                                </View>
                            ) : (
                                    searchResult != null ? (
                                        this.renderContact({ item: searchResult, isSearchResult: true })
                                    ) : (
                                            <Text
                                                style={{
                                                    fontFamily: 'Roboto-Regular',
                                                    fontSize: 16,
                                                    color: colors.mutedText,
                                                    paddingVertical: 16,
                                                    textAlign: 'center',
                                                }}
                                            >
                                                Илэрц олдсонгүй.
                                            </Text>
                                        )
                                )}
                        </React.Fragment>
                    )}
                    <FlatList
                        keyboardShouldPersistTaps = 'handled'
                        data={contacts.filter(contact =>
                            contact.contactName.toLowerCase().includes(keyword.toLowerCase()) ||
                            contact.phone.includes(keyword)
                        )}
                        refreshing={isLoading}
                        keyExtractor={item => item.userId}
                        extraData = {this.state.dataVersion}
                        renderItem={this.renderContact}
                        ListHeaderComponent={
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Light',
                                    fontSize: 14,
                                    color: colors.mutedText,
                                    padding: 15,
                                    paddingBottom: 5,
                                }}>
                                Найзууд:
                            </Text>
                        }
                        ListEmptyComponent={
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Regular',
                                    fontSize: 16,
                                    color: colors.mutedText,
                                    paddingVertical: 16,
                                    textAlign: 'center'
                                }}
                            >
                                {contacts.length > 0 ? 'Илэрц олдсонгүй.' : '...'}
                            </Text>
                        }
                        style={{ flex: 1 }}
                    />
                    <Button disabled = {this.state.selectedUsers.length == 0} style = {{margin: 16, marginTop: 0}} title = "Үргэлжлүүлэх" onPress = {this.startChat} />
                    {this.shouldInvite() ? <Button style = {{margin: 16, marginTop: 0}} title = "Урих" onPress = {this.invite} /> : null}

                </KeyboardAvoidingView>
                <ActivityIndicatorView animating={this.state.isLoading} light />

                </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bgGray,
    },

    topContainer: {
        backgroundColor: 'white',
        padding: 8,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.headerLineColor,
    },

    text: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },

    inviteButton:{
        height: 36,
        borderRadius: 4,
        backgroundColor: '#FA7268'
    },
});

export default ContactsScreen;
