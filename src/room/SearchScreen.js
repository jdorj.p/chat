import React, { Component } from "react";
import {View, FlatList , StyleSheet, SafeAreaView, KeyboardAvoidingView} from 'react-native';
import HeaderButton from '../components/HeaderButton';
import SearchInput from '../components/SearchInput';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import {SearchItem} from './SearchItem';
import Border from '../components/Border';

export default class SearchScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            data: [],
            isLoading: false
        }
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            headerShown: false,
        })
    }

    componentWillUnmount(): void {

    }

    getData = async (text) =>  {

        if(text.length != 8){
            this.setState({data: []});
            return
        }

        this.setState({isLoading: true});

        // let token = await AsyncStorage.getItem("token");

        let url = 'https://api.minu.mn/mimap/social/searchPeople?searchContent=' + text

        fetch(url,{
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                // 'Authorization': token,
                'X-API-KEY': "MID 3&sbuH72763P9O6:ZjM5OGM4ZDUzYWVlNWMzNDMyZWRhYTc2M2JiOGMwMWYzNDFlODFlZg==",
            },
        }).then((response) => response.json())
            .then((json) => {

                console.log(json)

                if(json.responseResultType == "FAILURE"){

                    this.setState({
                        isLoading: false,
                    });

                    CustomAlert.alert("Алдаа гарлаа!",error.message.messages[0].message, 'error');

                }else{

                    this.setState({
                        isLoading: false,
                        data: json,
                    });
                }

            })
            .catch((error) => {

                CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

                this.setState({
                    isLoading: false,
                    error: error,
                });

            });
    }

    render() {

        return (
            <SafeAreaView style = {styles.container} >
                <View style = {{flexDirection: 'row', padding: 8, paddingLeft: 16, paddingRight: 16, paddingBottom: 8}}>
                    <HeaderButton  icon = "close" onPress={()=>this.props.navigation.goBack()} style = {{}} />
                    <SearchInput onChangeText = {this.getData} style = {{flex:1,marginLeft: 8,}} placeholder = "Утас" />
                </View>
                <Border/>
                <KeyboardAvoidingView style = {{flex:1}} >
                    <FlatList
                        style = {styles.flatList}
                        data={this.state.data}
                        renderItem={({item, index}) =>(

                            <SearchItem
                                onPress = {()=>{
                                    this.props.navigation.navigate('Chat', {users:[item]})
                                }}
                                key = {index}
                                item = {item}
                            />)}
                    />
                    <ActivityIndicatorView animating = {this.state.isLoading} light/>
                </KeyboardAvoidingView>
            </SafeAreaView>

        );
    }

}

const styles = StyleSheet.create({

    container: {
        flex:1,
        backgroundColor: '#fff'
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
        borderRadius: 18,
        minHeight: 36,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        // borderRadius: 18,
        margin: 8,
        marginRight: 0,
        // backgroundColor: '#4CA2EB',
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        borderRadius: 18,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },

    imageBackground: {
        flex: 1,
        margin: 16,
    },

    scrollView:{
        flex:1,
    },

    title: {
        fontFamily: 'HeroldMon-Bold',
        color: '#fff',
        fontSize: 22,
        textAlign: 'center',
        margin: 8,
        marginBottom: 24,
    },

    text: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
    },

    textBottom: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
        textAlign: 'center',
    },

    exitButton: {
        width: 44,
        height: 44,
        position: 'absolute',
        right: 8,
        top: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },

    exitImage: {
        width: 34,
        height: 34,
    },

});
