import React, { Component } from "react";
import {View, Text, TouchableOpacity, FlatList , StyleSheet, SafeAreaView} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {RoomListItem} from './RoomListItem';
import HeaderButton from '../components/HeaderButton';
import {ProfileImage} from '../common/ProfileImage';
import UserUtil from '../common/UserUtil';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import PushNotification from '../common/PushNotification';
import ChatUtil from '../chat/ChatUtil';
import {RoomUtil} from './RoomUtil';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import {StompEventTypes, stompContext } from '../chat/Stomp'
import Border from '../components/Border';


class RoomListScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            user: {},
            rooms: [],
            dataVersion: 0,
            status: "Холбогдож байна",
            statusColor: Constants.COLORS.INFO,
            refreshing: false,
        }
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            headerShown: false,
        })

        PushNotification.handle((data)=>{

            console.log(data)

            if(data.roomId){

                console.log('ChatScreen',ChatUtil.chatScreen)

                if(ChatUtil.chatScreen){

                    ChatUtil.chatScreen.getRoom(data.roomId)
                }else{
                    this.props.navigation.navigate('Chat',{roomId: data.roomId})
                }
            }

        })

        this.unsubscribeFocus = this.props.navigation.addListener(
            'focus',
            payload => {

                UserUtil.getData().then((data)=>{

                    this.setState({user: data})

                })

                this.getData(false)
            }
        );

        this.connect()

    }


    connect = () => {

        stompContext.getStompClient()
        // console.log(client)
        // if(client.subscribe){
        //     this.getData()
        // }

        stompContext.addStompEventListener(
            StompEventTypes.Connect,
            this.onConnect,
            this
        )

        stompContext.addStompEventListener(
            StompEventTypes.Disconnect,
            this.onDisconnect,
            this
        )

        stompContext.addStompEventListener(
            StompEventTypes.WebSocketClose,
            this.onWebSocketClose,
            this
        )
    }

    onConnect = () => {
        this.setState({status: 'Холбогдлоо',statusColor: Constants.COLORS.SUCCESS})
        this.getData()
    }

    onDisconnect = () => {
        this.setState({status: 'Холболт саллаа',statusColor: Constants.COLORS.ERROR})
    }

    onWebSocketClose = () => {
        this.setState({status: 'Холболт саллаа',statusColor: Constants.COLORS.ERROR})
    }



    componentWillUnmount(): void {

        this.unsubscribeFocus();
        this.unsubscribe();

        stompContext.removeStompEventListener(StompEventTypes.Connect,this.onConnect,this)
        stompContext.removeStompEventListener(StompEventTypes.Disconnect,this.onDisconnect,this)
        stompContext.removeStompEventListener(StompEventTypes.WebSocketClose,this.onWebSocketClose,this)

    }

    subscribe = () => {

        this.unsubscribe()

        // console.log('room list subscribe')
        console.log('stomp client room',stompContext.getStompClient())


        try {

            this.subscribed = stompContext.getStompClient().subscribe('/queue/private.room.' + this.state.user.userId,
                (message) => {

                    let json = JSON.parse(message.body)
                    console.log(json)
                    this.setState({
                        rooms: json,
                        dataVersion: this.state.dataVersion + 1
                    })

                    ChatUtil.setBadgeNumber(json)

                })
        }catch (e) {

            console.log(e)
        }
    }

    getData = async (isLoading = true) => {

        this.setState({isLoading: isLoading});

        let userId = this.state.user.userId

        try{
            if(!userId){
                userId = await AsyncStorage.getItem("userId")
            }
        }catch (e) {
            console.log(e)
        }

        if(!this.roomUtil){
            this.roomUtil = new RoomUtil(userId)
        }

        fetch(Constants.WSURL + "wsock/room/findByUserId?userId=" + userId, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {

                console.log("findByUserId",json)
                // console.log("findByUserId",userId)

                if(json.status == "000") {


                    if(json.entity.length == 0){

                        console.log(userId)

                        this.findContact()
                    }
                    this.setState({
                        status: null,
                        rooms: json.entity,
                        dataVersion: this.state.dataVersion + 1,
                        refreshing: false,
                    })

                    ChatUtil.setBadgeNumber(json.entity)

                }else{

                    console.log('error',json)
                    CustomAlert.alert("Алдаа гарлаа!",json.errors, 'error');
                }

                this.subscribe()

                this.setState({
                    isLoading: false,
                    refreshing: false,
                })

            }).catch((error) => {

            console.log(error)

            CustomAlert.alert("Алдаа гарлаа!",error.message, 'error');

            this.setState({
                isLoading: false,
                refreshing: false,
            });
        });
    }

    refresh = () => {

        this.setState({refreshing: true});
        this.getData(false)

    }

    unsubscribe = () => {

        this.subscribed && this.subscribed.unsubscribe()
    }

    findContact = () => {

        console.log('find contact')

        this.props.navigation.navigate('Contacts')

    }

    render() {

        return (
            <SafeAreaView style = {styles.safeAreaView} >
                <View style = {styles.header} >
                    <View style = {{flexDirection: 'row'}} >
                    <TouchableOpacity onPress = {()=>this.props.navigation.navigate('User')} >
                        <ProfileImage size = {32} url = {this.state.user.profileImg}  />
                    </TouchableOpacity>
                    <Text style = {styles.title} >Чатууд</Text>
                    </View>
                    <HeaderButton icon = "add-circle" title = " Чат бичих" onPress={this.findContact} style = {{marginRight: 16}} />
                </View>
                <Border/>
                {(this.state.status) ? (<View style = {[styles.statusContainer,{backgroundColor: this.state.statusColor}]} ><Text style = {styles.status} >{this.state.status}</Text></View>) : null}
                <FlatList
                    style = {styles.flatList}
                    data={this.state.rooms}
                    extraData = {this.state.dataVersion}
                    refreshing = {this.state.refreshing}
                    onRefresh = {this.refresh}
                    renderItem={({item, index}) =>(
                        <RoomListItem
                            key = {index}
                            item = {item}
                            roomUtil = {this.roomUtil}
                            userId = {this.state.user.userId}
                            onPress = {()=>{
                                this.props.navigation.navigate('Chat',{room:item,
                                })
                            }}
                        />)}
                />
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}

export default RoomListScreen
// export default withStomp(RoomListScreen)

const styles = StyleSheet.create({

    safeAreaView:{
        flex:1,
        backgroundColor: '#fff',
    },

    container: {
        flex:1,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        // backgroundColor: '#fff',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 22,
        textAlign: 'center',
        marginLeft: 8,
    },

    statusContainer: {
        padding: 8,
        alignItems: 'center',
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
        borderRadius: 18,
        minHeight: 36,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        // borderRadius: 18,
        margin: 8,
        marginRight: 0,
        // backgroundColor: '#4CA2EB',
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        borderRadius: 18,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },

    imageBackground: {
        flex: 1,
        margin: 16,
    },

    scrollView:{
        flex:1,
    },

    text: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
    },

    textBottom: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
        textAlign: 'center',
    },

    exitButton: {
        width: 44,
        height: 44,
        position: 'absolute',
        right: 8,
        top: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },

    exitImage: {
        width: 34,
        height: 34,
    },

});
