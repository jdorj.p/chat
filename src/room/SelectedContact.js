import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";


export default class SelectedContact extends React.Component {

    render() {

        let text = (this.props.item.contactName) ? this.props.item.contactName : this.props.item.phone

        return (<View style = {[styles.container,this.props.style]}>
            <Text style={styles.text} >{text}</Text>
            <TouchableOpacity onPress = {this.props.onPress} style = {styles.touchableOpacity} >
                <Icon name = "close" size = {16} color = "#fff" />
            </TouchableOpacity>
        </View>);

    }

}


const styles = StyleSheet.create({

    container: {
        height: 32,
        borderRadius: 4,
        backgroundColor: '#FA7268',
        flexDirection: 'row',
        margin: 8,
        marginRight: 0,
        alignItems: 'center',
    },

    text: {
        lineHeight: 24,
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
        marginLeft: 8,
    },

    touchableOpacity: {
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#000',
        height: 32,
        width: 24
    }
});
