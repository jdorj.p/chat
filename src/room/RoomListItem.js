import React, { Component } from "react";
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {ProfileImage} from '../common/ProfileImage';
import DateUtil from '../common/DateUtil';
import ChatUtil from '../chat/ChatUtil';
import StatusView from '../components/StatusView';

export class RoomListItem extends Component {

    constructor(props){
        super(props);

        this.state = {
        }
    }

    componentDidMount() {

    }



    render() {

        let item = this.props.item

        let textStyle = {}

        let isSeen = ChatUtil.isSeen(item.lastChatMessage,this.props.userId)

        if(!isSeen){
            textStyle.fontFamily = 'RobotoCondensed-Bold'
        }


        let lastActiveUser = this.props.roomUtil.getLastActiveUser(item.userList)

        return (
            <TouchableOpacity style = {styles.container} onPress = {this.props.onPress} >
                <ProfileImage user = {this.props.roomUtil.roomImage(item)} disabled >
                    <StatusView user = {lastActiveUser} />
                </ProfileImage>
                <View style = {{flex: 1, marginLeft: 8, marginRight: 8, justifyContent: 'center'}} >
                    <Text style = {[styles.text,textStyle]} >
                        {this.props.roomUtil.roomName(item)}
                    </Text>
                    <View style = {{flexDirection: 'row', marginTop: 2}} >
                        <Text style = {[styles.content,textStyle]} numberOfLines={1} >{this.props.roomUtil.getContent(item.lastChatMessage,item.isGroup)}</Text>
                        <Text style = {[styles.date,textStyle]} >• {DateUtil.format(item.lastDate)}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        padding: 8,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
        // flex:1,
    },

    content: {
        flex:1,
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 16,
    },

    date: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 14,
    },


});
