import AsyncStorage from '@react-native-community/async-storage';


export class RoomUtil {

    constructor(userId) {

        this.userId = userId

    }


    roomName = (room) => {

        if(!room){
            return ""
        }

        if(room.name){
            return room.name
        }

        if(!room.userList){
            return ""
        }

        let users = room.userList.filter((user) => user.userId != this.userId)
        let userNames = users.map((user) => user.name)
        return userNames.join(", ").trim()

    }

    roomImage = (room) => {

        if(!room){
            return undefined
        }

        if(room.isGroup){
            return room.userList.filter((item)=>item)
        }else{
            let users = room.userList.filter((user) => user.userId != this.userId)
            return users
        }
    }

    otherUsers = (userList) => {

        if(!userList){
            return ""
        }

        let users = userList.filter((user) => user.userId != this.userId)
        return users

    }

    getLastActiveUser = (userList) => {

        if(!userList){
            return undefined
        }

        let users = userList.filter((user) => user.userId != this.userId)

        users = users.sort((a,b)=>{
            if(a.lastOnlineDate < b.lastOnlineDate){
                return 1
            }else{
                return -1
            }
        })

        return users[0]

    }

    setCurrentChatStatus = (roomList) => {

        if(!ChatUtil.chatScreen){
            return
        }

        let currentRoomId = ChatUtil.chatScreen.state.room.id

        for(let i = 0; i < roomList.length; i ++){

            let room = roomList[i]

            if(room.id == currentRoomId){

                let user = this.getLastActiveUser(room.userList)
                ChatUtil.chatScreen.setStatus(user)
                return;
            }
        }
    }

    getContent = (message,isGroup) => {

        if(!message){
            return null
        }

        let content = ''
        let from

        if(message){

            if(message.type == 'IMAGE'){
                content = 'Зураг'
            }else{
                content = message.content
            }

            if(message.fromUserId == this.userId) {
                from = 'Та'
            }else if(isGroup){
                from = message.fromUser.name
            }
        }

        if(from){
            return from + ': ' + content
        }else{
            return content
        }


    }

}
