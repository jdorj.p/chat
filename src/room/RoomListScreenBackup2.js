import React, { Component } from "react";
import {View, Text, TouchableOpacity, FlatList , StyleSheet, SafeAreaView} from 'react-native';
import {RoomListItem} from './RoomListItem';
import HeaderButton from '../components/HeaderButton';
import {ProfileImage} from '../common/ProfileImage';
import UserUtil from '../common/UserUtil';
import Constants from '../common/Constants';
import CustomAlert from '../common/CustomAlert';
import AsyncStorage from '@react-native-community/async-storage';
import PushNotification from '../common/PushNotification';
import ChatUtil from '../chat/ChatUtil';
import {RoomUtil} from './RoomUtil';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
import Border from '../components/Border';
import Room from '../data/Room';

class RoomListScreen extends Component {

    constructor(props){
        super(props);

        props.navigation.setOptions({
            headerShown: false,
         })

        this.state = {
            user: {},
            rooms: [],
            dataVersion: 0,
            refreshing: false,
        }

        Room.open()
    }

    componentDidMount() {

        PushNotification.handle((data)=>{

            if(data.roomId){

                console.log('ChatScreen',ChatUtil.chatScreen)

                if(ChatUtil.chatScreen){
                    ChatUtil.chatScreen.getRoom(data.roomId)
                }else{
                    this.props.navigation.navigate('Chat',{roomId: data.roomId})
                }
            }

        })

        this.unsubscribeFocus = this.props.navigation.addListener(
            'focus',
            payload => {

                UserUtil.getData().then((data)=>{

                    this.setState({user: data})

                })

            }
        );

        this.start()
    }

    start = async () => {

        await  this.getData()

        Room.addListener('fetching',()=>{
            if(this.state.rooms.length == 0){
                this.setState({isLoading: true})
            }
        })

        Room.addListener('update',(rooms)=>{
            this.setState({
                rooms: rooms,
                dataVersion: this.state.dataVersion + 1,
                isLoading: false,
            })
        })

        Room.addListener('offline',()=>{
            this.setState({status: 'Холболт салсан байна',statusColor: Constants.COLORS.ERROR})
        })

        Room.addListener('online',()=>{
            this.setState({status: null,statusColor: null})
        })

        Room.addListener('error',(message)=>{
            this.setState({isLoading: false})
            CustomAlert.alert("Алдаа гарлаа!",message, 'error');
        })
    }

    getUserId = async () => {

        if(!this.userId){
            this.userId = await AsyncStorage.getItem('userId')
        }
        return  this.userId
    }

    getRoomUtil = async () => {

        if(!this.roomUtil) {
            this.roomUtil = new RoomUtil(await this.getUserId())
        }
        return  this.roomUtil
    }

    componentWillUnmount(): void {
        this.unsubscribeFocus();
        Room.close()
    }


    getData = async () => {

        await this.getRoomUtil()
        let rooms = await Room.getData()
        this.setState({
            rooms: rooms,
            dataVersion: this.state.dataVersion + 1,
            refreshing: false,
        })
    }

    refresh = () => {

        this.setState({refreshing: true});
        this.getData(false)

    }

    findContact = () => {

        console.log('find contact')
        this.props.navigation.navigate('Contacts')

    }

    render() {

        return (
            <SafeAreaView style = {styles.safeAreaView} >
                <View style = {styles.header} >
                    <View style = {{flexDirection: 'row'}} >
                    <TouchableOpacity onPress = {()=>this.props.navigation.navigate('User')} >
                        <ProfileImage size = {32} url = {this.state.user.profileImg}  />
                    </TouchableOpacity>
                    <Text style = {styles.title} >Чатууд</Text>
                    </View>
                    <HeaderButton icon = "add-circle" title = " Чат бичих" onPress={this.findContact} style = {{marginRight: 16}} />
                </View>
                <Border/>
                {(this.state.status) ? (<View style = {[styles.statusContainer,{backgroundColor: this.state.statusColor}]} ><Text style = {styles.status} >{this.state.status}</Text></View>) : null}
                <FlatList
                    style = {styles.flatList}
                    data={this.state.rooms}
                    extraData = {this.state.dataVersion}
                    refreshing = {this.state.refreshing}
                    onRefresh = {this.refresh}
                    renderItem={({item, index}) =>(
                        <RoomListItem
                            key = {index}
                            item = {item}
                            roomUtil = {this.roomUtil}
                            userId = {this.state.user.userId}
                            onPress = {()=>{
                                this.props.navigation.navigate('Chat',{room:item,
                                })
                            }}
                        />)}
                />
                <ActivityIndicatorView animating = {this.state.isLoading} />
            </SafeAreaView>

        );
    }

}

export default RoomListScreen

const styles = StyleSheet.create({

    safeAreaView:{
        flex:1,
        backgroundColor: '#fff',
    },

    container: {
        flex:1,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        // backgroundColor: '#fff',
    },

    title: {
        fontFamily: 'RobotoCondensed-Bold',
        color: '#000',
        fontSize: 22,
        textAlign: 'center',
        marginLeft: 8,
    },

    statusContainer: {
        padding: 8,
        alignItems: 'center',
    },

    status: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#fff',
        fontSize: 16,
    },

    flatList: {
        flex:1,
    },

    inputContainer: {
        flexDirection: 'row',
        backgroundColor: '#E4EAF1',
    },

    input: {
        flex:1,
        backgroundColor: '#fff',
        margin: 8,
        borderRadius: 18,
        minHeight: 36,
    },

    buttonLeft:{
        height: 36,
        width: 36,
        // borderRadius: 18,
        margin: 8,
        marginRight: 0,
        // backgroundColor: '#4CA2EB',
        backgroundColor: 'transparent',
    },

    button:{
        height: 36,
        width: 36,
        borderRadius: 18,
        margin: 8,
        marginLeft: 0,
        backgroundColor: '#4CA2EB'
    },

    imageBackground: {
        flex: 1,
        margin: 16,
    },

    scrollView:{
        flex:1,
    },

    text: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
    },

    textBottom: {
        fontFamily: 'HeroldMon-Bold',
        color: '#6F322A',
        fontSize: 16,
        margin: 16,
        textAlign: 'center',
    },

    exitButton: {
        width: 44,
        height: 44,
        position: 'absolute',
        right: 8,
        top: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },

    exitImage: {
        width: 34,
        height: 34,
    },

});
