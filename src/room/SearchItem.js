import React, { Component } from "react";
import {View, Text,TouchableOpacity , StyleSheet} from 'react-native';
import {ProfileImage} from '../common/ProfileImage';

export class SearchItem extends Component {

    constructor(props){
        super(props);

        this.state = {
        }
    }

    componentDidMount() {

    }

    render() {

        let item = this.props.item

        return (
            <TouchableOpacity style = {styles.container} onPress = {this.props.onPress} >
                <ProfileImage size = {32} url = {item.userPhoto} />
                <View style = {{marginLeft: 8, marginRight: 8, justifyContent: 'center'}} >
                    <Text style = {styles.name} >{item.lastName} {item.firstName}</Text>
                    <Text style = {styles.message} >{item.phoneNo}</Text>
                </View>
            </TouchableOpacity>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        padding: 8,
    },

    name: {
        fontFamily: 'Roboto-Bold',
        color: '#000',
        fontSize: 14,
    },

    text: {
        fontFamily: 'RobotoCondensed-Regular',
        color: '#000',
        fontSize: 14,
        marginTop: 8,
    },


});
