import React, { Component } from "react";
import {StyleSheet, StatusBar, SafeAreaView, Dimensions} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import HeaderButton from '../components/HeaderButton';
import ActivityIndicatorView from '../common/ActivityIndicatorView';
const windowWidth = Dimensions.get('window').width;

export default class ImageScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {images: [{url: props.route.params.url + '?w=' + Math.floor(windowWidth * 2)}]}
        console.log(this.state.images)
    }

    componentDidMount() {

        this.props.navigation.setOptions({
            headerShown: false,
        })
    }

    componentWillUnmount(): void {

    }

    render() {

        return (<SafeAreaView style = {styles.safeAreaView} >
                    <StatusBar barStyle={'light-content'} />
                    <ImageViewer style = {styles.imageViewer} imageUrls={this.state.images} renderIndicator={()=>null} loadingRender = {()=>(<ActivityIndicatorView animating = {true}/>)} />
                    <HeaderButton iconColor = "#fff" icon = "close" onPress={()=>this.props.navigation.goBack()} style = {styles.close} />
                </SafeAreaView>);
    }

}

const styles = StyleSheet.create({

    container: {
        flex:1,

    },

    safeAreaView: {
        flex:1,
        backgroundColor: '#000'
    },

    imageViewer:{
        position: 'absolute',
        left:0,
        top:0,
        right:0,
        bottom:0
    },

    close:{
        margin:16,
    }

});
