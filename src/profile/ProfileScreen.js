import React from 'react';
import {
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    View,
    Text,
    Platform,
    TouchableOpacity,
    Linking,
} from 'react-native';

import { getStatusBarHeight } from 'react-native-status-bar-height';
import ListButton from '../components/ListButton';
import {ProfileImage} from '../common/ProfileImage';
import AsyncStorage from '@react-native-community/async-storage';
import Button from '../components/Button';
import ChatUtil from '../chat/ChatUtil';
import Constants from '../common/Constants';
import {RoomUtil} from '../room/RoomUtil';
import Badge from '../components/Badge';
import CustomAlert from '../common/CustomAlert';
import {MessageTypes} from '../data/MessageAdapter';
import getRealm from '../data/Realm';
let statusBarHeight = getStatusBarHeight(true);


export default class ProfileScreen extends React.Component {


    constructor(props){
        super(props);


        this.state = {
            user: props.route.params.user,
            room: props.route.params.room,
            isMuted: false
        }

    }

    componentDidMount() {

        this.props.navigation.setOptions({
            title: "",
        })

        this.getContractCount()
        this.getEscrowCount()
        this.isMuted()
    }


    isMuted = async () => {

        let userId = await AsyncStorage.getItem('userId')

        console.log(this.state.room,)

        let muteUserList = this.state.room.muteUserList.map(item=>item)

        this.setState({
            isMuted: muteUserList.includes(userId)
        })
    }

    componentWillUnmount() {

    }

    getContractCount = async () => {

        if(!this.state.room){
            return
        }

        let userId = await AsyncStorage.getItem('userId')
        let url = Constants.WSURL + "wsock/chat/contractListByUserId?userId=" + userId

        let withUserId = new RoomUtil(userId).otherUsers(this.state.room.userList)[0].userId

        if(withUserId){
            url += '&withUserId=' + withUserId
        }

        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {

                if(json.status == "000") {

                    this.setState({
                        contractCount: json.entity.length
                    })

                }

            }).catch((error) => {

            console.log(error)
        });
    }

    getEscrowCount = async () => {

        if(!this.state.room){
            return
        }

        let userId = await AsyncStorage.getItem('userId')

        let url = Constants.WSURL + "wsock/invoice/list?roomId="+ this.state.room.id +"&userId=" + userId

        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then((response) => response.json())
            .then((json) => {


                if(json.status == "000") {

                    this.setState({
                        escrowCount: json.entity.length
                    })
                }

            }).catch((error) => {

            console.log(error)
        });
    }

    contract = () => {

        if(ChatUtil.chatScreen){
            ChatUtil.chatScreen.setRoomByUsers([this.state.user])
            this.props.navigation.navigate('Chat', {users: [this.state.user]})
        }else{
            this.props.navigation.navigate('Chat', {users: [this.state.user]})
        }
    }

    contracts = async () => {

        let userId = await AsyncStorage.getItem('userId')
        let withUserIds = this.state.room.userIdList.filter((id)=> id != userId)
        this.props.navigation.navigate('Contracts',{withUserId: withUserIds[0]})

    }

    escrow = () => {

        this.props.navigation.navigate('Escrow',{roomId: this.state.room.id, receiverName: this.state.user.name})
    }


    call = () => {

        let url = "tel:" + this.state.user.phone
        Linking.openURL(url)
    }

    mute = async () => {

        let userId = await AsyncStorage.getItem('userId')

        let messageAdapter = this.props.route.params.messageAdapter

        let data = {
            roomId: this.state.room.id,
            fromUserId: userId,
        }

        if(this.state.isMuted){

            messageAdapter.send({
                type: MessageTypes.UNMUTE,
                ...data
            },false)

            this.setState({isMuted: false})

        }else{

            CustomAlert.confirm("Мэдэгдэл хаах","Та энэ чатнаас мэдэгдэл ирэхийг хаах гэж байна уу?",(response)=>{

                if(response){

                    messageAdapter.send({
                        type: MessageTypes.MUTE,
                        ...data
                    },false)

                    this.setState({isMuted: true})

                }

            })

        }

    }

    render() {


        return (
            <SafeAreaView style = {{flex: 1, backgroundColor: '#fff'}} >
                <KeyboardAvoidingView style = {{flex: 1}} behavior= {(Platform.OS === 'ios')? "padding" : null} keyboardVerticalOffset = {(Platform.OS === 'ios')? 88 + statusBarHeight : 0} >
                    <ScrollView >

                        <View style = {{padding: 16,flexDirection: 'row',alignItems: 'center'}} >

                            <TouchableOpacity onPress = {()=> this.state.user.avatar && this.props.navigation.navigate('Image',{url: this.state.user.avatar})} >
                                <ProfileImage size = {88} user = {[this.state.user]}  />
                            </TouchableOpacity>

                            <View style= {{marginLeft: 16}} >
                                { this.state.user.lastName ? <Text style = {styles.lastName} >{this.state.user.lastName}</Text> : null }
                                <Text style = {styles.name} >{this.state.user.name}</Text>
                                <TouchableOpacity onPress = {this.call}>
                                    <Text style = {styles.text} >{this.state.user.phone}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            {!this.state.room ? <Button noShadow cicon = "file-document-edit"  title = "Хэлцэл хийх" onPress = {this.contract} style = {{margin: 16}} /> : null }

                            {this.state.room ? <ListButton cicon = "file-document"  text = "Хэлцэлүүд" onPress = {this.contracts} >{this.state.contractCount ? <Badge count = {this.state.contractCount} style = {styles.badge} /> : null}</ListButton> : null }
                            {this.state.room ? <ListButton icon = "account-balance" iconColor = "#DB373D" text = "Эскроу данс" onPress = {this.escrow} >{this.state.escrowCount ? <Badge count = {this.state.escrowCount} style = {styles.badge} /> : null}</ListButton> : null }
                            {this.state.room ? <ListButton icon = {this.state.isMuted ? "notifications-off" : "notifications"} iconColor = "#DB373D" text = {this.state.isMuted ? "Мэдэгдэл нээх" : "Мэдэгдэл хаах"} onPress = {this.mute} hideChevron /> : null }
                        </View>

                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    imageBackground: {
        height: 88,
        width: 88,
        borderRadius: 44,
        overflow: 'hidden',
    },

    lastName: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 14,
        color: '#000',
    },

    name: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 20,
    },

    text:{
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#666666',
        marginTop: 4,
    },

    badge:{
        marginRight: -16,
    }
});
